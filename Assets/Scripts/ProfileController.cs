using System.Collections;
using System.Collections.Generic;
using EZXR.Glass.SixDof;
using UnityEngine;

public class ProfileController : MonoBehaviour
{
    public GameObject m_profilePrefab;
    private bool m_isInstantiated = false;
    // Start is called before the first frame update
    void Start()
    {
        m_isInstantiated = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (ARFrame.SessionStatus == EZVIOState.EZVIOCameraState_Tracking)
        {
            //实例化profile Prefab
            if(m_profilePrefab != null && m_isInstantiated == false)
            {
                GameObject profile = Instantiate(m_profilePrefab);
                //放置在HMDPoseTracker.Instance.leftCamera 正前方1米处，向下0.5米处
                profile.transform.position = HMDPoseTracker.Instance.leftCamera.transform.position + HMDPoseTracker.Instance.leftCamera.transform.forward * 1.0f;
                //profile.transform.position = new Vector3(profile.transform.position.x, profile.transform.position.y - 0.5f, profile.transform.position.z);
                profile.transform.rotation = HMDPoseTracker.Instance.leftCamera.transform.rotation;
                m_isInstantiated = true;
                Debug.Log("Profile Instantiated");
            }
            
        }

    }
}
