﻿using EZXR.Glass.Hand;
using EZXR.Glass.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HandTrackingDemo : MonoBehaviour
{
    public SpatialButton_Normal buttonSwitchRunMode;

    public ARHandManager arHand;

    private void OnEnable()
    {
        if (buttonSwitchRunMode != null)
        {
            string text = "Switch Hand RunMode to \n";
            if (arHand.handTrackingMode == ARHandManager.HandTrackingMode.HighEfficiency)
            {
                text += "HighPrecision";
            }
            else if (arHand.handTrackingMode == ARHandManager.HandTrackingMode.HighPrecision)
            {
                text += "HighEfficiency";
            }
            else
            {
                text += "Failed";
            }
            buttonSwitchRunMode.Text = text;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("EZXRGlassSDK/Demos/SamplesMenu/SamplesMenu");
        }
    }
    public void OnSwitchButton()
    {
        if (arHand == null)
        {
            return;
        }

        if (buttonSwitchRunMode != null)
        {
            string text = "Switch Hand RunMode to \n";
            ARHandManager.HandTrackingMode runMode = ARHandManager.HandTrackingMode.HighEfficiency;

            if (arHand.handTrackingMode == ARHandManager.HandTrackingMode.HighEfficiency)
            {
                runMode = ARHandManager.HandTrackingMode.HighPrecision;
                text += "HighEfficiency";
            }
            else if (arHand.handTrackingMode == ARHandManager.HandTrackingMode.HighPrecision)
            {
                runMode = ARHandManager.HandTrackingMode.HighEfficiency;
                text += "HighPrecision";
            }

            arHand.changeHandRunMode(runMode);

            buttonSwitchRunMode.Text = text;
        }
    }
    public void OnExitButton() {
        SceneManager.LoadScene("EZXRGlassSDK/Demos/SamplesMenu/SamplesMenu");
    }
}
