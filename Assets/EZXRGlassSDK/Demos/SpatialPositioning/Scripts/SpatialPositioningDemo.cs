﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpatialPositioningDemo : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("EZXRGlassSDK/Demos/SamplesMenu/SamplesMenu");
        }
    }
    public void OnExitButton()
    {
        SceneManager.LoadScene("EZXRGlassSDK/Demos/SamplesMenu/SamplesMenu");
    }
}
