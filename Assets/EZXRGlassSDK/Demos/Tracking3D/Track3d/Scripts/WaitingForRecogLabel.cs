﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaitingForRecogLabel : MonoBehaviour
{
    public Text label;

    private int detectingCount = 0;
    private string originText = "";
    // Start is called before the first frame update
    void Start()
    {
        originText = label.text;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (label != null) {
            label.text = originText;
            int dotNum = detectingCount / 20;
            for (int i = 0; i < dotNum; i++)
            {
                label.text += ".";
            }

            detectingCount++;
            if (detectingCount > 120)
                detectingCount = 0;
        }
        
    }
}
