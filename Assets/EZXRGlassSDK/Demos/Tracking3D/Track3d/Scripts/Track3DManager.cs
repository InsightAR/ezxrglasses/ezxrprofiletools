﻿using EZXR.Glass.SixDof;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZXR.Glass.Common;
using System.Runtime.InteropServices;
using UnityEngine.UI;
using System.IO;
using UnityEngine.SceneManagement;

[Serializable]
public class KVPair
{
    public GameObject objToShow;
    public string algModelPath;
}
[Serializable]
public enum ObjectType
{
    /// <summary>
    /// 大引擎
    /// </summary>
    TurbineBig,
    /// <summary>
    /// 小引擎
    /// </summary>
    TurbineSmall,
}
[Serializable]
public class ObjectInfo
{
    public ObjectType objectType;
    public KVPair infoPair;
}
public class Track3DManager : MonoBehaviour
{
    public static bool IsLocationSuccess { get { return isLocationSuccess; } }

    private static bool isLocationSuccess;

    /// <summary>
    /// 算法识别模型所在的路径以及对应要展示的3D资源
    /// </summary>
    public ObjectInfo[] objPathPair;
    /// <summary>
    /// 要进行识别的模型的类型
    /// </summary>
    public ObjectType targetTypeToShow;
    /// <summary>
    /// 场景中的3DPlacement
    /// </summary>
    public GameObject target3DObjectAnchor;

    private bool hasIntricsSet = false;
    private bool hasPoseOffsetSet = false;

    private bool isAssetsCopied = false;
    private EZXRTrack3dSession Track3dsession;
    private NormalRGBCameraDevice rgbCameraDevice;
    private EZVIOInputImage locCamImageBuffer;
    private float[] locCamIntriarray = new float[8];
    private OSTMatrices ost_m = new OSTMatrices();

    /// <summary>
    /// 当前正在识别的3D效果
    /// </summary>
    //public GameObject tips_Detecting;
    public Renderer tips_DetectTarget;
    public Texture2D[] detectTargetsImage;
    public GameObject tips_Scan;
    GameObject curObjToShow;
    GameObject curObjMeshToShow;
    public Animator animator;
    public GameObject ui;
    public GameObject tips_Success;

    // Start is called before the first frame update
    private void Awake()
    {
        isAssetsCopied = false;
        StartCoroutine(copyTrackAsset());
    }

    private void Start()
    {
        ShowObj((int)targetTypeToShow);
    }

    private void OnEnable()
    {
        StartCoroutine(startRGBCamera());
        //StartCoroutine(startTrackSession());
    }
    private void OnDisable()
    {
        stopTrackSession();
        stopRGBCamera();

        foreach (ObjectInfo itm in objPathPair)
        {
            itm.infoPair.objToShow.SetActive(false);
        }
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("EZXRGlassSDK/Demos/SamplesMenu/SamplesMenu");
        }
    }
    private void LateUpdate()
    {
        //UpdateCameraImage();
        RelocByTrack3d();

    }

    /// <summary>
    /// 启动或者从后台切到前台时调用
    /// </summary>
    /// <param name="pause"></param>
    private void OnApplicationPause(bool pause)
    {
        if (!pause)
        {
            if (!Application.isEditor)
            {
                Debug.Log("准备获得启动参数：");
                string startParams = AndroidUtils.GetActivityStartParams("startParams");
                Debug.Log("启动参数是：" + startParams);

                // ProcessLauncherStartParams(startParams);
            }
        }
    }


    private IEnumerator copyTrackAsset()
    {
        yield return null;
        AndroidJavaClass ezxrAssetUtilCls = new AndroidJavaClass("com.ezxr.ezglassarsdk.utils.AssetUtils");
        AndroidJavaClass unityActivityCls = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity = unityActivityCls.GetStatic<AndroidJavaObject>("currentActivity");
        isAssetsCopied = ezxrAssetUtilCls.CallStatic<bool>("copyAssetsToApplicationDir", unityActivity, "algModel/track3d");
        Debug.Log("copyTrackAsset isAssetsCopied=" + isAssetsCopied);
    }


    public void ShowObj(int id)
    {
        Debug.Log("-10001-  ShowObj id=" + id+ ",tips_DetectTarget="+ tips_DetectTarget+ ",ui="+ ui);
        if (tips_DetectTarget != null)
        {
            tips_DetectTarget.material.SetTexture("_MainTex", detectTargetsImage[id]);
        }
        //tips_Scan.SetActive(true);
        if (ui != null) { 
            ui.SetActive(false);
        }
        //tips_Success.SetActive(true);
        stopTrackSession();
        foreach (ObjectInfo itm in objPathPair)
        {
            itm.infoPair.objToShow.SetActive(false);
        }
        if (curObjMeshToShow != null)
        {
            curObjMeshToShow.SetActive(false);
        }
        targetTypeToShow = (ObjectType)id;
        StartCoroutine(startTrackSession());
    }

    private IEnumerator startRGBCamera()
    {
        yield return new WaitUntil(() => SessionManager.Instance != null && SessionManager.Instance.IsInited);
        hasIntricsSet = false;
        hasPoseOffsetSet = false;
        Debug.Log("-10001-startRGBCamera new rgbCameraDevice Open");
        locCamImageBuffer = new EZVIOInputImage();
        rgbCameraDevice = new NormalRGBCameraDevice();
        rgbCameraDevice.Open();
    }
    private void stopRGBCamera()
    {
        if (rgbCameraDevice != null)
            rgbCameraDevice.Close();
        if (locCamImageBuffer.fullImg != IntPtr.Zero)
        {
            Marshal.FreeHGlobal(locCamImageBuffer.fullImg);
            locCamImageBuffer.fullImg = IntPtr.Zero;
        }
    }

    private IEnumerator startTrackSession()
    {
        yield return new WaitUntil(() => isAssetsCopied);

        Track3dsession = new EZXRTrack3dSession();

        Debug.Log("-10001-startTrackSession Track3dsession.Create");
        if (Track3dsession != null)
        {
            foreach (ObjectInfo itm in objPathPair)
            {
                if (itm.objectType == targetTypeToShow)
                {
                    Track3dsession.Create(Application.persistentDataPath + "/" + itm.infoPair.algModelPath);
                    curObjToShow = itm.infoPair.objToShow;
                    //itm.infoPair.objToShow.SetActive(true);
                }
                //else
                //{
                //    itm.infoPair.objToShow.SetActive(false);
                //}
            }
        }
        InvokeRepeating("UpdateCameraImage", 0.5f, 3.0f);
        Debug.Log("-10001-startTrackSession end");
    }

    private void stopTrackSession()
    {
        Debug.Log("-10001-stopTrackSession");

        hasPoseOffsetSet = false;
        hasIntricsSet = false;

        if (Track3dsession != null)
        {
            Track3dsession.Destroy();
            Track3dsession = null;
        }
        CancelInvoke("UpdateCameraImage");
    }

    private void RelocByTrack3d()
    {
        bool isRelocSucc = false;
        //Debug.Log("-10001-RelocByTrack3d GetLocPose start");
        if (target3DObjectAnchor != null)
        {
            //Debug.Log("-10001-RelocByTrack3d GetLocPose start 0");
            if (Track3dsession != null)
            {
                //Debug.Log("-10001-RelocByTrack3d GetLocPose start 1");
                Pose pose = Pose.identity;
                isRelocSucc = Track3dsession.GettTrackedAnchorPose(ref pose);
                if (isRelocSucc)
                {
                    //Debug.Log("-10001-RelocByTrack3d GetLocPose success");
                    curObjToShow.SetActive(true);
                    if (ui != null)
                    {
                        ui.SetActive(false);
                    }

                    //Debug.Log("-10001-LateUpdate GettLocalizedAnchorPose successed");
                    target3DObjectAnchor.transform.position = pose.position;
                    target3DObjectAnchor.transform.rotation = pose.rotation;
                    //Invoke("stopTrackSession", 0.2f);
                    //stopTrackSession();
                }
                else
                {
                    curObjToShow.SetActive(false);
                    if (ui != null)
                    {
                        ui.SetActive(true);
                    }
                    //Debug.Log("-10001-RelocByTrack3d GetLocPose Failed");
                }
            }
        }
        if (isRelocSucc)
        {
            //tips_Detecting.SetActive(false);
            //tips_Scan.SetActive(false);
            //ui.SetActive(true);
        }
    }
    private void UpdateCameraImage()
    {
        if (ARFrame.SessionStatus != EZVIOState.EZVIOCameraState_Tracking)
            return;
        if (rgbCameraDevice == null)
        {
            Debug.Log("rgbCameraDevice is null");
            return;
        }
        if (Track3dsession == null)
        {
            Debug.Log("Track3dsession is null");
            return;
        }
        if (!hasPoseOffsetSet)
        {
            NativeTracking.GetOSTParams(ref ost_m);
            Track3dsession.SetPoseFromHeadToLocCam(ost_m.T_RGB_Head);
            hasPoseOffsetSet = true;
        }
        bool res = false;

        //Debug.Log("call getCurrentRGBImage");
        res = rgbCameraDevice.getCurrentImage(ref locCamImageBuffer, locCamIntriarray);
        //Debug.Log("call getCurrentRGBImage " + res);
        if (res)
        {
            if (!hasIntricsSet)
            {
                //Debug.Log("-10001-UpdateCameraImage Track3dsession.UpdateRGBCameraIntrics");
                Track3dsession.SetCameraIntrics(locCamIntriarray);
                hasIntricsSet = true;
            }
            double timestamp_sec = locCamImageBuffer.timestamp;
            Pose imageTsHeadPose = ARFrame.GetHistoricalHeadPose(timestamp_sec);
            Track3dsession.UpdateHeadPose(imageTsHeadPose, timestamp_sec);
            int format = (int)locCamImageBuffer.imgFormat;
            //Debug.Log("-10001-UpdateRgbFrame Track3dsession.UpdateRGBImage:"+ rgbImageBuffer.fullImg+","+timestamp_sec.ToString("f3"));
            //Debug.Log("-10001-UpdateRgbFrame Track3dsession.UpdateRGBImage:" + locCamImageBuffer.imgRes.width + "," + locCamImageBuffer.imgRes.height + ",fotmat=" + locCamImageBuffer.imgFormat);
            Track3dsession.UpdateImage(locCamImageBuffer.fullImg, timestamp_sec, (int)locCamImageBuffer.imgRes.width, (int)locCamImageBuffer.imgRes.height, format);
        }
    }

}
