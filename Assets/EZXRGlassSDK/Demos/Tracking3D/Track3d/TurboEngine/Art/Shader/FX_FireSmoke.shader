// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:0,cusa:True,bamd:0,cgin:,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:1,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:False,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:False,rfrpn:Refraction,coma:15,ufog:False,aust:False,igpj:False,qofs:1,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:33916,y:32317,varname:node_3138,prsc:2|emission-6099-OUT,voffset-9373-OUT;n:type:ShaderForge.SFN_TexCoord,id:5301,x:30884,y:31939,varname:node_5301,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Color,id:3312,x:31841,y:32583,ptovrint:False,ptlb:color_A,ptin:_color_A,varname:node_3312,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.4758621,c3:0,c4:1;n:type:ShaderForge.SFN_Color,id:3180,x:31841,y:32421,ptovrint:False,ptlb:color_B,ptin:_color_B,varname:_color_A_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.75,c2:0.8862069,c3:1,c4:1;n:type:ShaderForge.SFN_Lerp,id:4055,x:32140,y:32496,varname:node_4055,prsc:2|A-3180-RGB,B-3312-RGB,T-1586-OUT;n:type:ShaderForge.SFN_Slider,id:8674,x:32259,y:32667,ptovrint:False,ptlb:Mask_Ctrl,ptin:_Mask_Ctrl,varname:node_8674,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:10;n:type:ShaderForge.SFN_Tex2d,id:389,x:31686,y:32762,varname:node_389,prsc:2,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:2,isnm:False|UVIN-467-UVOUT,TEX-7611-TEX;n:type:ShaderForge.SFN_Multiply,id:6099,x:33600,y:32460,varname:node_6099,prsc:2|A-4055-OUT,B-2522-OUT,C-6491-OUT,D-2951-OUT;n:type:ShaderForge.SFN_Rotator,id:1163,x:31522,y:33003,varname:node_1163,prsc:2|UVIN-3644-OUT,ANG-3736-OUT;n:type:ShaderForge.SFN_Tau,id:4843,x:30791,y:32823,varname:node_4843,prsc:2;n:type:ShaderForge.SFN_Multiply,id:3166,x:30966,y:32747,varname:node_3166,prsc:2|A-4843-OUT,B-6966-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:7611,x:31522,y:33165,ptovrint:False,ptlb:Mask_1,ptin:_Mask_1,varname:node_7611,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:2795,x:31686,y:32913,varname:node_2795,prsc:2,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:0,isnm:False|UVIN-15-UVOUT,TEX-7611-TEX;n:type:ShaderForge.SFN_Tex2d,id:8077,x:31686,y:33060,varname:node_8077,prsc:2,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:0,isnm:False|UVIN-1163-UVOUT,TEX-7611-TEX;n:type:ShaderForge.SFN_Slider,id:6966,x:30601,y:32744,ptovrint:False,ptlb:Mask_Rotate,ptin:_Mask_Rotate,varname:node_6966,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:3;n:type:ShaderForge.SFN_Rotator,id:15,x:31510,y:32847,varname:node_15,prsc:2|UVIN-3644-OUT,ANG-3467-OUT;n:type:ShaderForge.SFN_Add,id:5718,x:31867,y:32788,varname:node_5718,prsc:2|A-389-R,B-2795-R,C-8077-R;n:type:ShaderForge.SFN_Rotator,id:467,x:31510,y:32691,varname:node_467,prsc:2|UVIN-3644-OUT,ANG-3166-OUT;n:type:ShaderForge.SFN_Multiply,id:3736,x:31324,y:32978,varname:node_3736,prsc:2|A-3467-OUT,B-6539-OUT;n:type:ShaderForge.SFN_Vector1,id:6539,x:31140,y:32991,varname:node_6539,prsc:2,v1:2;n:type:ShaderForge.SFN_Multiply,id:3525,x:32608,y:32494,varname:node_3525,prsc:2|A-8642-RGB,B-1586-OUT,C-8674-OUT,D-5724-OUT,E-8642-A;n:type:ShaderForge.SFN_Set,id:1771,x:31613,y:32178,varname:UV,prsc:2|IN-9470-OUT;n:type:ShaderForge.SFN_Get,id:3644,x:31303,y:32914,varname:node_3644,prsc:2|IN-1771-OUT;n:type:ShaderForge.SFN_Time,id:3417,x:31004,y:32067,varname:node_3417,prsc:2;n:type:ShaderForge.SFN_Slider,id:3022,x:30847,y:32361,ptovrint:False,ptlb:Time_Scale,ptin:_Time_Scale,varname:node_3022,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-50,cur:1,max:50;n:type:ShaderForge.SFN_VertexColor,id:8642,x:32337,y:32431,varname:node_8642,prsc:2;n:type:ShaderForge.SFN_RemapRange,id:1586,x:32012,y:32788,varname:node_1586,prsc:2,frmn:0,frmx:3,tomn:0,tomx:1|IN-5718-OUT;n:type:ShaderForge.SFN_Slider,id:2522,x:32608,y:32725,ptovrint:False,ptlb:Light_add,ptin:_Light_add,varname:node_2522,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:100;n:type:ShaderForge.SFN_Clamp01,id:6491,x:32765,y:32564,varname:node_6491,prsc:2|IN-3525-OUT;n:type:ShaderForge.SFN_NormalVector,id:2251,x:32651,y:32915,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:9373,x:32885,y:32856,varname:node_9373,prsc:2|A-2251-OUT,B-7741-OUT,C-2626-OUT,D-740-XYZ;n:type:ShaderForge.SFN_Slider,id:7741,x:32493,y:33091,ptovrint:False,ptlb:VertexOffset,ptin:_VertexOffset,varname:node_7741,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:0,max:10;n:type:ShaderForge.SFN_Max,id:3814,x:32221,y:32855,varname:node_3814,prsc:2|A-1586-OUT,B-2940-OUT;n:type:ShaderForge.SFN_Slider,id:2940,x:31907,y:32972,ptovrint:False,ptlb:OffsetMin,ptin:_OffsetMin,varname:node_2940,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Min,id:2626,x:32387,y:32855,varname:node_2626,prsc:2|A-3814-OUT,B-5367-OUT;n:type:ShaderForge.SFN_Slider,id:5367,x:32037,y:33089,ptovrint:False,ptlb:OffsetMax,ptin:_OffsetMax,varname:node_5367,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Set,id:8491,x:35114,y:31980,varname:Line,prsc:2|IN-1116-OUT;n:type:ShaderForge.SFN_Divide,id:3467,x:31155,y:32598,varname:node_3467,prsc:2|A-3166-OUT,B-5345-OUT;n:type:ShaderForge.SFN_Vector1,id:5345,x:30890,y:32514,varname:node_5345,prsc:2,v1:3;n:type:ShaderForge.SFN_Vector1,id:1116,x:34930,y:31991,varname:node_1116,prsc:2,v1:0;n:type:ShaderForge.SFN_Add,id:5882,x:31736,y:31747,varname:node_5882,prsc:2|A-3052-OUT,B-2558-OUT;n:type:ShaderForge.SFN_Append,id:2558,x:31392,y:31918,varname:node_2558,prsc:2|A-5872-X,B-5872-Y;n:type:ShaderForge.SFN_Vector4Property,id:5872,x:31214,y:31930,ptovrint:False,ptlb:Mask_Offset,ptin:_Mask_Offset,varname:node_3445,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0,v2:0,v3:0,v4:0;n:type:ShaderForge.SFN_Append,id:4914,x:31004,y:32194,varname:node_4914,prsc:2|A-8588-X,B-8588-Y;n:type:ShaderForge.SFN_Vector4Property,id:8588,x:30821,y:32175,ptovrint:False,ptlb:Panner_Speed,ptin:_Panner_Speed,varname:node_364,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0,v2:1,v3:0,v4:0;n:type:ShaderForge.SFN_Add,id:9470,x:31434,y:32178,varname:node_9470,prsc:2|A-4759-OUT,B-4877-OUT;n:type:ShaderForge.SFN_Multiply,id:4759,x:31228,y:32243,varname:node_4759,prsc:2|A-4914-OUT,B-3022-OUT,C-3417-T;n:type:ShaderForge.SFN_RemapRange,id:6946,x:31397,y:31723,varname:node_6946,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-2732-OUT;n:type:ShaderForge.SFN_Length,id:4265,x:31911,y:31747,varname:node_4265,prsc:2|IN-5882-OUT;n:type:ShaderForge.SFN_OneMinus,id:7205,x:32068,y:31747,varname:node_7205,prsc:2|IN-4265-OUT;n:type:ShaderForge.SFN_Lerp,id:3052,x:31618,y:31583,varname:node_3052,prsc:2|A-6946-OUT,B-4553-OUT,T-2298-OUT;n:type:ShaderForge.SFN_Vector2,id:4553,x:31397,y:31481,varname:node_4553,prsc:2,v1:0,v2:0;n:type:ShaderForge.SFN_Vector4Property,id:4973,x:31142,y:31541,ptovrint:False,ptlb:Mask_Adj,ptin:_Mask_Adj,varname:node_4973,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0,v2:0,v3:0,v4:0;n:type:ShaderForge.SFN_ComponentMask,id:2298,x:31397,y:31573,varname:node_2298,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-4973-XYZ;n:type:ShaderForge.SFN_Dot,id:5117,x:32553,y:32190,varname:node_5117,prsc:2,dt:1|A-1791-OUT,B-7084-OUT;n:type:ShaderForge.SFN_ViewVector,id:9996,x:31777,y:32231,varname:node_9996,prsc:2;n:type:ShaderForge.SFN_Multiply,id:6871,x:32716,y:32144,varname:node_6871,prsc:2|A-5117-OUT,B-5117-OUT;n:type:ShaderForge.SFN_Lerp,id:6837,x:33204,y:32132,varname:node_6837,prsc:2|A-4234-OUT,B-6871-OUT,T-1809-OUT;n:type:ShaderForge.SFN_Slider,id:1809,x:32841,y:32011,ptovrint:False,ptlb:Fresnel,ptin:_Fresnel,varname:node_1809,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:3;n:type:ShaderForge.SFN_Clamp01,id:6803,x:33363,y:32132,varname:node_6803,prsc:2|IN-6837-OUT;n:type:ShaderForge.SFN_TexCoord,id:5511,x:30884,y:31710,varname:node_5511,prsc:2,uv:1,uaff:False;n:type:ShaderForge.SFN_Multiply,id:6584,x:32261,y:31757,varname:node_6584,prsc:2|A-7205-OUT,B-9321-OUT;n:type:ShaderForge.SFN_Clamp01,id:5724,x:32490,y:31788,varname:node_5724,prsc:2|IN-6584-OUT;n:type:ShaderForge.SFN_Slider,id:9321,x:31911,y:31894,ptovrint:False,ptlb:Mask_Contrast,ptin:_Mask_Contrast,varname:node_9321,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:1,max:5;n:type:ShaderForge.SFN_Tex2d,id:1484,x:33054,y:32216,ptovrint:False,ptlb:Custom_Fresnel,ptin:_Custom_Fresnel,varname:node_1484,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:1abc5d7a124a2274f841285f60592f29,ntxv:2,isnm:False|UVIN-5572-OUT;n:type:ShaderForge.SFN_Append,id:5572,x:32877,y:32216,varname:node_5572,prsc:2|A-6871-OUT,B-714-OUT;n:type:ShaderForge.SFN_Vector1,id:714,x:32716,y:32277,varname:node_714,prsc:2,v1:1;n:type:ShaderForge.SFN_FragmentPosition,id:6680,x:32667,y:31754,varname:node_6680,prsc:2;n:type:ShaderForge.SFN_Normalize,id:9528,x:32841,y:31754,varname:node_9528,prsc:2|IN-4787-XYZ;n:type:ShaderForge.SFN_ObjectPosition,id:4787,x:33054,y:31707,varname:node_4787,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:1534,x:31777,y:32060,prsc:2,pt:False;n:type:ShaderForge.SFN_Add,id:2951,x:33450,y:32301,varname:node_2951,prsc:2|A-1484-RGB,B-6803-OUT;n:type:ShaderForge.SFN_Vector1,id:4234,x:32841,y:32090,varname:node_4234,prsc:2,v1:1;n:type:ShaderForge.SFN_Vector4Property,id:740,x:32638,y:33213,ptovrint:False,ptlb:Vertexoffset_Split,ptin:_Vertexoffset_Split,varname:node_740,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1,v2:1,v3:1,v4:0;n:type:ShaderForge.SFN_Add,id:8383,x:32358,y:31953,varname:node_8383,prsc:2|A-5940-OUT,B-4741-XYZ;n:type:ShaderForge.SFN_Vector4Property,id:4741,x:32117,y:31945,ptovrint:False,ptlb:Fresnel_adj,ptin:_Fresnel_adj,varname:node_4741,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0,v2:0,v3:0,v4:0;n:type:ShaderForge.SFN_Lerp,id:2732,x:31194,y:31723,varname:node_2732,prsc:2|A-5511-UVOUT,B-5301-UVOUT,T-7152-OUT;n:type:ShaderForge.SFN_Slider,id:7152,x:30768,y:31868,ptovrint:False,ptlb:UV2mask,ptin:_UV2mask,varname:node_7152,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Lerp,id:4877,x:31280,y:32057,varname:node_4877,prsc:2|A-5511-UVOUT,B-5301-UVOUT,T-7685-OUT;n:type:ShaderForge.SFN_Slider,id:7685,x:30756,y:32094,ptovrint:False,ptlb:UV2Offset,ptin:_UV2Offset,varname:node_7685,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_ComponentMask,id:9948,x:31946,y:32221,varname:node_9948,prsc:2,cc1:0,cc2:1,cc3:2,cc4:-1|IN-9996-OUT;n:type:ShaderForge.SFN_Abs,id:5440,x:32127,y:32310,varname:node_5440,prsc:2|IN-9948-B;n:type:ShaderForge.SFN_ComponentMask,id:3981,x:31946,y:32060,varname:node_3981,prsc:2,cc1:0,cc2:1,cc3:2,cc4:-1|IN-1534-OUT;n:type:ShaderForge.SFN_Normalize,id:1791,x:32520,y:31953,varname:node_1791,prsc:2|IN-8383-OUT;n:type:ShaderForge.SFN_Abs,id:974,x:32127,y:32143,varname:node_974,prsc:2|IN-3981-B;n:type:ShaderForge.SFN_Append,id:5940,x:32300,y:32095,varname:node_5940,prsc:2|A-3981-R,B-3981-G,C-974-OUT;n:type:ShaderForge.SFN_Append,id:7084,x:32337,y:32256,varname:node_7084,prsc:2|A-9948-R,B-9948-G,C-5440-OUT;proporder:3312-3180-7611-8674-6966-3022-2522-7741-2940-5367-8588-5872-4973-1809-9321-1484-740-4741-7152-7685;pass:END;sub:END;*/

Shader "AR/FX_FireSmoke" {
    Properties {
        _color_A ("color_A", Color) = (1,0.4758621,0,1)
        _color_B ("color_B", Color) = (0.75,0.8862069,1,1)
        _Mask_1 ("Mask_1", 2D) = "white" {}
        _Mask_Ctrl ("Mask_Ctrl", Range(0, 10)) = 1
        _Mask_Rotate ("Mask_Rotate", Range(0, 3)) = 0.5
        _Time_Scale ("Time_Scale", Range(-50, 50)) = 1
        _Light_add ("Light_add", Range(0, 100)) = 1
        _VertexOffset ("VertexOffset", Range(-10, 10)) = 0
        _OffsetMin ("OffsetMin", Range(0, 1)) = 0
        _OffsetMax ("OffsetMax", Range(0, 1)) = 1
        _Panner_Speed ("Panner_Speed", Vector) = (0,1,0,0)
        _Mask_Offset ("Mask_Offset", Vector) = (0,0,0,0)
        _Mask_Adj ("Mask_Adj", Vector) = (0,0,0,0)
        _Fresnel ("Fresnel", Range(0, 3)) = 0
        _Mask_Contrast ("Mask_Contrast", Range(1, 5)) = 1
        _Custom_Fresnel ("Custom_Fresnel", 2D) = "black" {}
        _Vertexoffset_Split ("Vertexoffset_Split", Vector) = (1,1,1,0)
        _Fresnel_adj ("Fresnel_adj", Vector) = (0,0,0,0)
        _UV2mask ("UV2mask", Range(0, 1)) = 1
        _UV2Offset ("UV2Offset", Range(0, 1)) = 0
    }
    SubShader {
        Tags {
            "Queue"="Transparent+1"
            "RenderType"="Transparent"
            "CanUseSpriteAtlas"="True"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _color_A;
            uniform float4 _color_B;
            uniform float _Mask_Ctrl;
            uniform sampler2D _Mask_1; uniform float4 _Mask_1_ST;
            uniform float _Mask_Rotate;
            uniform float _Time_Scale;
            uniform float _Light_add;
            uniform float _VertexOffset;
            uniform float _OffsetMin;
            uniform float _OffsetMax;
            uniform float4 _Mask_Offset;
            uniform float4 _Panner_Speed;
            uniform float4 _Mask_Adj;
            uniform float _Fresnel;
            uniform float _Mask_Contrast;
            uniform sampler2D _Custom_Fresnel; uniform float4 _Custom_Fresnel_ST;
            uniform float4 _Vertexoffset_Split;
            uniform float4 _Fresnel_adj;
            uniform float _UV2mask;
            uniform float _UV2Offset;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float node_3166 = (6.28318530718*_Mask_Rotate);
                float node_467_ang = node_3166;
                float node_467_spd = 1.0;
                float node_467_cos = cos(node_467_spd*node_467_ang);
                float node_467_sin = sin(node_467_spd*node_467_ang);
                float2 node_467_piv = float2(0.5,0.5);
                float4 node_3417 = _Time + _TimeEditor;
                float2 UV = ((float2(_Panner_Speed.r,_Panner_Speed.g)*_Time_Scale*node_3417.g)+lerp(o.uv1,o.uv0,_UV2Offset));
                float2 node_3644 = UV;
                float2 node_467 = (mul(node_3644-node_467_piv,float2x2( node_467_cos, -node_467_sin, node_467_sin, node_467_cos))+node_467_piv);
                float4 node_389 = tex2Dlod(_Mask_1,float4(TRANSFORM_TEX(node_467, _Mask_1),0.0,0));
                float node_3467 = (node_3166/3.0);
                float node_15_ang = node_3467;
                float node_15_spd = 1.0;
                float node_15_cos = cos(node_15_spd*node_15_ang);
                float node_15_sin = sin(node_15_spd*node_15_ang);
                float2 node_15_piv = float2(0.5,0.5);
                float2 node_15 = (mul(node_3644-node_15_piv,float2x2( node_15_cos, -node_15_sin, node_15_sin, node_15_cos))+node_15_piv);
                float4 node_2795 = tex2Dlod(_Mask_1,float4(TRANSFORM_TEX(node_15, _Mask_1),0.0,0));
                float node_1163_ang = (node_3467*2.0);
                float node_1163_spd = 1.0;
                float node_1163_cos = cos(node_1163_spd*node_1163_ang);
                float node_1163_sin = sin(node_1163_spd*node_1163_ang);
                float2 node_1163_piv = float2(0.5,0.5);
                float2 node_1163 = (mul(node_3644-node_1163_piv,float2x2( node_1163_cos, -node_1163_sin, node_1163_sin, node_1163_cos))+node_1163_piv);
                float4 node_8077 = tex2Dlod(_Mask_1,float4(TRANSFORM_TEX(node_1163, _Mask_1),0.0,0));
                float node_1586 = ((node_389.r+node_2795.r+node_8077.r)*0.3333333+0.0);
                v.vertex.xyz += (v.normal*_VertexOffset*min(max(node_1586,_OffsetMin),_OffsetMax)*_Vertexoffset_Split.rgb);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float node_3166 = (6.28318530718*_Mask_Rotate);
                float node_467_ang = node_3166;
                float node_467_spd = 1.0;
                float node_467_cos = cos(node_467_spd*node_467_ang);
                float node_467_sin = sin(node_467_spd*node_467_ang);
                float2 node_467_piv = float2(0.5,0.5);
                float4 node_3417 = _Time + _TimeEditor;
                float2 UV = ((float2(_Panner_Speed.r,_Panner_Speed.g)*_Time_Scale*node_3417.g)+lerp(i.uv1,i.uv0,_UV2Offset));
                float2 node_3644 = UV;
                float2 node_467 = (mul(node_3644-node_467_piv,float2x2( node_467_cos, -node_467_sin, node_467_sin, node_467_cos))+node_467_piv);
                float4 node_389 = tex2D(_Mask_1,TRANSFORM_TEX(node_467, _Mask_1));
                float node_3467 = (node_3166/3.0);
                float node_15_ang = node_3467;
                float node_15_spd = 1.0;
                float node_15_cos = cos(node_15_spd*node_15_ang);
                float node_15_sin = sin(node_15_spd*node_15_ang);
                float2 node_15_piv = float2(0.5,0.5);
                float2 node_15 = (mul(node_3644-node_15_piv,float2x2( node_15_cos, -node_15_sin, node_15_sin, node_15_cos))+node_15_piv);
                float4 node_2795 = tex2D(_Mask_1,TRANSFORM_TEX(node_15, _Mask_1));
                float node_1163_ang = (node_3467*2.0);
                float node_1163_spd = 1.0;
                float node_1163_cos = cos(node_1163_spd*node_1163_ang);
                float node_1163_sin = sin(node_1163_spd*node_1163_ang);
                float2 node_1163_piv = float2(0.5,0.5);
                float2 node_1163 = (mul(node_3644-node_1163_piv,float2x2( node_1163_cos, -node_1163_sin, node_1163_sin, node_1163_cos))+node_1163_piv);
                float4 node_8077 = tex2D(_Mask_1,TRANSFORM_TEX(node_1163, _Mask_1));
                float node_1586 = ((node_389.r+node_2795.r+node_8077.r)*0.3333333+0.0);
                float3 node_3981 = i.normalDir.rgb;
                float3 node_9948 = viewDirection.rgb;
                float node_5117 = max(0,dot(normalize((float3(node_3981.r,node_3981.g,abs(node_3981.b))+_Fresnel_adj.rgb)),float3(node_9948.r,node_9948.g,abs(node_9948.b))));
                float node_6871 = (node_5117*node_5117);
                float2 node_5572 = float2(node_6871,1.0);
                float4 _Custom_Fresnel_var = tex2D(_Custom_Fresnel,TRANSFORM_TEX(node_5572, _Custom_Fresnel));
                float3 emissive = (lerp(_color_B.rgb,_color_A.rgb,node_1586)*_Light_add*saturate((i.vertexColor.rgb*node_1586*_Mask_Ctrl*saturate(((1.0 - length((lerp((lerp(i.uv1,i.uv0,_UV2mask)*2.0+-1.0),float2(0,0),_Mask_Adj.rgb.rg)+float2(_Mask_Offset.r,_Mask_Offset.g))))*_Mask_Contrast))*i.vertexColor.a))*(_Custom_Fresnel_var.rgb+saturate(lerp(1.0,node_6871,_Fresnel))));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _Mask_1; uniform float4 _Mask_1_ST;
            uniform float _Mask_Rotate;
            uniform float _Time_Scale;
            uniform float _VertexOffset;
            uniform float _OffsetMin;
            uniform float _OffsetMax;
            uniform float4 _Panner_Speed;
            uniform float4 _Vertexoffset_Split;
            uniform float _UV2Offset;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
                float2 uv1 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float node_3166 = (6.28318530718*_Mask_Rotate);
                float node_467_ang = node_3166;
                float node_467_spd = 1.0;
                float node_467_cos = cos(node_467_spd*node_467_ang);
                float node_467_sin = sin(node_467_spd*node_467_ang);
                float2 node_467_piv = float2(0.5,0.5);
                float4 node_3417 = _Time + _TimeEditor;
                float2 UV = ((float2(_Panner_Speed.r,_Panner_Speed.g)*_Time_Scale*node_3417.g)+lerp(o.uv1,o.uv0,_UV2Offset));
                float2 node_3644 = UV;
                float2 node_467 = (mul(node_3644-node_467_piv,float2x2( node_467_cos, -node_467_sin, node_467_sin, node_467_cos))+node_467_piv);
                float4 node_389 = tex2Dlod(_Mask_1,float4(TRANSFORM_TEX(node_467, _Mask_1),0.0,0));
                float node_3467 = (node_3166/3.0);
                float node_15_ang = node_3467;
                float node_15_spd = 1.0;
                float node_15_cos = cos(node_15_spd*node_15_ang);
                float node_15_sin = sin(node_15_spd*node_15_ang);
                float2 node_15_piv = float2(0.5,0.5);
                float2 node_15 = (mul(node_3644-node_15_piv,float2x2( node_15_cos, -node_15_sin, node_15_sin, node_15_cos))+node_15_piv);
                float4 node_2795 = tex2Dlod(_Mask_1,float4(TRANSFORM_TEX(node_15, _Mask_1),0.0,0));
                float node_1163_ang = (node_3467*2.0);
                float node_1163_spd = 1.0;
                float node_1163_cos = cos(node_1163_spd*node_1163_ang);
                float node_1163_sin = sin(node_1163_spd*node_1163_ang);
                float2 node_1163_piv = float2(0.5,0.5);
                float2 node_1163 = (mul(node_3644-node_1163_piv,float2x2( node_1163_cos, -node_1163_sin, node_1163_sin, node_1163_cos))+node_1163_piv);
                float4 node_8077 = tex2Dlod(_Mask_1,float4(TRANSFORM_TEX(node_1163, _Mask_1),0.0,0));
                float node_1586 = ((node_389.r+node_2795.r+node_8077.r)*0.3333333+0.0);
                v.vertex.xyz += (v.normal*_VertexOffset*min(max(node_1586,_OffsetMin),_OffsetMax)*_Vertexoffset_Split.rgb);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
