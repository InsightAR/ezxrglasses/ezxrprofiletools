using System.Collections;
using System.Collections.Generic;
using EZXR.Glass.HandleController;
using UnityEngine;
using UnityEngine.UI;

public class HandleControllerDemo : MonoBehaviour
{
    public Text keyCodeText;
    public Text sliderValueText;
    public Slider sliderView;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        keyCodeText.text = string.Empty;

        if (HandleControllerManager.Instance.GetButton(HandType.Right, HandleKeyCode.Grid))
            keyCodeText.text = "检测到【侧握键】";
        if (HandleControllerManager.Instance.GetButton(HandType.Right, HandleKeyCode.Home))
            keyCodeText.text = "检测到【Home键】";
        if (HandleControllerManager.Instance.GetButton(HandType.Right, HandleKeyCode.Primary))
            keyCodeText.text = "检测到【A键】";
        if (HandleControllerManager.Instance.GetButton(HandType.Right, HandleKeyCode.Return))
            keyCodeText.text = "检测到【返回键】";
        if (HandleControllerManager.Instance.GetButton(HandType.Right, HandleKeyCode.Rocker))
            keyCodeText.text = "检测到【摇杆键】";
        if (HandleControllerManager.Instance.GetButton(HandType.Right, HandleKeyCode.Secondary))
            keyCodeText.text = "检测到【B键】";
        if (HandleControllerManager.Instance.GetButton(HandType.Right, HandleKeyCode.Trigger))
            keyCodeText.text = "检测到【扳机键】";

        var axis = HandleControllerManager.Instance.GetAxis2D(HandType.Right);
        if (axis != Vector2.zero)
        {
            keyCodeText.text += $"\n( {axis.x.ToString("F1")},  {axis.y.ToString("F1")} )";
        }

    }

    public void OnSiliderChanged()
    {
        sliderValueText.text = sliderView.value.ToString();
    }

    public void OnVibrateHandle()
    {
        HandleControllerManager.Instance.VibrateHandle(HandType.Right, 6, 500);
    }
}
