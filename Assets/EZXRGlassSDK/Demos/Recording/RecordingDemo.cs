using EZXR.Glass.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RecordingDemo : MonoBehaviour
{
    public SpatialButton_Normal buttonRecording;
    public SpatialButton_Normal buttonCapture;
    private bool isRecording = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("EZXRGlassSDK/Demos/SamplesMenu/SamplesMenu");
        }
    }
    public void OnRecordButton()
    {
        Debug.Log("-20002- RecordingDemo OnRecordButton current isRecording=" + isRecording);
        if (!isRecording)
        {
            RecordingManager.Instance.HandleRecordingOpen();
            isRecording = true;
        }
        else {
            RecordingManager.Instance.HandleRecordingClose();
            isRecording = false;
        }

        if (buttonRecording != null)
        {
            buttonRecording.Text = isRecording ? "Stop Recording" : "Start Recording";
        }
    }
    public void OnCaptureButton()
    {
        Debug.Log("-20002- RecordingDemo OnCaptureButton current isRecording=" + isRecording);
        RecordingManager.Instance.HandleScreenCapture();
    }
    public void OnExitButton()
    {
        SceneManager.LoadScene("EZXRGlassSDK/Demos/SamplesMenu/SamplesMenu");
    }
}
