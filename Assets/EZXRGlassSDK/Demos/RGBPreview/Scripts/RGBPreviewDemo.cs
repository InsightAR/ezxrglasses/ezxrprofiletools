using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using EZXR.Glass.UI;

public class RGBPreviewDemo : MonoBehaviour
{
    private bool isPreview = false;
    public GameObject previewObj = null;
    public SpatialButton_Normal buttonPreview;

    // Start is called before the first frame update
    void Start()
    {
        if (previewObj != null)
        {
            isPreview = previewObj.activeSelf;
            Debug.Log("-20002- RGBPreviewDemo isPreview=" + isPreview);
        }
        if (buttonPreview != null)
        {
            buttonPreview.Text = isPreview ? "StopPreview" : "StartPreview";
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("EZXRGlassSDK/Demos/SamplesMenu/SamplesMenu");
        }
    }

    public void OnPreviewButton() {
        Debug.Log("-20002- RGBPreviewDemo OnPreviewButton current isPreview=" + isPreview);
        
        if (previewObj != null) {
            previewObj.SetActive(!isPreview);
            isPreview = !isPreview;
        }
        if (buttonPreview != null)
        {
            buttonPreview.Text = isPreview ? "StopPreview" : "StartPreview";
        }
    }
    public void OnExitButton()
    {
        SceneManager.LoadScene("EZXRGlassSDK/Demos/SamplesMenu/SamplesMenu");
    }
}
