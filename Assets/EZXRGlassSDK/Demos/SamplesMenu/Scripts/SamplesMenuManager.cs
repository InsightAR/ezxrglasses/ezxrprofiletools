﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SamplesMenuManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    public void onMenuButton(GameObject button)
    {
        string scene = "EZXRGlassSDK/Demos/";
        bool isValidScene = true;
        bool isQuit = false;
        switch (button.name)
        {
            case "SpatialTracking":
                scene += "SpatialTracking/SpatialTracking";
                break;
            case "SpatialPositioning":
                scene += "SpatialPositioning/SpatialPositioning";
                break;
            case "HandTracking":
                scene += "HandTracking/HandTracking";
                break;
            case "Tracking3D":
                scene += "Tracking3D/Tracking3D";
                break;
            case "PlaneDetection":
                scene += "PlaneDetection/PlaneDetection";
                break;
            case "RGBPreview":
                scene += "RGBPreview/RGBPreview";
                break;
            case "Recording":
                scene += "Recording/Recording";
                break;
            case "ApiTest":
                scene += "APITestSample/ApiTestSample";
                break;
            case "Exit":
                isQuit = true;
                isValidScene = false;
                break;
            default:
                isValidScene = false;
                break;
        }
        if (isQuit) {
            Application.Quit();
        }
        if (isValidScene)
            SceneManager.LoadScene(scene);
    }
}
