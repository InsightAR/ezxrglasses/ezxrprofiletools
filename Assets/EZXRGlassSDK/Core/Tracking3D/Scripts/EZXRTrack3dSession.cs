﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class EZXRTrack3dSession
{

    public struct EZXRPose
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public float[] transform;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public float[] quaternion;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public float[] center;
        public double timestamp;
    }
    public struct EZXR3DObjectAnchor
    {
        public EZXRPose pose;
        [MarshalAs(UnmanagedType.I1)]
        public bool isValid;
    }

    private bool isCreated = false;
    private IntPtr imagebufferPtr = IntPtr.Zero;
    public void Create(string path)
    {
        if (isCreated)
            return;
        isCreated = NativeApi.nativeInitialize3dtracker(path);
    }
    public void Destroy()
    {
        if (!isCreated)
            return;
        NativeApi.nativeDestroy3dtracker();
        if (imagebufferPtr != IntPtr.Zero)
        {
            Marshal.FreeHGlobal(imagebufferPtr);
        }
    }

    public bool GettTrackedAnchorPose(ref Pose pose)
    {
        pose = Pose.identity;
        if (!isCreated)
        {
            pose = Pose.identity;
            return false;
        }
        EZXR3DObjectAnchor anchor = NativeApi.nativeGetTrackedAnchor();
        if (anchor.isValid == true)
        {
            pose.position = new Vector3(anchor.pose.center[0], anchor.pose.center[1], anchor.pose.center[2]);
            pose.rotation = new Quaternion(anchor.pose.quaternion[0], anchor.pose.quaternion[1], anchor.pose.quaternion[2], anchor.pose.quaternion[3]);
        }
        return anchor.isValid;
    }

    public void UpdateImage(IntPtr buffer, double timestamp, int width, int height, int format)
    {
        if (!isCreated)
        {
            return;
        }
        if (buffer == null)
            return;
        NativeApi.nativeOnImageAvailable(buffer.ToInt64(), timestamp, width, height, format);
    }
    public void UpdateImage(byte[] buffer, double timestamp, int width, int height, int format)
    {
        if (!isCreated)
        {
            return;
        }
        if (buffer == null)
            return;
        if (buffer.Length == 0)
            return;
        if (imagebufferPtr == IntPtr.Zero)
        {
            imagebufferPtr = Marshal.AllocHGlobal(buffer.Length);
        }
        Marshal.Copy(buffer, 0, imagebufferPtr, buffer.Length);

        //Debug.Log("-10001- OnUpdateFrame length=" + buffer.Length + ",size=" + width + "," + height + " | " + imagebufferPtr.ToInt64() + "," + timestamp);
        NativeApi.nativeOnImageAvailable(imagebufferPtr.ToInt64(), timestamp, width, height, format);
    }

    public void SetCameraIntrics(float[] intrics)
    {
        if (!isCreated)
        {
            return;
        }
        NativeApi.nativeSetImageIntrinsic(intrics);
    }

    public void SetPoseFromHeadToLocCam(float[] pose)
    {
        if (!isCreated)
        {
            return;
        }
        NativeApi.nativeSetRTFromHeadToLocCam(pose);
    }

    public void UpdateHeadPose(float[] pose, double timestamp)
    {
        if (!isCreated)
        {
            return;
        }
        NativeApi.nativeOnHeadPoseUpdated(pose, timestamp);
    }
    public void UpdateHeadPose(Pose pose, double timestamp_sec) {
        if (!isCreated)
        {
            return;
        }
        if (pose == null)
            return;
        float[] posearray = new float[7] {
                pose.position.x,pose.position.y,pose.position.z,
                    pose.rotation.x,pose.rotation.y,pose.rotation.z,pose.rotation.w
            };
        NativeApi.nativeOnHeadPoseUpdated(posearray, timestamp_sec);
    }

    private partial struct NativeApi
    {
#if UNITY_EDITOR
        public static bool nativeInitialize3dtracker(string assetpath) {
            return false;
        }

        public static void nativeDestroy3dtracker() { 
        }

        public static EZXR3DObjectAnchor nativeGetTrackedAnchor() {
            EZXR3DObjectAnchor temp = new EZXR3DObjectAnchor();
            temp.isValid = false;
            return temp;
        }

        public static void nativeOnImageAvailable(Int64 ptr, double timestamp, int width, int height, int format) { 
        }

        public static void nativeSetImageIntrinsic([Out, In][MarshalAs(UnmanagedType.LPArray, SizeConst = 8)] float[] ptr) { 
        }

        public static void nativeSetRTFromHeadToLocCam([Out, In][MarshalAs(UnmanagedType.LPArray, SizeConst = 16)] float[] ptr) { 
        }
        public static bool nativeOnHeadPoseUpdated([MarshalAs(UnmanagedType.LPArray, SizeConst = 7)] float[] ptr, double timestamp) {
            return false;
        }
#else
        private const string nativelibraryname = "ezglass3dtrack";

        [DllImport(nativelibraryname)]
        public static extern bool nativeInitialize3dtracker(string assetpath);

        [DllImport(nativelibraryname)]
        public static extern void nativeDestroy3dtracker();

        [DllImport(nativelibraryname)]
        public static extern EZXR3DObjectAnchor nativeGetTrackedAnchor();

        [DllImport(nativelibraryname)]
        public static extern void nativeOnImageAvailable(Int64 ptr, double timestamp, int width, int height, int format);

        [DllImport(nativelibraryname)]
        public static extern void nativeSetImageIntrinsic([Out, In][MarshalAs(UnmanagedType.LPArray, SizeConst = 8)] float[] ptr);

        [DllImport(nativelibraryname)]
        public static extern void nativeSetRTFromHeadToLocCam([Out, In][MarshalAs(UnmanagedType.LPArray, SizeConst = 16)] float[] ptr);

        [DllImport(nativelibraryname)]
        public static extern bool nativeOnHeadPoseUpdated([MarshalAs(UnmanagedType.LPArray, SizeConst = 7)] float[] ptr, double timestamp);
#endif
    }
}
