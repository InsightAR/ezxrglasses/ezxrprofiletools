﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EZXR.Glass.Common
{
    [ScriptExecutionOrder(-30)]
    public partial class Utilities : MonoBehaviour
    {
        private static Utilities_Android instance_Android;
        public static Utilities_Android Android
        {
            get
            {
                if (instance_Android == null)
                {
                    instance_Android = FindObjectOfType<Utilities_Android>();
                    if (instance_Android == null)
                    {
                        GameObject go = new GameObject("Utilities_Android");
                        instance_Android = go.AddComponent<Utilities_Android>();
                    }
                }
                return instance_Android;
            }
        }

        public class Utilities_Android : MonoBehaviour
        {
            /// <summary>
            /// 如果要打开的是当前Activity，则直接通过此回调传递参数
            /// </summary>
            public Action<string[]> SameActivityDirectCallback;
            /// <summary>
            /// 跳转到外部应用
            /// </summary>
            public Action GotoExternalApp;

            /// <summary>
            /// 获得应用启动时OnCreate中的Intent传参（>>>>>>>>>>>>>>>>>>>>>>注意：需要EZXRUnityPlayerActivity.java<<<<<<<<<<<<<<<<<<<<<<<<<<）
            /// </summary>
            /// <param name="paramName"></param>
            /// <returns></returns>
            public string GetActivityCreateParams(string paramName)
            {
                // 获取启动当前Activity时传递的Intent对象
                AndroidJavaClass EZXRUnityPlayerActivity = new AndroidJavaClass("com.unity3d.player.EZXRUnityPlayerActivity");
                AndroidJavaObject intent = EZXRUnityPlayerActivity.GetStatic<AndroidJavaObject>("oncreateIntent");
                bool hasExtra = intent.Call<bool>("hasExtra", paramName);
                Debug.Log("AndroidUtils--> GetActivityStartParams: hasExtra: " + hasExtra);
                Debug.Log("AndroidUtils--> getStringExtra: " + intent.Call<string>("getStringExtra", paramName));
                if (hasExtra)
                {
                    //AndroidJavaObject extras = intent.Call<AndroidJavaObject>("getExtras");
                    return intent.Call<string>("getStringExtra", paramName);
                }
                return "";
            }

            /// <summary>
            /// 得到此Android应用被外部打开时传入的参数
            /// </summary>
            /// <param name="paramName"></param>
            /// <returns></returns>
            public string GetActivityStartParams(string paramName)
            {
                AndroidJavaObject intent;
                AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
                intent = /*intent ??*/ currentActivity.Call<AndroidJavaObject>("getIntent");
                bool hasExtra = intent.Call<bool>("hasExtra", paramName);
                Debug.Log("AndroidUtils--> GetActivityStartParams: hasExtra: " + hasExtra);
                Debug.Log("AndroidUtils--> getStringExtra: " + intent.Call<string>("getStringExtra", paramName));
                if (hasExtra)
                {
                    //AndroidJavaObject extras = intent.Call<AndroidJavaObject>("getExtras");
                    return intent.Call<string>("getStringExtra", paramName);
                }
                return "";
            }

            public void StartActivityDelayed(string pkgName, float delay = 1, params string[] extraData)
            {
                if (Application.identifier == pkgName)
                {
                    if (SameActivityDirectCallback != null)
                    {
                        SameActivityDirectCallback(extraData);
                    }
                }
                else
                {
                    StartCoroutine(Coroutine_StartActivityDelayed(pkgName, delay, extraData));
                }
            }

            IEnumerator Coroutine_StartActivityDelayed(string pkgName, float delay = 1, params string[] extraData)
            {
                yield return new WaitForSeconds(delay);

                StartActivity(pkgName, extraData);
            }

            /// <summary>
            /// 打开指定包名的Activity
            /// </summary>
            /// <param name="pkgName">包名</param>
            /// <param name="extraData">要传递的参数，需要成对传入，按一个key后跟一个value的形式</param>
            public void StartActivity(string pkgName, params string[] extraData)
            {
                if (Application.platform == RuntimePlatform.Android)
                {
                    AndroidJavaClass jcPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                    AndroidJavaObject joActivity = jcPlayer.GetStatic<AndroidJavaObject>("currentActivity");
                    AndroidJavaObject joPackageManager = joActivity.Call<AndroidJavaObject>("getPackageManager");
                    AndroidJavaObject joIntent = joPackageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", pkgName);
                    if (null != joIntent)
                    {
                        AndroidJavaObject joNIntent = joIntent.Call<AndroidJavaObject>("addFlags", joIntent.GetStatic<int>("FLAG_ACTIVITY_REORDER_TO_FRONT"));
                        if (extraData.Length > 0 && extraData.Length % 2 == 0)
                        {
                            for (int i = 0; i < extraData.Length; i += 2)
                            {
                                joIntent.Call<AndroidJavaObject>("putExtra", extraData[i], extraData[i + 1]);
                            }
                        }
                        joActivity.Call("startActivity", joNIntent);
                        joIntent.Dispose();

                        if (GotoExternalApp != null)
                        {
                            GotoExternalApp.Invoke();
                        }
                    }
                    else
                    {
                        string msg = "Package <" + pkgName + "> not exist on device.";
                        Debug.Log(msg);

                        //ShowToast(msg);
                    }
                }
            }

            /// <summary>
            /// 显示Toast通知
            /// </summary>
            /// <param name="content"></param>
            public void ShowToast(string content)
            {
                Debug.Log("Toast: " + content);

                AndroidJavaClass jcPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject joActivity = jcPlayer.GetStatic<AndroidJavaObject>("currentActivity");
                AndroidJavaClass jT = new AndroidJavaClass("android.widget.Toast");
                AndroidJavaObject jMsg = new AndroidJavaObject("java.lang.String", content);
                AndroidJavaObject jC = joActivity.Call<AndroidJavaObject>("getApplicationContext");
                int length = jT.GetStatic<int>("LENGTH_SHORT");
                AndroidJavaObject toast = jT.CallStatic<AndroidJavaObject>("makeText", jC, jMsg, length);
                toast.Call("show");
            }

            public void SendIntent(string action, string packageName, params string[] extraData)
            {
                if (Application.isEditor)
                {
                    return;
                }

                AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
                AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject unityActivity = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");

                intentObject.Call<AndroidJavaObject>("setAction", action);
                if (extraData.Length > 0 && extraData.Length % 2 == 0)
                {
                    for (int i = 0; i < extraData.Length; i += 2)
                    {
                        Debug.Log("SendIntent: " + extraData[i] + ", " + extraData[i + 1]);
                        intentObject.Call<AndroidJavaObject>("putExtra", extraData[i], extraData[i + 1]);
                    }
                }

                if (string.IsNullOrEmpty(packageName))
                {
                    AndroidJavaClass activityUtils = new AndroidJavaClass("com.unity3d.player.ActivityUtils");
                    string[] getForegroundActivity = activityUtils.CallStatic<string[]>("getForegroundActivity", unityActivity);

                    // 输出字符串数组
                    Debug.Log("getForegroundActivity: " + string.Join(", ", getForegroundActivity));

                    packageName = getForegroundActivity[0];
                }

                if (!string.IsNullOrEmpty(packageName))
                {
                    // 设置要发送广播的包名
                    intentObject.Call<AndroidJavaObject>("setPackage", packageName);

                    string permissionName = "com.ezxr.glass.permission.SYSTEM_MENU_NOTIFICATION";
                    // 检查是否已经声明了自定义权限
                    AndroidJavaObject packageManagerObject = unityActivity.Call<AndroidJavaObject>("getPackageManager");
                    int permissionStatus = packageManagerObject.Call<int>("checkPermission", permissionName, unityActivity.Call<string>("getPackageName"));
                    if (permissionStatus == 0)
                    {
                        Debug.Log("MyBroadcastReceiver, permission get");
                        unityActivity.Call("sendBroadcast", intentObject, permissionName);
                    }
                    else
                    {
                        // 没有声明权限，不发送广播
                        Debug.LogError("MyBroadcastReceiver, No permission to send broadcast");
                    }
                }
            }

            public void KillTopMostApp()
            {
                AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
                AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject unityActivity = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");

                AndroidJavaClass activityUtils = new AndroidJavaClass("com.unity3d.player.ActivityUtils");
                Debug.Log("getForegroundActivity: 开始");
                string[] getForegroundActivity = activityUtils.CallStatic<string[]>("getForegroundActivity", unityActivity);

                // 输出字符串数组
                Debug.Log("getForegroundActivity: " + string.Join(", ", getForegroundActivity));

                string packageName = getForegroundActivity[0];
                if (packageName != "com.ezxr.glass.arlauncher")
                {
                    AndroidJavaClass activityManagerClass = new AndroidJavaClass("android.app.ActivityManager");
                    AndroidJavaObject activityManager = unityActivity.Call<AndroidJavaObject>("getSystemService", "activity");
                    activityManager.Call("forceStopPackage", packageName);
                }
            }
        }
    }
}