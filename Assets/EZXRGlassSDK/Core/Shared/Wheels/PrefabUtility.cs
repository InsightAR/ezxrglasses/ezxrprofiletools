﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace EZXR.Glass.Common
{
    public class PrefabUtility : MonoBehaviour
    {
#if UNITY_EDITOR
        public static Object InstantiatePrefabWithUndoAndSelection(Object assetComponentOrGameObject, Transform parent = null)
        {
            GameObject obj = UnityEditor.PrefabUtility.InstantiatePrefab(assetComponentOrGameObject, parent) as GameObject;
            Undo.RegisterCreatedObjectUndo(obj, assetComponentOrGameObject.name);
            obj.transform.SetAsLastSibling();
            List<Object> temp = new List<Object>();
            //temp.AddRange(Selection.objects);
            temp.Add(obj);
            Selection.objects = temp.ToArray();
            return obj;
        }

        public static Object InstantiateInstanceWithUndoAndSelection(GameObject assetComponentOrGameObject, Transform parent = null)
        {
            GameObject obj = Instantiate(assetComponentOrGameObject, parent);
            Undo.RegisterCreatedObjectUndo(obj, assetComponentOrGameObject.name);
            obj.transform.SetAsLastSibling();
            obj.name = obj.name.Remove(obj.name.Length - 7);
            List<Object> temp = new List<Object>();
            //temp.AddRange(Selection.objects);
            temp.Add(obj);
            Selection.objects = temp.ToArray();
            return obj;
        }

        public static void UnpackPrefabInstance(GameObject instanceRoot)
        {
            if (UnityEditor.PrefabUtility.IsPartOfPrefabInstance(instanceRoot))
            {
                UnityEditor.PrefabUtility.UnpackPrefabInstance(instanceRoot, PrefabUnpackMode.Completely, InteractionMode.AutomatedAction);
            }
        }
#endif
    }
}