﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Wheels.Unity
{
    public static class Vector3Extensions
    {
        public static Vector3 TransformPosition(this Pose p, Vector3 localPosition)
        {
            Matrix4x4 rotationMatrix = Matrix4x4.TRS(Vector3.zero, p.rotation, Vector3.one);
            return rotationMatrix.MultiplyPoint3x4(localPosition) + p.position;
        }

        public static Vector3 TransformDirection(this Pose p, Vector3 localDirection)
        {
            Matrix4x4 rotationMatrix = Matrix4x4.TRS(Vector3.zero, p.rotation, Vector3.one);
            return rotationMatrix.MultiplyPoint3x4(localDirection);
        }
    }
}