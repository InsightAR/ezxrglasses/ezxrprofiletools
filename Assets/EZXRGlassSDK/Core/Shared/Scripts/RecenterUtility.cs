﻿using EZXR.Glass.SixDof;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wheels.Unity;

namespace EZXR.Glass.Common
{
    public static class RecenterUtility
    {
        /// <summary>
        /// 重置到相机前方
        /// </summary>
        /// <param name="self"></param>
        /// <param name="distance">出现在相机前方的距离</param>
        public static void Recenter(this Transform self, float distance)
        {
            Vector3 newPosition = HMDPoseTracker.Instance.Head.transform.position + Vector3.ProjectOnPlane(HMDPoseTracker.Instance.Head.transform.forward, Vector3.up).normalized * distance;

            SpatialAnchor spatialAnchor = self.GetComponent<SpatialAnchor>();
            if (spatialAnchor != null)
            {
                spatialAnchor.SetPosition(newPosition);
            }
            else
            {
                self.position = newPosition;
            }

            self.LookAt(HMDPoseTracker.Instance.Head.transform);
            //self.LookAtWithAxisLock(HMDPoseTracker.Instance.Head.transform, Vector3.forward);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="offset">相对于Head的</param>
        /// <param name="lockAxis">锁定Y轴朝上，Z轴朝向目标</param>
        public static void Recenter(this Transform self, Vector3 offset, bool lockAxis = true)
        {

            Vector3 forward = Vector3.ProjectOnPlane(HMDPoseTracker.Instance.Head.transform.forward, Vector3.up).normalized;
            Vector3 up = Vector3.up;
            Vector3 right = Vector3.Cross(forward, up);
            Vector3 newPosition = HMDPoseTracker.Instance.Head.transform.position + forward * offset.z + Vector3.up * offset.y + right * offset.x;

            SpatialAnchor spatialAnchor = self.GetComponent<SpatialAnchor>();
            if (spatialAnchor != null)
            {
                spatialAnchor.SetPosition(newPosition);
            }
            else
            {
                self.position = newPosition;
            }

            //if (lockAxis)
            //{
            //    self.LookAtWithAxisLock(HMDPoseTracker.Instance.Head.transform, Vector3.forward);
            //}
            //else
            {
                self.LookAt(HMDPoseTracker.Instance.Head.transform);
            }
        }
    }
}