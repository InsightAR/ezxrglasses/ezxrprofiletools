﻿using EZXR.Glass.SixDof;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace EZXR.Glass.Common
{
    public class NormalRGBCameraDevice
    {
        private bool hasCameraOpened = false;
        private int mCameraID = 0;//0:rgb,1:left grey FishEye,2:ToF
        public void Open()
        {
            if (!Application.isEditor)
            {
                NativeApi.nativeOpenCameraWithHandle(GetHashCode(), mCameraID);
            }
            hasCameraOpened = true;
        }

        public void Close()
        {
            hasCameraOpened = false;
            if (!Application.isEditor)
            {
                NativeApi.nativeCloseCameraWithHandle(GetHashCode(), mCameraID);
            }
        }

        public bool IsStarted()
        {
            if (!Application.isEditor)
            {
                return NativeApi.nativeIsCameraStarted(mCameraID);
            }
            return false;
        }
        public bool getCurrentImage(ref EZVIOInputImage image, float[] intrinsic, EZVIOImageFormat specifiedFormat)
        {
            if (hasCameraOpened)
            {
                return NativeApi.nativeGetCurrentImageWithFormat(mCameraID, ref image, intrinsic, (int)specifiedFormat);
            }
            return false;
        }

        public bool getCurrentImage(ref EZVIOInputImage image, float[] intrinsic)
        {
            if (hasCameraOpened)
            {
                return NativeApi.nativeGetCurrentImage(mCameraID, ref image, intrinsic);
            }
            return false;
        }

        /// <summary>
        /// 获取RGB相机是否在剧烈运动
        /// </summary>
        /// <returns></returns>
        public bool isCameraMotionViolently()
        {
            if (hasCameraOpened)
            {
                return NativeApi.nativeIsCameraMotionViolently(mCameraID);
            }
            return false;
        }
        public IntPtr GetDrawRGBVideoFrameFunc()
        {
            return NativeApi.GetDrawRGBVideoFrameFunc();
        }
        public Int32 GetRGBVideoTexture()
        {
            return NativeApi.GetRGBVideoTexture();
        }
        public void DrawRGBVideoFrame()
        {
            NativeApi.DrawRGBVideoFrame();
        }
        public void DrawRGBVideoFrame(IntPtr texturePtr)
        {
            NativeApi.DrawRGBVideoFrameWithPtr(texturePtr);
        }

        [Obsolete]
        public bool getCurrentRGBImage(ref EZVIOInputImage image, float[] intrinsic)
        {
            if (hasCameraOpened)
            {
                return NativeApi.nativeGetCurrentImage(mCameraID, ref image, intrinsic);
            }
            return false;
        }
        [Obsolete]
        public bool getCameraIntrics(float[] inttrics)
        {
            if (hasCameraOpened)
            {
                return NativeApi.nativeGetRGBCameraIntrics(inttrics);
            }
            return false;
        }
        private partial struct NativeApi
        {
            
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void nativeOpenCameraWithHandle(int holderHandle, int CameraID);
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void nativeCloseCameraWithHandle(int holderHandle, int CameraID);
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern bool nativeIsCameraStarted(int CameraID);

            [DllImport(NativeConsts.NativeLibrary)]
            public static extern bool nativeGetCurrentImage(int cameraId, ref EZVIOInputImage image, float[] intrinsic);

            [DllImport(NativeConsts.NativeLibrary)]
            public static extern bool nativeGetCurrentImageWithFormat(int cameraId, ref EZVIOInputImage image, float[] intrinsic, int outputFormat);

            [DllImport(NativeConsts.NativeLibrary)]
            public static extern bool nativeIsCameraMotionViolently(int cameraId);

            [DllImport(NativeConsts.NativeLibrary)]
            public static extern IntPtr GetDrawRGBVideoFrameFunc();
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern Int32 GetRGBVideoTexture();

            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void DrawRGBVideoFrame();

            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void DrawRGBVideoFrameWithPtr(IntPtr texturePtr);

            [DllImport(NativeConsts.NativeLibrary)]
            [Obsolete]
            public static extern void nativeOpenRGBCamera();

            [DllImport(NativeConsts.NativeLibrary)]
            [Obsolete]
            public static extern void nativeCloseRGBCamera();

            [DllImport(NativeConsts.NativeLibrary)]
            [Obsolete]
            public static extern bool nativeGetRGBCameraIntrics([Out, In][MarshalAs(UnmanagedType.LPArray, SizeConst = 4)] float[] ptr);

            [DllImport(NativeConsts.NativeLibrary)]
            [Obsolete]
            public static extern bool nativeGetCurrentImage(ref EZVIOInputImage image, float[] intrinsic);

            [DllImport(NativeConsts.NativeLibrary)]
            [Obsolete]
            public static extern bool nativeGetCurrentRGBImage(ref EZVIOInputImage image);
            [Obsolete]
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void nativeOpenCamera(int CameraID);
            [Obsolete]
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void nativeCloseCamera(int CameraID);
        }
    }
}
