﻿using EZXR.Glass.Common;
using EZXR.Glass.SixDof;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wheels.Unity;

namespace EZXR.Glass.Hand
{
    public enum GestureType
    {
        Unknown = -1,
        OpenHand = 0,
        Grab,
        Pinch,
        Point,
        Victory,
        Call,
    }

    public enum HandJointType
    {
        Thumb_0,
        Thumb_1,
        Thumb_2,
        /// <summary>
        /// 拇指尖
        /// </summary>
        Thumb_3,
        /// <summary>
        /// 食指根节点
        /// </summary>
        Index_1,
        Index_2,
        Index_3,
        /// <summary>
        /// 食指尖
        /// </summary>
        Index_4,
        /// <summary>
        /// 中指根节点
        /// </summary>
        Middle_1,
        Middle_2,
        Middle_3,
        /// <summary>
        /// 中指指尖
        /// </summary>
        Middle_4,
        Ring_1,
        Ring_2,
        Ring_3,
        /// <summary>
        /// 无名指指尖
        /// </summary>
        Ring_4,
        Pinky_0,
        /// <summary>
        /// 小指根节点
        /// </summary>
        Pinky_1,
        Pinky_2,
        Pinky_3,
        /// <summary>
        /// 小指指尖
        /// </summary>
        Pinky_4,
        /// <summary>
        /// 掌心点
        /// </summary>
        Palm,
        /// <summary>
        /// 手腕横切线，靠近拇指根节点的点
        /// </summary>
        Wrist_Thumb,
        /// <summary>
        /// 手腕横切线，靠近小指根节点的点
        /// </summary>
        Wrist_Pinky,
        /// <summary>
        /// 手腕横切线，中点
        /// </summary>
        Wrist_Middle,
    }


    /// <summary>
    /// 包含单个手的各种状态信息，是个基础信息类，供其他类进行状态获取和协同
    /// </summary>
    [ScriptExecutionOrder(-52)]
    public class HandInfo : HandInfoBase
    {
        /// <summary>
        /// 手的所有关节等物体的父节点
        /// </summary>
        public Transform root
        {
            get
            {
                return transform;
            }
        }

        /// <summary>
        /// 初始化，设置当前HandInfo所属的手是左手还是右手
        /// </summary>
        /// <param name="handType">0为左手，1为右手</param>
        public override void Init(HandType handType)
        {
            base.Init(handType);

            //设定肩部点，用于射线计算
            shoulderPoint = new Vector3(handType == HandType.Left ? -0.15f : 0.15f, -0.15f, -0.1f);

            palm = new GameObject("Palm").transform;
            palm.parent = transform;
        }

        public void UpdateHandInfoData(NativeSwapManager.HandTrackingData handTrackingData)
        {
            this.handTrackingData = handTrackingData;
            if (handTrackingData.handJointData != null)
            {
                Pose pose;
                for (int i = 0; i < handTrackingData.handJointData.Length; i++)
                {
                    NativeSwapManager.Mat4f mat4 = handTrackingData.handJointData[i].handJointPose;
                    Matrix4x4 m = new Matrix4x4();
                    m.SetColumn(0, new Vector4(mat4.col0.x, mat4.col0.y, mat4.col0.z, mat4.col0.w));
                    m.SetColumn(1, new Vector4(mat4.col1.x, mat4.col1.y, mat4.col1.z, mat4.col1.w));
                    m.SetColumn(2, new Vector4(mat4.col2.x, mat4.col2.y, mat4.col2.z, mat4.col2.w));
                    m.SetColumn(3, new Vector4(mat4.col3.x, mat4.col3.y, mat4.col3.z, mat4.col3.w));
                    if (!m.ValidTRS())
                    {
                        return;
                    }
                    pose.rotation = ARBodyManager.Instance == null ? m.rotation : (ARBodyManager.Instance.transform.rotation * m.rotation);
                    Vector3 pos = m.GetColumn(3);
                    pose.position = ARBodyManager.Instance == null ? pos : (ARBodyManager.Instance.transform.TransformPoint(pos));
                    HandJointType handJointType = (HandJointType)i;
                    if (jointsPose.ContainsKey(handJointType))
                    {
                        jointsPose[handJointType] = pose;
                    }
                    else
                    {
                        jointsPose.Add(handJointType, pose);
                    }
                }
            }
        }

        protected override void Update()
        {
            if (Exist)
            {
                UpdatePinching();

                palm.position = GetJointData(HandJointType.Palm).position;
                palm.rotation = GetJointData(HandJointType.Palm).rotation;

                //掌心面向的方向
                palmNormal = GetJointData(HandJointType.Palm).TransformDirection(Vector3.forward);

                //手掌指尖朝向
                palmDirection = GetJointData(HandJointType.Palm).TransformDirection(Vector3.up);

                //射线方向
                rayDirection = (GetJointData(HandJointType.Index_1).position - ARHandManager.head.TransformPoint(shoulderPoint)).normalized;

                //射线起点和终点
                rayPoint_Start = GetJointData(HandJointType.Index_1).position + rayDirection * rayStartDistance;

                ////用于双手操作，得到左手加滤波之后的掌心点位置
                //NativeSwapManager.Point3 temp = new NativeSwapManager.Point3(rayPoint_Start);
                //NativeSwapManager.filterPoint(ref temp, transform.GetInstanceID());
                //Vector3 tarPos = new Vector3(temp.x, temp.y, temp.z);
                //rayPoint_Start_Left = tarPos;

                //从算法得到的预估的射线终点位置
                //rayPoint_End = new Vector3(ray[1].x, Main.ray[1].y, ray[1].z);
            }
        }

        protected override void UpdatePinching()
        {
            bool curPinchState = handTrackingData.isTracked && (handTrackingData.gestureType == GestureType.Pinch || handTrackingData.gestureType == GestureType.Grab);
            if (isPinching)
            {
                if (curPinchState)
                {
                    startPinch = false;
                }
                else
                {
                    endPinch = true;
                }
            }
            else
            {
                if (curPinchState)
                {
                    startPinch = true;
                }
                else
                {
                    endPinch = false;
                }
            }
            isPinching = curPinchState;
        }

        #region 手交互能力开关
        /// <summary>
        /// 设置或获得当前手的显示状态（仅仅影响显示，并不影响交互）
        /// </summary>
        public bool Visibility
        {
            get
            {
                return GetComponent<HandsVisualization>().Visibility;
            }
            set
            {
                GetComponent<HandsVisualization>().Visibility = value;
            }
        }
        /// <summary>
        /// 设置或获得远距离射线交互状态
        /// </summary>
        public bool RaycastInteraction
        {
            get
            {
                return GetComponent<HandRaycast>().enabled;
            }
            set
            {
                GetComponent<HandRaycast>().enabled = value;
            }
        }

        /// <summary>
        /// 设置或获得近距离交互状态
        /// </summary>
        public bool TouchInteraction
        {
            get
            {
                return GetComponent<HandTouch>().enabled;
            }
            set
            {
                GetComponent<HandTouch>().enabled = value;
            }
        }

        /// <summary>
        /// 设置或获得手部的物理碰撞交互状态
        /// </summary>
        public bool PhysicsInteraction
        {
            get
            {
                return GetComponent<HandsCollision>().enabled;
            }
            set
            {
                GetComponent<HandsCollision>().enabled = value;
            }
        }
        #endregion

        #region 单手信息
        /// <summary>
        /// 当前手节点是否在视野内
        /// </summary>
        /// <param name="jointType"></param>
        /// <returns></returns>
        public bool InView(HandJointType jointType)
        {
            if (Application.isEditor)
            {
                Vector3 viewportPoint = HMDPoseTracker.Instance.centerCamera.WorldToViewportPoint(GetJointData(jointType).position);
                Debug.Log(viewportPoint.ToString("0.##"));
                return viewportPoint.x > 0 && viewportPoint.x < 1 && viewportPoint.y > 0 && viewportPoint.y < 1 && viewportPoint.z > 0;
            }
            else
            {
                Vector3 viewportPoint_L = HMDPoseTracker.Instance.leftCamera.WorldToViewportPoint(GetJointData(jointType).position);
                Vector3 viewportPoint_R = HMDPoseTracker.Instance.rightCamera.WorldToViewportPoint(GetJointData(jointType).position);
                return (viewportPoint_L.x > 0 && viewportPoint_L.x < 1 && viewportPoint_L.y > 0 && viewportPoint_L.y < 1 && viewportPoint_L.z > 0) || (viewportPoint_R.x > 0 && viewportPoint_R.x < 1 && viewportPoint_R.y > 0 && viewportPoint_R.y < 1 && viewportPoint_R.z > 0);
            }
        }
        /// <summary>
        /// 当前眼镜视野中是否存在这个手
        /// </summary>
        public override bool Exist
        {
            get
            {
                return handTrackingData.isTracked;
            }
        }

        /// <summary>
        /// 手掌正在朝向头部，如果手掌朝向和手掌与头的连线的夹角小于angle则认为手掌是朝向了头部
        /// </summary>
        /// <param name="angle">默认90</param>
        /// <returns></returns>
        public override bool isPalmFacingHead(float angle = 90)
        {
            Pose palm = GetJointData(HandJointType.Palm);
            Vector3 palmNormal = palm.TransformDirection(Vector3.forward * 0.1f);
            Vector3 palmToHead = HMDPoseTracker.Instance.Head.position - palm.position;
            //求palmNormal和palmToHead的夹角
            float includedAngle = Vector3.Angle(palmNormal, palmToHead);

            if (includedAngle < angle)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 从算法得到的此手的原始关节点数据，单位为mm
        /// </summary>
        NativeSwapManager.HandTrackingData handTrackingData;

        /// <summary>
        /// 自行定义的一个肩部点，相对于头部坐标系的
        /// </summary>
        Vector3 shoulderPoint;

        /// <summary>
        /// 手的动作
        /// </summary>
        public GestureType gestureType
        {
            get
            {
                return handTrackingData.gestureType;
            }
        }

        /// <summary>
        /// 所有手指关节点的Pose
        /// </summary>
        public Dictionary<HandJointType, Pose> jointsPose = new Dictionary<HandJointType, Pose>();
        /// <summary>
        /// 手掌面向的方向
        /// </summary>
        public Vector3 palmNormal;
        /// <summary>
        /// 手掌指尖朝向（从手腕点到中指和无名指根节点中间点的朝向，世界坐标系）
        /// </summary>
        public Vector3 palmDirection;
        public Transform palm;


        public bool JointDataExist(HandJointType handJointType)
        {
            return jointsPose.ContainsKey(handJointType);
        }

        public Pose GetJointData(HandJointType handJointType)
        {
            if (jointsPose.ContainsKey(handJointType))
            {
                return jointsPose[handJointType];
            }
            return Pose.identity;
        }

        public Pose GetJointData(int handJointTypeID)
        {
            return GetJointData((HandJointType)handJointTypeID);
        }
        #endregion

        #region 双手信息
        /// <summary>
        /// 正在执行双手射线抓取操作
        /// </summary>
        public static bool isTwoHandRayGrabbing;
        /// <summary>
        /// 正在执行双手近距离抓取操作
        /// </summary>
        public static bool isTwoHandCloseGrabbing;
        /// <summary>
        /// 用于双手，左手当前捏住物体拖动的目标坐标点
        /// </summary>
        public static Vector3 targetObjectPosition_Left;
        /// <summary>
        /// 用于双手，右手当前捏住物体拖动的目标坐标点
        /// </summary>
        public static Vector3 targetObjectPosition_Right;
        #endregion

        #region 当前预触碰的物体信息
        /// <summary>
        /// 在手的近距离交互预判区内是否存在物体，只是在手的触发区域内但是还没有碰到食指指尖（主要用于避免在手即将触摸到旋转握把的时候会出现射线射到另外一个握把或者缩放角）
        /// </summary>
        public bool preCloseContacting;
        /// <summary>
        /// 手部近距离交互预判区（TriggerForFarNearSwitch中定义区域大小，此区域内则认为用户要近距离抓取）内存在的距离最近的物体
        /// </summary>
        Transform preCloseContactingTarget;
        /// <summary>
        /// 手部近距离交互预判区内存在的距离最近的物体
        /// </summary>
        public Transform PreCloseContactingTarget
        {
            get
            {
                return preCloseContactingTarget;
            }
            set
            {
                //if (preCloseContactingTarget != value)
                {
                    //记录上一帧食指指尖的近距离交互预判区内存在的距离最近的物体
                    Last_PreCloseContactingTarget = preCloseContactingTarget;

                    preCloseContactingTarget = value;
                    preCloseContactingTarget_Renderer = (preCloseContactingTarget == null ? null : preCloseContactingTarget.GetComponent<Renderer>());
                    if (preCloseContactingTarget_Renderer != null)
                    {
                        if (preCloseContactingTarget_PropertyBlock == null)
                        {
                            preCloseContactingTarget_PropertyBlock = new MaterialPropertyBlock();
                        }
                        preCloseContactingTarget_Renderer.GetPropertyBlock(preCloseContactingTarget_PropertyBlock);
                    }

                    if (preCloseContactingTarget == null)
                    {
                        preCloseContacting = false;
                    }
                    else
                    {
                        preCloseContacting = true;
                    }
                }
            }
        }
        public Renderer preCloseContactingTarget_Renderer;
        public MaterialPropertyBlock preCloseContactingTarget_PropertyBlock;

        /// <summary>
        /// 上一帧手部近距离交互预判区内存在的距离最近的物体
        /// </summary>
        Transform last_PreCloseContactingTarget;
        /// <summary>
        /// 上一帧手部近距离交互预判区内存在的距离最近的物体
        /// </summary>
        public Transform Last_PreCloseContactingTarget
        {
            get
            {
                return last_PreCloseContactingTarget;
            }
            set
            {
                if (last_PreCloseContactingTarget != value)
                {
                    last_PreCloseContactingTarget = value;
                    last_PreCloseContactingTarget_Renderer = (last_PreCloseContactingTarget == null ? null : last_PreCloseContactingTarget.GetComponent<Renderer>());
                    if (last_PreCloseContactingTarget_Renderer != null)
                    {
                        if (last_PreCloseContactingTarget_PropertyBlock == null)
                        {
                            last_PreCloseContactingTarget_PropertyBlock = new MaterialPropertyBlock();
                        }
                        last_PreCloseContactingTarget_Renderer.GetPropertyBlock(last_PreCloseContactingTarget_PropertyBlock);
                    }
                }
            }
        }
        public Renderer last_PreCloseContactingTarget_Renderer;
        public MaterialPropertyBlock last_PreCloseContactingTarget_PropertyBlock;
        #endregion

        #region 当前正在触碰的物体信息
        /// <summary>
        /// 指示手是否正在近距离触碰某个物体（被拇指和食指指尖的连线穿过）
        /// </summary>
        public bool isCloseContacting;

        Collider curCloseContactingTarget;
        /// <summary>
        /// 当前正在被近距离触碰的物体
        /// </summary>
        public Collider CurCloseContactingTarget
        {
            get
            {
                return curCloseContactingTarget;
            }
            set
            {
                if (curCloseContactingTarget != null && value != null)
                {
                    if (curCloseContactingTarget != value)
                    {
                        SpatialObject.PerformOnHandTriggerExit(curCloseContactingTarget);
                        SpatialObject.PerformOnHandTriggerEnter(curCloseContactingTarget);
                    }
                    else
                    {
                        SpatialObject.PerformOnHandTriggerStay(value);
                    }
                }
                else if (curCloseContactingTarget == null && value != null)
                {
                    SpatialObject.PerformOnHandTriggerEnter(value);
                }
                else if (curCloseContactingTarget != null && value == null)
                {
                    SpatialObject.PerformOnHandTriggerExit(curCloseContactingTarget);
                }

                curCloseContactingTarget = value;
                if (curCloseContactingTarget == null)
                {
                    isCloseContacting = false;
                }
                else
                {
                    isCloseContacting = true;
                }
            }
        }

        /// <summary>
        /// 指示手是否正在近距离触碰某个物体或者射线是否正射在某个物体上
        /// </summary>
        public bool isContacting
        {
            get
            {
                return isCloseContacting | isRayContacting;
            }
        }
        /// <summary>
        /// 当前正在被触碰的物体（无论射线还是近距离）
        /// </summary>
        public Collider CurContactingTarget
        {
            get
            {
                if (CurCloseContactingTarget == null)
                {
                    if (curRayContactingTarget != null)
                    {
                        return curRayContactingTarget;
                    }
                }
                else
                {
                    return CurCloseContactingTarget;
                }
                return null;
            }
        }
        #endregion

        #region 当前正在抓取的物体的信息
        /// <summary>
        /// 指示手正在通过近距离抓取的方式拖拽物体
        /// </summary>
        private bool isCloseGrabbing;
        /// <summary>
        /// 指示手正在通过近距离抓取的方式拖拽物体
        /// </summary>
        public bool IsCloseGrabbing
        {
            get
            {
                return isCloseGrabbing;
            }
            set
            {
                isCloseGrabbing = value;
                if (isCloseGrabbing)
                {
                    if (((handType == HandType.Left && ARHandManager.rightHand.IsCloseGrabbing) || (handType == HandType.Right && ARHandManager.leftHand.IsCloseGrabbing)) && ARHandManager.leftHand.CurCloseGrabbingTarget == ARHandManager.rightHand.CurCloseGrabbingTarget)
                    {
                        isTwoHandCloseGrabbing = true;
                    }
                }
                else
                {
                    isTwoHandCloseGrabbing = false;
                }
            }
        }
        #endregion

    }
}