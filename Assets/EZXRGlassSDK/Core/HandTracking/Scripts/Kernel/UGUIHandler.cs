﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using EZXR.Glass.SixDof;
using EZXR.Glass.Hand;
using EZXR.Glass.HandleController;

namespace EZXR.Glass.UI
{
    public class UGUIHandler : MonoBehaviour
    {
        #region singleton
        private static UGUIHandler instance;
        public static UGUIHandler Instance
        {
            get
            {
                return instance;
            }
        }
        #endregion
        
        Camera eventCamera;
        //Canvas[] canvas;

        private void Awake()
        {
            instance = this;
        }

        // Start is called before the first frame update
        void Start()
        {
            GetComponent<EventSystem>().pixelDragThreshold = 80;

            eventCamera = Application.isEditor ? HMDPoseTracker.Instance.centerCamera : HMDPoseTracker.Instance.leftCamera;
        }

        // Update is called once per frame
        void Update()
        {
            if (HandInputModule.controllerType == ControllerType.Gestures)
            {
                if (Hand.ARHandManager.leftHand.Exist && !Hand.ARHandManager.leftHand.isPalmFacingHead())
                {
                    HandInputModule.touches[0].position = eventCamera.WorldToScreenPoint(Hand.ARHandManager.leftHand.rayPoint_End.position);
                }
                else
                {
                    HandInputModule.touches[0].position = Vector2.zero;
                }

                if (Hand.ARHandManager.rightHand.Exist && !Hand.ARHandManager.rightHand.isPalmFacingHead())
                {
                    HandInputModule.touches[1].position = eventCamera.WorldToScreenPoint(Hand.ARHandManager.rightHand.rayPoint_End.position);
                }
                else
                {
                    HandInputModule.touches[1].position = Vector2.zero;
                }
            }

            if (HandInputModule.controllerType == ControllerType.Handles)
            {
                if (HandleControllerManager.leftHand.Exist)
                {
                    HandInputModule.touches[0].position = eventCamera.WorldToScreenPoint(HandleControllerManager.leftHand.rayPoint_End.position);
                }
                else
                {
                    HandInputModule.touches[0].position = Vector2.zero;
                }

                if (HandleControllerManager.rightHand.Exist)
                {
                    HandInputModule.touches[1].position = eventCamera.WorldToScreenPoint(HandleControllerManager.rightHand.rayPoint_End.position);
                }
                else
                {
                    HandInputModule.touches[1].position = Vector2.zero;
                }
            }

            //if (Input.GetKeyDown(KeyCode.Q))
            //    Debug.Log($"HandControllerSession, ctrlTouch: ({HandInputModule.touches[1].position.x}, {HandInputModule.touches[1].position.y})");

        }

        //public void ConfigAllCanvas()
        //{
        //    eventCamera = Application.isEditor ? HMDPoseTracker.Instance.centerCamera : HMDPoseTracker.Instance.leftCamera;

        //    canvas = FindObjectsOfType<Canvas>();
        //    foreach (Canvas item in canvas)
        //    {
        //        if (item.transform.parent != null && item.transform.parent.GetComponent<PhoneUIRenderer>() != null)
        //        {
        //            continue;
        //        }
        //        if (item.gameObject.GetComponent<BoxCollider>() == null)
        //        {
        //            item.gameObject.AddComponent<BoxCollider>().size = item.GetComponent<RectTransform>().sizeDelta;
        //        }
        //        item.worldCamera = eventCamera;
        //    }
        //}
    }
}