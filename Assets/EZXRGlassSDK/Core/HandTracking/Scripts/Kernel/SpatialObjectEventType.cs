﻿namespace EZXR.Glass.Hand
{
    public enum SpatialObjectEventType
    {
        /// <summary>
        /// 当手近距离触碰到或者远距离射线碰到物体时触发
        /// </summary>
        OnHandEnter,
        /// <summary>
        /// 当手近距离或者远距离射线持续触碰物体时会持续触发
        /// </summary>
        OnHandStay,
        /// <summary>
        /// 当手结束近距离或者远距离射线触碰时触发
        /// </summary>
        OnHandExit,
        /// <summary>
        /// 当手近距离或者远距离射线抓取物体时触发
        /// </summary>
        OnHandGrab,
        /// <summary>
        /// 当手近距离或者远距离射线松开物体时触发
        /// </summary>
        OnHandRelease,
        /// <summary>
        /// 当手近距离触碰到物体时触发
        /// </summary>
        OnHandTriggerEnter,
        /// <summary>
        /// 当手近距离持续触碰物体时会持续触发
        /// </summary>
        OnHandTriggerStay,
        /// <summary>
        /// 当手结束近距离触碰时触发
        /// </summary>
        OnHandTriggerExit,
        /// <summary>
        /// 当手近距离抓取物体时触发
        /// </summary>
        OnHandTriggerGrab,
        /// <summary>
        /// 当手近距离松开物体时触发
        /// </summary>
        OnHandTriggerRelease,
        /// <summary>
        /// 当手远距离射线碰到物体时触发
        /// </summary>
        OnHandRayEnter,
        /// <summary>
        /// 当手远距离射线持续触碰物体时会持续触发
        /// </summary>
        OnHandRayStay,
        /// <summary>
        /// 当手结束远距离射线触碰时触发
        /// </summary>
        OnHandRayExit,
        /// <summary>
        /// 当手远距离射线抓取物体时触发
        /// </summary>
        OnHandRayGrab,
        /// <summary>
        /// 当手远距离射线松开物体时触发
        /// </summary>
        OnHandRayRelease,
    }
} 