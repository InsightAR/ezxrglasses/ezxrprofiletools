﻿using EZXR.Glass.SixDof;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasHandler : MonoBehaviour
{
    public bool canvasCollider = true;

    private void Awake()
    {
        Camera eventCamera = Application.isEditor ? HMDPoseTracker.Instance.centerCamera : HMDPoseTracker.Instance.leftCamera;

        Canvas canvas = GetComponent<Canvas>();
        if (canvasCollider)
        {
            if (canvas.gameObject.GetComponent<BoxCollider>() == null)
            {
                canvas.gameObject.AddComponent<BoxCollider>().size = canvas.GetComponent<RectTransform>().sizeDelta;
            }
        }
        canvas.worldCamera = eventCamera;
    }
}
