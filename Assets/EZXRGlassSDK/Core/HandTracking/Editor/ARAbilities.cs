﻿using EZXR.Glass.Hand;
using EZXR.Glass.HandleController;
using UnityEditor;
using UnityEngine;

namespace EZXR.Glass.Common
{
    public partial class ARAbilities : MonoBehaviour
    {
        #region HandTracking
        [MenuItem("GameObject/XR Abilities/HandTracking", false, 20)]
        public static void EnableHandTracking()
        {
            if (FindObjectOfType<ARHandManager>() == null)
            {
                string filePath = "Assets/EZXRGlassSDK/Core/HandTracking/Prefabs/HandRig.prefab";
                if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
                {
                    filePath = AssetDatabase.GUIDToAssetPath("6f7860851f83dcf4cb1798a8fac3f550");
                }
                Common.PrefabUtility.InstantiatePrefabWithUndoAndSelection(AssetDatabase.LoadAssetAtPath<GameObject>(filePath));
            }

            if (FindObjectOfType<HandleControllerManager>() != null)
            {
                DestroyImmediate(FindObjectOfType<HandleControllerManager>().gameObject);
                Debug.LogWarning("Destroy HandleController gameObject");
            }
        }
        #endregion
    }
}