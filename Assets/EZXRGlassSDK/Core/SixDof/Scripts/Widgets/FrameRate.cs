using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FrameRate : MonoBehaviour
{
    public TextMeshPro text;
    float fps;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        fps = 1 / Time.smoothDeltaTime;
        text.text = "FPS:" + fps.ToString("0.0");
    }
}
