﻿namespace EZXR.Glass.SixDof
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using System.Runtime.InteropServices;
    using System;
    using System.IO;

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct EZXRRendererHotConfig
    {

        [MarshalAs(UnmanagedType.Bool)]
        public bool warp_1;

        public EZXRRendererHotConfig(bool isWarp)
        {
            this.warp_1 = isWarp;
        }
    }

    public class EZXRNativeRenderring
    {

        public IntPtr FrameInfoPtr;
        private IntPtr m_FrameTexturesPtr;
        private EZXRRendererHotConfig m_cacheCfg;

        public EZXRNativeRenderring()
        {
            int sizeOfFrameInfo = Marshal.SizeOf(typeof(FrameInfo));
            FrameInfoPtr = Marshal.AllocHGlobal(sizeOfFrameInfo);
        }

        ~EZXRNativeRenderring()
        {
            Marshal.FreeHGlobal(FrameInfoPtr);
        }


        /// <summary>
        /// start ezxr mtp renderer. return value aligns with @startEZXRRenderer
        /// </summary>
        /// <returns>
        ///  0: succ
        /// -1: displayer not showed up
        /// -2: environment issues
        /// -3: ezxr mtp renderer is already started up
        /// </returns>
        public int Start()
        {
            return NativeAPI.startEZXRRenderer();
        }

        public void DoExtendedRenderring()
        {
            FrameInfo frameinfo = (FrameInfo)Marshal.PtrToStructure(FrameInfoPtr, typeof(FrameInfo));
            NativeAPI.setFrameInfo(FrameInfoPtr);
        }

        public void WriteFrameData(FrameInfo frame)
        {
            Marshal.StructureToPtr(frame, FrameInfoPtr, true);
        }

        public void Stop()
        {
            NativeAPI.stopEZXRRenderer();
        }

        public void WriteRendererHotConfig(EZXRRendererHotConfig cfg)
        {
            m_cacheCfg = cfg;
        }

        public void HotUpdateRenderer()
        {
            NativeAPI.hotConfigEZXRRenderer(m_cacheCfg);
        }

        private partial struct NativeAPI
        {

#if UNITY_EDITOR
            public static int startEZXRRenderer() { return -1; }

            //public static void setFrameInfo(ref FrameInfo frameInfoPtr) { }
            public static void setFrameInfo(IntPtr frameInfoPtr) { }

            public static void stopEZXRRenderer() { }

            public static void hotConfigEZXRRenderer(EZXRRendererHotConfig cfg) { }

#elif UNITY_ANDROID
        [DllImport(NativeConsts.NativeLibrary)]
        public static extern int startEZXRRenderer();

        [DllImport(NativeConsts.NativeLibrary)]
        //public static extern void setFrameInfo(ref FrameInfo frameInfoPtr);
        public static extern void setFrameInfo(IntPtr frameInfoPtr);

        [DllImport(NativeConsts.NativeLibrary)]
        public static extern void stopEZXRRenderer();

        [DllImport(NativeConsts.NativeLibrary)]
        public static extern void hotConfigEZXRRenderer(EZXRRendererHotConfig cfg);
#endif
        }
    }

}