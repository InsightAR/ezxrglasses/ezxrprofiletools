﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using UnityEngine.Android;
using AOT;
using EZXR.Glass.Common;
using System.Runtime.InteropServices;

namespace EZXR.Glass.SixDof
{
    [ScriptExecutionOrder(-70)]
    public class SessionManager : MonoBehaviour
    {
        #region 单例
        private static SessionManager instance;
        public static SessionManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<SessionManager>();
                    if (instance == null)
                    {
                        GameObject go = new GameObject("SessionManager");
                        instance = go.AddComponent<SessionManager>();
                    }
                }
                return instance;
            }
        }
        #endregion // 单例

        //#region 单例标志符
        //// by @xuninghao: unity session manager单例模式 结合 native session的生命周期，
        ////                需要确保非维护单例对象在 awake and destroy中，是不允许操作到native session的；
        ////                当前只保护到private void DestroySession()/StartSession()
        ////                后续可在所有Session操作中推广；
        //private static bool instanceExsits = false;
        //private static int instanceID;
        //#endregion // 单例标志符

        //#region 刷新单例
        //// beta接口，慎用；
        //// by @xuninghao: 当在同一个进程里手动destroy掉该component时，为确保下一次可以重新刷新单例；
        ////                调用时机在显式执行DestroyImmediatey之后；但同时存在风险，即其他脚本调用instance需要做保护判断
        ////                实际上是按照c++的写法多次一举了，更好的方式其实直接用intance自己比就可以了，不需要引入这两个变量，当前只是临时使用。
        //public static void resetInstance()
        //{
        //    instance = null;
        //    instanceExsits = false;
        //    instanceID = -1;
        //}
        //#endregion

        #region params
        //private bool m_isRequestCamera;

        private RenderMode m_renderMode = RenderMode.Bino;
        public RenderMode RenderMode
        {
            get
            {
                return m_renderMode;
            }
        }

        public bool IsInited
        {
            get
            {
                return NativeTracking.GetIsARSessionInited();
            }
        }


        private ARConfig m_ARInitConfig = ARConfig.DefaultConfig;
        public ARConfig ARInitConfig
        {
            get
            {
                return m_ARInitConfig;
            }
        }


        #endregion

        #region unity functions
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);

            Application.targetFrameRate = 60;

            Debug.Log("=============Unity Log===============   SessionManager:" + this.GetInstanceID() + " -- Awake");

            instance = this;
            Debug.Log("=============Unity Log===============   SessionManager:" + this.GetInstanceID() + " -- Awake" + " exsits instance=null, so set \"this\" as new instance");

            ARFrame.OnAwake();

            //config = ARConfig.DefaultConfig;
            ////TODO:only for Test
            //config.HandsFindingMode = HandsFindingMode.Disable;
            //config.MarkerFindingMode = MarkerFindingMode.Enable;
        }

        private void Start()
        {
            Debug.Log("=============Unity Log===============   SessionManager:" + this.GetInstanceID() + " -- Start");
            if (CheckAndRequestPermissions())
            {
#if EZXRCS
                if (!Application.isEditor)
                {
                    NativeTracking.StartArServer();
                }
#endif
                StartSession();
            }
        }

        private void Update()
        {
            // NativeTracking.GetIsARSessionInited() 必须调用
            if (!IsInited)
            {
                Debug.Log("=============Unity Log===============   SessionManager:" + this.GetInstanceID() + " -- Update NativeTracking GetIsARSessionInited: not initilized");
                return;
            }

            //Debug.Log("xnh M2P: sessionmanager Update, BEFORE get propagate pose, systime: " + NativeTracking.GetSystemTime() / 1e9);
            // update vio
            ARFrame.OnUpdate();
            //Debug.Log("xnh M2P: sessionmanager Update, AFTER get propagate pose, systime: " + NativeTracking.GetSystemTime() / 1e9 + " VIORESULT: " + ARFrame.OriginVIOResult.timestamp);

            //update gesture
            //ARGesture.OnFixedUpdate();
        }


        private void OnApplicationPause(bool pause)
        {
            Debug.Log("=============Unity Log===============   SessionManager:" + this.GetInstanceID() + " -- OnApplicationPause " + pause);

            if (pause)
            {
                //DisableSession();
            }
            else
            {
                if (CheckAndRequestPermissions())
                {
                    StartSession();
                }
                //ResumeSession();
            }
        }

        private void OnDisable()
        {
            Debug.Log("=============Unity Log===============   SessionManager:" + this.GetInstanceID() + " -- OnDisable");
            DisableSession();
        }

        private void OnDestroy()
        {
            Debug.Log("=============Unity Log===============   SessionManager:" + this.GetInstanceID() + " -- OnDestroy");
            DestroySession();
            //instance = null;
        }
        #endregion

        #region custom functions
        private bool CheckAndRequestPermissions()
        {
            Debug.Log("=============Unity Log===============   SessionManager:" + this.GetInstanceID() + " -- RequestCamera");
            bool writePermissionGranted = Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite);
            bool cameraPermissionGranted = Permission.HasUserAuthorizedPermission(Permission.Camera);
            if (!writePermissionGranted)
            {
                Permission.RequestUserPermission(Permission.ExternalStorageWrite);
                return false;
            }
            if (!cameraPermissionGranted)
            {
                Permission.RequestUserPermission(Permission.Camera);
                return false;
            }
            if (!GlassDevicePermissionHelper.IsGlassDevicePermissionAllGranted())
            {
                GlassDevicePermissionHelper.RequestGlassDevicePermission(OnGlassPermssionChange);
                return false;
            }
            return true;
        }

        [MonoPInvokeCallback(typeof(GlassDevicePermissionHelper.GlassDevicePermissionListener))]
        private void OnGlassPermssionChange(int usbVenderId, int usbProductId, bool isGranted)
        {
            Debug.Log("=============Unity Log=============== SessionManager:" + this.GetInstanceID() + " -- OnGlassPermssionChange " + usbVenderId + "," + usbProductId + "," + isGranted);
            if (GlassDevicePermissionHelper.IsGlassDevicePermissionAllGranted())
            {
                StartSession();
            }
            else
            {
                CheckAndRequestPermissions();
            }
        }

        private void StartSession()
        {
            Debug.Log("=============Unity Log=============== SessionManager:" + this.GetInstanceID() + " -- StartSession");
            if (this.GetInstanceID() != instance.GetInstanceID())
            {
                //Debug.Log("=============Unity Log=============== SessionManager:" + this.GetInstanceID() + " -- StartSession"
                //    + " no authority to do further steps cuz singleton id is: " + instanceID);
                return;
            }
            if (!IsInited)
            {
                NativeTracking.StartARSession(m_ARInitConfig);
            }
        }

        private void DisableSession()
        {
            Debug.Log("=============Unity Log=============== SessionManager:" + this.GetInstanceID() + " -- DisableSession");
            if (!IsInited) return;
            if (this.GetInstanceID() != instance.GetInstanceID())
            {
                //Debug.Log("=============Unity Log=============== SessionManager:" + this.GetInstanceID() + " -- DestroySession"
                //    + " no authority to do further steps cuz singleton id is: " + instanceID);
                return;
            }
#if EZXRCS
            NativeTracking.pauseMtp();
#endif
        }

        public void ResumeSession()
        {
            Debug.Log("=============Unity Log=============== SessionManager:" + this.GetInstanceID() + " -- ResumeSession");
            if (!IsInited) return;
            if (this.GetInstanceID() != instance.GetInstanceID())
            {
                //Debug.Log("=============Unity Log=============== SessionManager:" + this.GetInstanceID() + " -- DestroySession"
                //    + " no authority to do further steps cuz singleton id is: " + instanceID);
                return;
            }
            NativeTracking.ConfigureARSession(ARInitConfig);
            NativeTracking.ResumeARSession();
        }

        private void DestroySession()
        {
            Debug.Log("=============Unity Log=============== SessionManager:" + this.GetInstanceID() + " -- DestroySession");

            if (this.GetInstanceID() != instance.GetInstanceID())
            {
                //Debug.Log("=============Unity Log=============== SessionManager:" + this.GetInstanceID() + " -- DestroySession"
                //    + " no authority to do further steps cuz singleton id is: " + instanceID);
                return;
            }

            if (!NativeTracking.GetIsARSessionInited()) return;

            NativeTracking.StopARSession();
        }
        #endregion
    }
}