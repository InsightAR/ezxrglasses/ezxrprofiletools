using EZXR.Glass.SixDof;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wheels.Unity;

public class SpatialAnchor : MonoBehaviour
{
    public enum Mode
    {
        SixDof,
        ThreeDof,
        ZeroDof
    }
    public Mode mode;
    Vector3 reletivePosition;

    public void SetPosition(Vector3 newPosition)
    {
        transform.position = newPosition;
        Refresh();
    }

    void Refresh()
    {
        switch (mode)
        {
            case Mode.ThreeDof:
                reletivePosition = transform.position - HMDPoseTracker.Instance.Head.position;
                break;
            case Mode.ZeroDof:
                transform.ActAsChild(HMDPoseTracker.Instance.Head);
                break;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Refresh();
    }

    // Update is called once per frame
    void Update()
    {
        if (mode == Mode.ThreeDof)
        {
            transform.position = HMDPoseTracker.Instance.Head.position + reletivePosition;
        }
    }
}
