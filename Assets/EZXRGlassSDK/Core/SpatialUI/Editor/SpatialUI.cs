﻿using UnityEditor;
using UnityEngine;

namespace EZXR.Glass.UI
{
    public class SpatialUI : MonoBehaviour
    {
        public static void InstantiatePrefab(GameObject prefab)
        {
            bool parentExists = false;
            Transform parent = null;
            Transform subElements = null;
            if (Selection.activeTransform != null)
            {
                parentExists = true;
                parent = Selection.activeTransform;
            }
            GameObject obj = Common.PrefabUtility.InstantiateInstanceWithUndoAndSelection(prefab, parent) as GameObject;

            SpatialUIElement spatialUIElement = obj.GetComponent<SpatialUIElement>();

            if (spatialUIElement != null)
            {
                //所有新创建的UI使用SpatialUIController中设定的unitsPerPixel
                spatialUIElement.curUnitsPerPixel = SpatialUIController.Instance.unitsPerPixel;
            }

            if (parentExists)
            {
                obj.transform.localPosition = Vector3.zero;

                if (parent.GetComponent<SpatialUIElement>() != null)
                {
                    //如果在SpatialUIElement物体上右键创建UI的话，会直接创建到子物体SubElements下
                    subElements = parent.Find("SubElements");
                    if (subElements == null)
                    {
                        subElements = new GameObject("SubElements").transform;
                        subElements.parent = parent;
                        subElements.localEulerAngles = Vector3.zero;
                    }
                    obj.transform.parent = subElements;

                    SpatialUIElement spatialUIElement_Parent = parent.GetComponent<SpatialUIElement>();
                    //避免子级的SpatialUI显示反向
                    obj.transform.localEulerAngles = Vector3.zero;

                    if (spatialUIElement != null)
                    {
                        //避免子级的SpatialUI被父级盖住
                        spatialUIElement.sortingOrder = spatialUIElement_Parent.sortingOrder + 1;
                    }
                }
            }
        }

        #region SpatialButton
        [MenuItem("GameObject/XR UI(Beta)/Button/Normal", false, 20)]
        public static void AddSpatialButtonNormal()
        {
            string filePath = "Assets/EZXRGlassSDK/Core/SpatialUI/Resources/SpatialButton(Normal).prefab";
            if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
            {
                filePath = AssetDatabase.GUIDToAssetPath("06c2f8e21ae9b374daebeac328167671");
            }
            InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(filePath));
        }
        [MenuItem("GameObject/XR UI(Beta)/Button/RoundThick", false, 20)]
        public static void AddSpatialButton()
        {
            string filePath = "Assets/EZXRGlassSDK/Core/SpatialUI/Resources/SpatialButton(RoundThick).prefab";
            if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
            {
                filePath = AssetDatabase.GUIDToAssetPath("bb9542d808f4aa045ad968ed06c6851e");
            }
            InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(filePath));
        }
        #endregion

        #region SpatialImage
        [MenuItem("GameObject/XR UI(Beta)/Image", false, 20)]
        public static void AddSpatialImage()
        {
            string filePath = "Assets/EZXRGlassSDK/Core/SpatialUI/Resources/SpatialImage.prefab";
            if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
            {
                filePath = AssetDatabase.GUIDToAssetPath("383887ce0ab85e84d9bacacf6f8496f2");
            }
            InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(filePath));
        }
        #endregion

        #region SpatialPanel
        [MenuItem("GameObject/XR UI(Beta)/Panel", false, 20)]
        public static void AddSpatialPanel()
        {
            string filePath = "Assets/EZXRGlassSDK/Core/SpatialUI/Resources/SpatialPanel.prefab";
            if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
            {
                filePath = AssetDatabase.GUIDToAssetPath("229f883ea3b83b046bcbfa049550b0a8");
            }
            InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(filePath));
        }
        #endregion

        #region SpatialTrigger
        [MenuItem("GameObject/XR UI(Beta)/Trigger", false, 20)]
        public static void AddSpatialTrigger()
        {
            string filePath = "Assets/EZXRGlassSDK/Core/SpatialUI/Resources/SpatialTrigger.prefab";
            if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
            {
                filePath = AssetDatabase.GUIDToAssetPath("44279d0548e666040a3e3fe81d68bccd");
            }
            InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(filePath));
        }
        #endregion

        #region SpatialUIController
        [MenuItem("GameObject/XR UI(Beta)/UIController", false, 20)]
        public static void AddSpatialUIController()
        {
            string filePath = "Assets/EZXRGlassSDK/Core/SpatialUI/Resources/SpatialUIController.prefab";
            if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
            {
                filePath = AssetDatabase.GUIDToAssetPath("13c81cfdca600f04fac9a6a4f117013f");
            }
            Common.PrefabUtility.InstantiatePrefabWithUndoAndSelection(AssetDatabase.LoadAssetAtPath<GameObject>(filePath));
        }
        #endregion

        #region SpatialUIEventSystem
        [MenuItem("GameObject/XR UI(Beta)/UIEventSystem", false, 20)]
        public static void AddSpatialUIEventSystem()
        {
            string filePath = "Assets/EZXRGlassSDK/Core/SpatialUI/Resources/SpatialUIEventSystem.prefab";
            if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
            {
                filePath = AssetDatabase.GUIDToAssetPath("2facc6fb6d026004486467c9b4ef2d63");
            }
            Common.PrefabUtility.InstantiatePrefabWithUndoAndSelection(AssetDatabase.LoadAssetAtPath<GameObject>(filePath));
        }
        #endregion
    }
}