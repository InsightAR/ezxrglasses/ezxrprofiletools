﻿using EZXR.Glass.SixDof;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;

namespace EZXR.Glass.UI
{
    [ExecuteInEditMode]
    public class CurvedLayoutGroup : LayoutGroup
    {
        /// <summary>
        /// 扇形区域的半径
        /// </summary>
        public float radius = 3;
        float prevRadius;
        /// <summary>
        /// 每个物体之间的间隔角度
        /// </summary>
        public float angle = 60f;
        float prevAngle;
        List<SpatialUIElement> children = new List<SpatialUIElement>();
        float prevChildCount;
        Vector3 prevSize;

        public override void OnValidate()
        {
            if (!Application.isPlaying)
            {
                base.OnValidate();

                GetSpatialUIElement();

                if (radius != prevRadius || angle != prevAngle || spatialUIElement.size != prevSize || (children != null && children.Count != prevChildCount))
                {
                    prevRadius = radius;
                    prevAngle = angle;
                    prevSize = spatialUIElement.size;
                    prevChildCount = children.Count;

                    needReLayout = true;
                }
            }
        }

        private void OnDisable()
        {
            children = null;
        }

        // Update is called once per frame
        void Update()
        {
            if (startLayout)
            {
                if (needReLayout)
                {
                    needReLayout = false;

                    if (transform.childCount > 0)
                    {
                        children = new List<SpatialUIElement>();
                        for (int i = 0; i < transform.childCount; i++)
                        {
                            SpatialUIElement temp = transform.GetChild(i).GetComponent<SpatialUIElement>();
                            if (temp != null && temp.gameObject.activeInHierarchy)
                            {
                                children.Add(temp);
                            }
                        }

                        prevRadius = radius;
                        prevAngle = angle;

                        // 从相机的位置和朝向获取扇形的起始角度
                        Quaternion sectorRotation = Quaternion.LookRotation(Vector3.ProjectOnPlane(HMDPoseTracker.Instance.leftCamera.transform.forward, Vector3.up), Vector3.up);
                        Quaternion startRotation = Quaternion.AngleAxis(-angle * (children.Count - 1) / 2f, Vector3.up) * sectorRotation;

                        for (int i = 0; i < children.Count; i++)
                        {
                            if (children[i] != null)
                            {
                                // 计算当前物体的旋转
                                Quaternion currentRotation = startRotation * Quaternion.AngleAxis(angle * i, Vector3.up);

                                // 计算当前物体的位置
                                children[i].transform.position = currentRotation * (Vector3.forward * radius) + HMDPoseTracker.Instance.leftCamera.transform.position;

                                children[i].transform.LookAt(HMDPoseTracker.Instance.leftCamera.transform);
                            }
                        }
                    }
                    else
                    {
                        children = null;
                    }
                }
            }
        }
    }
}