﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace EZXR.Glass.UI
{
    [CustomEditor(typeof(SpatialButton_NormalZoomBG))]
    [CanEditMultipleObjects]
    public class SpatialButton_NormalZoomBGInspector : SpatialButtonNormalInspector
    {
        private SpatialButton_NormalZoomBG _target_SpatialButtonNormal;


        protected override void OnEnable()
        {
            base.OnEnable();

            _target_SpatialButtonNormal = target as SpatialButton_NormalZoomBG;

        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            serializedObject.Update();


            serializedObject.ApplyModifiedProperties();

            SceneView.RepaintAll();
        }
    }
}