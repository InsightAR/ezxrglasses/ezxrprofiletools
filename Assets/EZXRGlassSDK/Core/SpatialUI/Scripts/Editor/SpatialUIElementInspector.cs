﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace EZXR.Glass.UI
{
    [CustomEditor(typeof(SpatialUIElement))]
    [CanEditMultipleObjects]
    public class SpatialUIElementInspector : Editor
    {
        private SpatialUIElement _target_SpatialUIElement;

        SerializedProperty interactable;
        SerializedProperty positionType;
        SerializedProperty size;
        SerializedProperty text;
        SerializedProperty textColor;
        SerializedProperty color;
        SerializedProperty texture;
        SerializedProperty sortingOrder;

        protected virtual void OnEnable()
        {
            _target_SpatialUIElement = target as SpatialUIElement;

            interactable = serializedObject.FindProperty("interactable");
            positionType = serializedObject.FindProperty("positionType");
            size = serializedObject.FindProperty("size");
            text = serializedObject.FindProperty("text");
            textColor = serializedObject.FindProperty("textColor");
            color = serializedObject.FindProperty("color");
            texture = serializedObject.FindProperty("texture");
            sortingOrder = serializedObject.FindProperty("sortingOrder");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(interactable, new GUIContent("Interactable", "是否可以与射线或者手进行交互"));
            EditorGUILayout.PropertyField(positionType);
            EditorGUILayout.PropertyField(size, new GUIContent("Size（单位：m）"));
            EditorGUILayout.PropertyField(text);
            EditorGUILayout.PropertyField(textColor);
            EditorGUILayout.PropertyField(color);
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.PropertyField(texture);
                if (GUILayout.Button(new GUIContent("Set Physical Size", "按照SpatialUIController.unitsPerPixel中设定的“像素-UnityUnit转换比例”将Texture以换算后的物理尺寸绘制在空间中")))
                {
                    Undo.RecordObject(_target_SpatialUIElement, "Set Physical Size");
                    _target_SpatialUIElement.size = new Vector3(_target_SpatialUIElement.Texture.width * _target_SpatialUIElement.curUnitsPerPixel, _target_SpatialUIElement.Texture.height * _target_SpatialUIElement.curUnitsPerPixel, _target_SpatialUIElement.size.z);
                    //_target_SpatialUIElement.curUnitsPerPixel = SpatialUIController.Instance.unitsPerPixel;
                    EditorApplication.QueuePlayerLoopUpdate();
                    //SceneView.RepaintAll();
                }
            }
            EditorGUILayout.EndHorizontal();

            EditorGUI.BeginChangeCheck();
            _target_SpatialUIElement.sortingOrder = EditorGUILayout.DelayedIntField("SortingOrder", _target_SpatialUIElement.sortingOrder);
            if (EditorGUI.EndChangeCheck())
            {
                if (_target_SpatialUIElement._text != null && _target_SpatialUIElement._text.GetComponent<MeshRenderer>() != null)
                {
                    _target_SpatialUIElement._text.GetComponent<MeshRenderer>().sortingOrder = _target_SpatialUIElement.sortingOrder + 1;
                }
            }

            serializedObject.ApplyModifiedProperties();

            SceneView.RepaintAll();
        }
    }
}