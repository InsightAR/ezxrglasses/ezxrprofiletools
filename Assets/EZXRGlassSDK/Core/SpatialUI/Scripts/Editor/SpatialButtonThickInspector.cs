﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace EZXR.Glass.UI
{
    [CustomEditor(typeof(SpatialButton_Thick))]
    [CanEditMultipleObjects]
    public class SpatialButtonThickInspector : SpatialButtonInspector
    {
        SpatialButton_Thick _target_SpatialButton_Thick;

        SerializedProperty icon;
        SerializedProperty iconSize;
        SerializedProperty circleHoverThick;

        protected override void OnEnable()
        {
            base.OnEnable();

            _target_SpatialButton_Thick = target as SpatialButton_Thick;

            icon = serializedObject.FindProperty("icon");
            iconSize = serializedObject.FindProperty("iconSize");
            circleHoverThick = serializedObject.FindProperty("circleHoverThick");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            serializedObject.Update();

            if (_target_SpatialButton_Thick.size.z <= _target_SpatialButton_Thick.heightMin)
            {
                _target_SpatialButton_Thick.size = new Vector3(_target_SpatialButton_Thick.size.x, _target_SpatialButton_Thick.size.y, _target_SpatialButton_Thick.heightMin * 2);
            }

            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.PropertyField(icon);
                if (GUILayout.Button(new GUIContent("Set Physical Size", "按照SpatialUIController.unitsPerPixel中设定的“像素-UnityUnit转换比例”将Icon以换算后的物理尺寸绘制在空间中")))
                {
                    Undo.RecordObject(_target_SpatialButton_Thick, "Set Physical Size");
                    _target_SpatialButton_Thick.iconSize = new Vector3(_target_SpatialButton_Thick.Icon.width * _target_SpatialButton_Thick.curUnitsPerPixel, _target_SpatialButton_Thick.Icon.height * _target_SpatialButton_Thick.curUnitsPerPixel, _target_SpatialButton_Thick.iconSize.z);
                    //_target_SpatialButton_Thick.curUnitsPerPixel = SpatialUIController.Instance.unitsPerPixel;
                    EditorApplication.QueuePlayerLoopUpdate();
                }
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.PropertyField(iconSize);

            EditorGUILayout.PropertyField(circleHoverThick);

            serializedObject.ApplyModifiedProperties();

            SceneView.RepaintAll();
        }
    }
}