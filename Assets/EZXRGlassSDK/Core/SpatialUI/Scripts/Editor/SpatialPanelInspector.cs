﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace EZXR.Glass.UI
{
    [CustomEditor(typeof(SpatialPanel))]
    [CanEditMultipleObjects]
    public class SpatialPanelInspector : SpatialUIElementInspector
    {
        SpatialPanel _target_SpatialPanel;

        protected override void OnEnable()
        {
            base.OnEnable();

            _target_SpatialPanel = target as SpatialPanel;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            serializedObject.Update();

            serializedObject.ApplyModifiedProperties();

            SceneView.RepaintAll();
        }

        public void OnSceneGUI()
        {
            Quaternion rotation = _target_SpatialPanel.transform.rotation;
            Matrix4x4 matrix = Matrix4x4.TRS(_target_SpatialPanel.transform.position, rotation, Vector3.one);
            Handles.matrix = matrix;
            Handles.DrawWireCube(Vector3.zero, _target_SpatialPanel.size);
        }
    }
}