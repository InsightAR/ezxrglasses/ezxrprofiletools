﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using EZXR.Glass.Hand;
using UnityEditor;
using EZXR.Glass.Common;

namespace EZXR.Glass.UI
{
    /// <summary>
    /// 有厚度的按钮，hover的时候会有半透明浮层在指尖位置，随着指尖按下浮层会有被按下去的效果
    /// </summary>
    public class SpatialButton_Thick : SpatialButton
    {
        public Transform _iconMesh;
        public MeshRenderer _iconRenderer;
        [SerializeField]
        private Texture2D icon;
        /// <summary>
        /// 按钮的图标
        /// </summary>
        public Texture2D Icon
        {
            get
            {
                return icon;
            }
            set
            {
                icon = value;
                SetIconMaterial();
            }
        }
        public Vector3 iconSize = new Vector3(0.1f, 0.1f, 0.1f);
        protected MaterialPropertyBlock icon_MaterialPropertyBlock;

        /// <summary>
        /// 浮层式Button的浮层，当用指尖进入触发区的时候显示此浮层
        /// </summary>
        public GameObject buttonHover;
        protected MeshRenderer buttonHoverRenderer;
        protected MaterialPropertyBlock buttonHover_MaterialPropertyBlock;
        /// <summary>
        /// 浮层顶部圆环的粗细
        /// </summary>
        public float circleHoverThick = 0.01f;

        // Start is called before the first frame update
        protected override void Start()
        {
            base.Start();

            if (Application.isPlaying)
            {
                //buttonHover.SetActive(false);

                SetIconMaterial();

                SetButtonHoverMaterial();
            }
            else
            {
#if UNITY_EDITOR
                icon_MaterialPropertyBlock = new MaterialPropertyBlock();
                _iconRenderer?.GetPropertyBlock(icon_MaterialPropertyBlock);

                buttonHoverRenderer = buttonHover?.GetComponent<MeshRenderer>();
                buttonHover_MaterialPropertyBlock = new MaterialPropertyBlock();
                buttonHoverRenderer?.GetPropertyBlock(buttonHover_MaterialPropertyBlock);
#endif
            }
        }

        protected override void Update()
        {
            base.Update();

            if (!Application.isPlaying)
            {
#if UNITY_EDITOR
                _iconRenderer.sortingOrder = sortingOrder + 1;

                SetIconMaterial();

                SetButtonHoverMaterial();

                //设置UI尺寸
                _iconMesh.localScale = new Vector3(iconSize.x / size.x, iconSize.y / size.y, 0.0001f);
#endif
            }
        }

        void SetIconMaterial()
        {
            //确保icon_MaterialPropertyBlock不为空
            if (icon_MaterialPropertyBlock == null)
            {
                icon_MaterialPropertyBlock = new MaterialPropertyBlock();
                _iconRenderer.GetPropertyBlock(icon_MaterialPropertyBlock);
            }

            //icon有更改的话同步修改到Material
            if (icon != null)
            {
                _iconRenderer.gameObject.SetActive(true);
                icon_MaterialPropertyBlock.SetTexture("_MainTex", icon);
            }
            else
            {
                _iconRenderer.gameObject.SetActive(false);
                //重置materialPropertyBlock，因为无法直接SetTexture为null
                icon_MaterialPropertyBlock.Clear();

            }
            _iconRenderer.SetPropertyBlock(icon_MaterialPropertyBlock);
        }

        void SetButtonHoverMaterial()
        {
            //确保buttonHover_MaterialPropertyBlock不为空
            if (buttonHover_MaterialPropertyBlock == null)
            {
                buttonHoverRenderer = buttonHover?.GetComponent<MeshRenderer>();
                buttonHover_MaterialPropertyBlock = new MaterialPropertyBlock();
                buttonHoverRenderer?.GetPropertyBlock(buttonHover_MaterialPropertyBlock);
            }
            buttonHover_MaterialPropertyBlock.SetFloat("_Thickness", circleHoverThick);
            buttonHover_MaterialPropertyBlock.SetFloat("_Radius", 0.25f - circleHoverThick);
            buttonHoverRenderer.SetPropertyBlock(buttonHover_MaterialPropertyBlock);
        }

        /// <inheritdoc />
        public override void OnPointerHoverEnter(Spatial.InteractiveMode interactiveMode = Spatial.InteractiveMode.None)
        {
            Debug.Log("OnPointerHoverEnter: " + gameObject.name);

            //显示浮层
            buttonHover.SetActive(true);

            _iconMesh.localPosition = new Vector3(0, 0, _mesh.localScale.z / 2.0f / size.z);

        }

        /// <inheritdoc />
        public override void OnPointerHoverExit()
        {
            Debug.Log("OnPointerHoverExit: " + gameObject.name);

            ////隐藏浮层
            //buttonHover.SetActive(false);

            _iconMesh.localPosition = new Vector3(0, 0, _mesh.localScale.z / 2.0f / size.z);
            //_iconMesh.localPosition = new Vector3(0, 0, 0.00001f);

        }

        /// <inheritdoc />
        public override void OnPointerPressing()
        {
            Debug.Log("OnPointerPressing: " + gameObject.name);
            //将算出的距离直接赋给按键的scale（因为此处按键本身就是按照实际尺寸进行的缩放，所以计算出的length就是scale.z）
            _mesh.localScale = new Vector3(_mesh.localScale.x, _mesh.localScale.y, clampLength);

            _iconMesh.localPosition = new Vector3(0, 0, _mesh.localScale.z / 2.0f / size.z);

        }

        /// <inheritdoc />
        public override void OnPointerDown()
        {
            Debug.Log("OnPointerDown: " + gameObject.name);
            //将按键按到底
            _mesh.localScale = new Vector3(_mesh.localScale.x, _mesh.localScale.y, heightMin);

            _iconMesh.localPosition = new Vector3(0, 0, _mesh.localScale.z / 2.0f / size.z);

        }

        /// <inheritdoc />
        public override void OnPointerUp()
        {
            Debug.Log("OnPointerUp: " + gameObject.name);
            _mesh.localScale = new Vector3(_mesh.localScale.x, _mesh.localScale.y, heightMax);

            _iconMesh.localPosition = new Vector3(0, 0, _mesh.localScale.z / 2.0f / size.z);

        }

    }
}