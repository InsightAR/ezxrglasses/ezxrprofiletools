﻿using EZXR.Glass;
using EZXR.Glass.Common;
using EZXR.Glass.SixDof;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;


namespace EZXR.Glass.SpatialPositioning
{
    public enum LocCam
    {
        LocCam_RGB,
        LocCam_FisheyeGray
    }
    public enum ReLocalizationApplyMode
    {
        /// <summary>
        /// 定位成功后VirtualWorld的transform将被改变
        /// </summary>
        MapAsAnchor,
        /// <summary>
        /// 定位成功后BodyRig的transform将被改变，VirtualWorld的position和rotation保持为0
        /// </summary>
        MapAsWorld
    }

    public class SpatialPositioningManager : MonoBehaviour
    {
        #region singleton
        private static SpatialPositioningManager _instance;
        public static SpatialPositioningManager Instance
        {
            get
            {
                return _instance;
            }
        }
        #endregion

        /// <summary>
        /// 定位资源的URL
        /// </summary>
        public string url = "https://reloc-gw.easexr.com/api/alg/cloud/aw/reloc/proxy?routeApp=parkc&map=arglass";
        //用于外部修改url
        public static string URL = "";
        /// <summary>
        /// 所有的3D资源都应放在此物体下（可以认作此物体的局部坐标系为世界坐标系）
        /// </summary>
        public GameObject virtualWorld;
        public GameObject bodyRig;
        public LocCam locCamType = LocCam.LocCam_RGB;
        public ReLocalizationApplyMode reLocalizationMode = ReLocalizationApplyMode.MapAsWorld;


        public static bool IsLocationSuccess { get { return isLocationSuccess; } }

        private static bool isLocationSuccess;

        protected EzxrCloudLocalization ezxrCloudLocalization = null;
        private bool hasIntricsSet = false;
        private bool hasPoseOffsetSet = false;
        private NormalRGBCameraDevice rgbCameraDevice;
        private EZVIOInputImage locCamImageBuffer;
        private float[] locCamIntriarray = new float[8];
        private OSTMatrices ost_m = new OSTMatrices();
        private GameObject httpManagerObject = null;

        private void Awake()
        {
            if (bodyRig != null)
            {
                bodyRig.transform.position = Pose.identity.position;
                bodyRig.transform.rotation = Pose.identity.rotation;
            }

            virtualWorld.SetActive(false);
        }

        private void Start()
        {
            if (reLocalizationMode == ReLocalizationApplyMode.MapAsWorld)
            {
                HMDPoseTracker.Instance.UseLocalPose = true;
            }
        }

        void OnDestroy()
        {
            //#if UNITY_EDITOR
            //            ARProjectConfig config = ARProjectConfigManager.config;
            //            if (config.spatialPositioning && config.spatialPositioningRefCount > 0)
            //            {
            //                if (Application.isEditor && !Application.isPlaying && Time.frameCount != 0 && Time.renderedFrameCount != 0)//not loading scene
            //                {
            //                    config.spatialPositioningRefCount--;
            //                    if (config.spatialPositioningRefCount == 0)
            //                    {
            //                        config.spatialPositioning = false;
            //                    }
            //                }
            //            }
            //#endif
        }

        private void OnEnable()
        {
            Debug.LogWarning("-ar- LocalizeManager OnEnable");

            if (!string.IsNullOrEmpty(URL))
            {
                url = URL;
                URL = "";
            }

            if (locCamType == LocCam.LocCam_RGB)
            {
                StartCoroutine(startRGBCamera());
            }
            startTrackSession();
        }

        private void OnDisable()
        {
            isLocationSuccess = false;
            stopTrackSession();
            if (locCamType == LocCam.LocCam_RGB)
            {
                stopRGBCamera();
            }
        }
        private void Update()
        {
            if (ezxrCloudLocalization != null)
            {
                ezxrCloudLocalization.HandleLocalizerRequest(url);
            }

            //UpdateCameraImage();
            RelocByCloudLoc();
        }

        private IEnumerator startRGBCamera()
        {
            yield return new WaitUntil(() => SessionManager.Instance != null && SessionManager.Instance.IsInited);
            hasIntricsSet = false;
            hasPoseOffsetSet = false;
            Debug.LogWarning("-10001-startRgbCamera new rgbCameraDevice Open");
            locCamImageBuffer = new EZVIOInputImage();
            rgbCameraDevice = new NormalRGBCameraDevice();
            rgbCameraDevice.Open();
        }

        private void stopRGBCamera()
        {
            if (rgbCameraDevice != null)
                rgbCameraDevice.Close();
            rgbCameraDevice = null;
            if (locCamImageBuffer.fullImg != IntPtr.Zero)
            {
                Marshal.FreeHGlobal(locCamImageBuffer.fullImg);
                locCamImageBuffer.fullImg = IntPtr.Zero;
            }
        }

        private void startTrackSession()
        {
            ezxrCloudLocalization = new EzxrCloudLocalization();

            Debug.LogWarning("-10001-startTrackSession ezxrCloudLocalization.Create");

            if (ezxrCloudLocalization != null)
            {
                ezxrCloudLocalization.Create();
            }

            Debug.LogWarning("-10001-startTrackSession end");
            //Invoke("RelocByTrack2d", 2.0f);
            InvokeRepeating("UpdateCameraImage", 0.5f, 1.0f);
        }

        private void stopTrackSession()
        {
            Debug.LogWarning("-10001-stopTrackSession");

            if (ezxrCloudLocalization != null)
            {
                ezxrCloudLocalization.Destroy();
                ezxrCloudLocalization = null;
            }
        }

        private void RelocByCloudLoc()
        {
            if (reLocalizationMode == ReLocalizationApplyMode.MapAsAnchor)
            {
                if (ezxrCloudLocalization != null)
                {
                    Pose pose = Pose.identity;
                    isLocationSuccess = ezxrCloudLocalization.GettLocalizedAnchorPose(ref pose);
                    if (isLocationSuccess)
                    {
                        virtualWorld.SetActive(true);
                        Debug.LogWarning("-10001-RelocByCloudLoc anchor succ pose=" + pose.position.ToString("f3"));
                        if (virtualWorld != null)
                        {
                            virtualWorld.transform.position = pose.position;
                            virtualWorld.transform.rotation = pose.rotation;
                        }
                    }
                }
            }
            else
            { //reLocalizationMode == ReLocalizationApplyMode.MapAsWorld
                if (ezxrCloudLocalization != null)
                {
                    Pose pose = Pose.identity;
                    //Test
                    //isLocationSuccess = ezxrCloudLocalization.GettLocalizedAnchorPose(ref pose);
                    isLocationSuccess = ezxrCloudLocalization.GettReLocalizationTransform(ref pose);
                    if (isLocationSuccess)
                    {
                        Debug.LogWarning("-10001-RelocByCloudLoc world succ pose=" + pose.position.ToString("f3") + pose.rotation.ToEuler().ToString());
                        virtualWorld.SetActive(true);
                        if (bodyRig != null)
                        {
                            bodyRig.transform.position = pose.position;
                            bodyRig.transform.rotation = pose.rotation;
                            Debug.LogWarning("-10001-RelocByCloudLoc world succ CamPose=" + HMDPoseTracker.Instance.leftCamera.transform.position.ToString("f3") + HMDPoseTracker.Instance.leftCamera.transform.rotation.ToEuler().ToString());
                        }
                    }
                }
            }
        }
        private void UpdateCameraImage()
        {
            if (ARFrame.SessionStatus != EZVIOState.EZVIOCameraState_Tracking)
                return;
            if (ezxrCloudLocalization == null)
                return;

            if (!hasPoseOffsetSet)
            {
                NativeTracking.GetOSTParams(ref ost_m);
            }
            if (locCamType == LocCam.LocCam_RGB)
            {
                if (rgbCameraDevice == null)
                {
                    return;
                }
                if (!hasPoseOffsetSet)
                {
                    ezxrCloudLocalization.SetPoseFromHeadToLocCam(ost_m.T_RGB_Head);
                    hasPoseOffsetSet = true;
                }
                bool isCameraMotionViolently = rgbCameraDevice.isCameraMotionViolently();
                if (!isCameraMotionViolently)
                {
                    bool res = false;
                    res = rgbCameraDevice.getCurrentImage(ref locCamImageBuffer, locCamIntriarray);
                    //res = NativeTracking.GetCuttedTrackingImage(ref locCamImageBuffer, locCamIntriarray);
                    if (res)
                    {
                        if (!hasIntricsSet)
                        {
                            // @xunighao: 内参永远一致？
                            ezxrCloudLocalization.SetCameraIntrics(locCamIntriarray);
                            hasIntricsSet = true;
                        }
                        double timestamp_sec = locCamImageBuffer.timestamp;
                        Pose imageTsHeadPose = ARFrame.GetHistoricalHeadPose(timestamp_sec);
                        ezxrCloudLocalization.UpdateHeadPose(imageTsHeadPose, timestamp_sec);
                        int format = (int)locCamImageBuffer.imgFormat;
                        //Debug.Log("-10001-UpdateRgbFrame ezxrCloudLocalization.UpdateRGBImage:"+ locCamImageBuffer.fullImg+","+timestamp_sec.ToString("f3"));
                        ezxrCloudLocalization.UpdateImage(locCamImageBuffer.fullImg, timestamp_sec, (int)locCamImageBuffer.imgRes.width, (int)locCamImageBuffer.imgRes.height, format);
                    }
                }
            }

            if (locCamType == LocCam.LocCam_FisheyeGray)
            {
                if (!hasPoseOffsetSet)
                {
                    ezxrCloudLocalization.SetPoseFromHeadToLocCam(ost_m.T_TrackCam_Head);
                    hasPoseOffsetSet = true;
                }
                bool res = NativeTracking.GetCurrentImage(ref locCamImageBuffer, locCamIntriarray);
                if (res)
                {
                    if (!hasIntricsSet)
                    {
                        ezxrCloudLocalization.SetCameraIntrics(locCamIntriarray);
                        hasIntricsSet = true;
                    }
                    double timestamp_sec = locCamImageBuffer.timestamp;
                    Pose imageTsHeadPose = ARFrame.GetHistoricalHeadPose(timestamp_sec);
                    ezxrCloudLocalization.UpdateHeadPose(imageTsHeadPose, timestamp_sec);
                    int format = 0;
                    if (locCamImageBuffer.imgFormat == EZVIOImageFormat.EZVIOImageFormat_RGB)
                    {
                        format = 0;
                    }
                    else
                    {
                        format = 2;//gray
                    }
                    //Debug.Log("-10001-UpdateRgbFrame ezxrCloudLocalization.UpdateRGBImage:"+ locCamImageBuffer.fullImg+","+timestamp_sec.ToString("f3"));
                    ezxrCloudLocalization.UpdateImage(locCamImageBuffer.fullImg, timestamp_sec, (int)locCamImageBuffer.imgRes.width, (int)locCamImageBuffer.imgRes.height, format);
                }

            }
        }
    }
}
