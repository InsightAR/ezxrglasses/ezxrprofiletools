﻿using EZXR.Glass.SpatialPositioning;
using UnityEditor;
using UnityEngine;

namespace EZXR.Glass.Common
{
    public partial class ARAbilities : MonoBehaviour
    {
        #region SpatialPositioning
        [MenuItem("GameObject/XR Abilities/SpatialPositioning", false, 20)]
        public static void EnableSpatialPositioning()
        {
            if (FindObjectOfType<SpatialPositioningManager>() == null)
            {
                string filePath = "Assets/EZXRGlassSDK/Core/SpatialPositioning/Prefabs/SpatialPositioningManager.prefab";
                if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
                {
                    filePath = AssetDatabase.GUIDToAssetPath("06bbeec2a98d8bd47a5d23c6d228eb60");
                }
                Common.PrefabUtility.InstantiatePrefabWithUndoAndSelection(AssetDatabase.LoadAssetAtPath<GameObject>(filePath));

                UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene());
            }
        }
        #endregion
    }
}