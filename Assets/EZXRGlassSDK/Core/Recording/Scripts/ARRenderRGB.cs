﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering;
using System.IO;
using System.Threading;
using EZXR.Glass.Common;
using EZXR.Glass.SixDof;
using System.Runtime.InteropServices;
using AOT;
using EZXR.Glass.Hand;

namespace EZXR.Glass.Recording
{
    public class ARRenderRGB : MonoBehaviour
    {
        public static ARRenderRGB Instance
        {
            get
            {
                return _instance;
            }
        }

        private bool _isReady = false;
        public bool isReady
        {
            get
            {
                return _isReady;
            }
        }


        private static ARRenderRGB _instance = null;

        private const int RENDER_EVENT_DRAWRGB = 0x0001;


        /// <summary> Renders the event delegate described by eventID. </summary>
        /// <param name="eventID"> Identifier for the event.</param>
        private delegate void RenderEventDelegate(int eventID);
        /// <summary> Handle of the render thread. </summary>
        private static RenderEventDelegate RenderThreadHandle = new RenderEventDelegate(RunOnRenderThread);
        private static IntPtr RenderThreadHandlePtr = Marshal.GetFunctionPointerForDelegate(RenderThreadHandle);


        #region params
        private GameObject m_VideoQuad;
        private Material m_BackgroundMaterial;
        private Camera m_Camera;

        private Texture2D m_VideoTexture;

        private static int screenWidth;
        private static int screenHeight;
        private static float _imageWidth = 0;
        private static float _imageHeight = 0;

        private NormalRGBCameraDevice rgbCameraDevice;

        private Coroutine drawRgbCameraBackgroundCorotine;


        #endregion

        #region unity functions

        private void Awake()
        {
            _instance = this;
            m_Camera = GetComponent<Camera>();
            rgbCameraDevice = new NormalRGBCameraDevice();
        }

        private void Start()
        {
        }

        private void OnDisable()
        {
            
        }

        private void OnDestroy()
        {
            _instance = null;
        }
        #endregion

        #region custom functions
        public void HandleARRenderOpen()
        {
            if (drawRgbCameraBackgroundCorotine != null)
                return;
            m_Camera.enabled = true;

            StartCoroutine(startRGBCamera());
            StartCoroutine(ConfigARRenderCamera());
            drawRgbCameraBackgroundCorotine = StartCoroutine(CallPluginAtEndOfFrames());
        }

        public void HandleARRenderClose()
        {
            m_Camera.enabled = false;
            if (drawRgbCameraBackgroundCorotine == null)
                return;
            stopRGBCamera();
            StopCoroutine(drawRgbCameraBackgroundCorotine);
            if (m_VideoQuad)
            {
                Destroy(m_VideoQuad);
                m_VideoQuad = null;
            }
            drawRgbCameraBackgroundCorotine = null;
        }

        public bool HandleARRenderShot()
        {
            if (!isReady) return false;
            if (rgbCameraDevice != null)
            {
                rgbCameraDevice.DrawRGBVideoFrame();
                if (rgbCameraDevice.GetRGBVideoTexture() > 0)
                {
                    if (m_VideoTexture != null)
                    {
                        m_VideoTexture.UpdateExternalTexture((IntPtr)rgbCameraDevice.GetRGBVideoTexture());
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary> Executes the 'on render thread' operation. </summary>
        /// <param name="eventID"> Identifier for the event.</param>
        [MonoPInvokeCallback(typeof(RenderEventDelegate))]
        private static void RunOnRenderThread(int eventID)
        {
            if (eventID == RENDER_EVENT_DRAWRGB)
            {
                if (_instance != null)
                    _instance.drawRGBTexture();
            }
        }

        private void drawRGBTexture()
        {
            if (rgbCameraDevice != null)
            {
                if (m_VideoTexture != null) {
                    rgbCameraDevice.DrawRGBVideoFrame(m_VideoTexture.GetNativeTexturePtr());
                }
            }
        }

        public static void SetRGBResolution(int imageWidth, int imageHeight)
        {
            _imageWidth = imageWidth;
            _imageHeight = imageHeight;
            Debug.Log("arrenderrgb set: " + _imageWidth + "," + _imageHeight);
        }

        private void ConfigVideoPlane()
        {
            if (m_VideoQuad != null)
                return;

            m_VideoQuad = GameObject.CreatePrimitive(PrimitiveType.Plane);
            Collider videoQuadCollider = m_VideoQuad.GetComponent<Collider>();
            if (videoQuadCollider != null)
            {
                Destroy(videoQuadCollider);
            }

            float[] intrinsic = new float[8];
            //rgbCameraDevice.getCameraIntrics(intrinsic);
            //float fx = intrinsic[0];
            float fx = intrinsic[0] = 999;
            m_VideoQuad.transform.parent = this.transform;
            m_VideoQuad.transform.localRotation = Quaternion.AngleAxis(-90, Vector3.right) * Quaternion.AngleAxis(180, Vector3.up);
            m_VideoQuad.transform.localPosition = Vector3.forward * 50;
            Debug.Log("arrenderrgb: " + _imageWidth + "," + _imageHeight);
            m_VideoQuad.transform.localScale = new Vector3(_imageWidth * 5 / fx, 1.0f, _imageHeight * 5 / fx);

            //选择第2个空layer用于投屏
            string[] layerNames = new string[32];
            for (int i = 0; i < 32; i++)
            {
                layerNames[i] = LayerMask.LayerToName(i);
            }
            int count = 0;
            for (int i = 0; i < layerNames.Length; i++)
            {
                if (string.IsNullOrEmpty(layerNames[i]))
                {
                    if (count == 1)
                    {
                        m_VideoQuad.layer = i;
                        m_Camera.cullingMask |= 1 << i;

                        HMDPoseTracker.Instance.leftCamera.cullingMask = ~(~HMDPoseTracker.Instance.leftCamera.cullingMask | (1 << i));
                        HMDPoseTracker.Instance.rightCamera.cullingMask = ~(~HMDPoseTracker.Instance.rightCamera.cullingMask | (1 << i));
                        HMDPoseTracker.Instance.centerCamera.cullingMask = ~(~HMDPoseTracker.Instance.centerCamera.cullingMask | (1 << i));
                        break;
                    }
                    else
                    {
                        count++;
                    }
                }
            }
            m_VideoQuad.GetComponent<MeshRenderer>().material = m_BackgroundMaterial;
        }

        private IEnumerator ConfigARRenderCamera()
        {
            yield return new WaitUntil(() => rgbCameraDevice.IsStarted());
            
            Debug.Log("UNITY LOG ========= ARRenderRGB, ConfigARRenderCamera");

            if (m_BackgroundMaterial == null)
            {
                m_BackgroundMaterial = new Material(Shader.Find("ARBackgroundNew"));
                //m_BackgroundMaterial = new Material(Shader.Find("Unlit/Texture"));
            }

            CameraResolution cameraResolution = new CameraResolution();
            NativeTracking.GetRGBCameraResolution(ref cameraResolution);
            screenWidth = cameraResolution.width;
            screenHeight = cameraResolution.height;
            _imageWidth = cameraResolution.width;
            _imageHeight = cameraResolution.height;

            // Create a texture
            m_VideoTexture = new Texture2D(screenWidth, screenHeight, TextureFormat.RGBA32, false);
            m_VideoTexture.filterMode = FilterMode.Bilinear;
            m_VideoTexture.wrapMode = TextureWrapMode.Clamp;

            m_BackgroundMaterial.SetTexture("_MainTex", m_VideoTexture);
            //Texture 坐标和opengl图像坐标是上下颠倒的,修正方向
            m_BackgroundMaterial.SetVector(
                "_UvTopLeftRight",
                new Vector4(
                    0.0f, 1.0f, 1.0f, 1.0f));
            m_BackgroundMaterial.SetVector(
                "_UvBottomLeftRight",
                new Vector4(0.0f, 0.0f, 1.0f, 0.0f));

            ConfigVideoPlane();
            

            _isReady = true;
            Debug.Log("UNITY LOG ========= ARRenderRGB, _isReady = " + isReady);
        }

        private IEnumerator CallPluginAtEndOfFrames()
        {
            if (!Application.isEditor)
            {
                yield return new WaitUntil(() => rgbCameraDevice.IsStarted());
                while (true)
                {
                    // Wait until all frame rendering is done
                    yield return new WaitForEndOfFrame();

                    // Issue a plugin event with arbitrary integer identifier.
                    // The plugin can distinguish between different
                    // things it needs to do based on this ID.
                    // For our simple plugin, it does not matter which ID we pass here.
                    //GL.IssuePluginEvent(rgbCameraDevice.GetDrawRGBVideoFrameFunc(), 1);
                    GL.IssuePluginEvent(RenderThreadHandlePtr, RENDER_EVENT_DRAWRGB);
                }
            }
        }

        private IEnumerator startRGBCamera()
        {
            yield return new WaitUntil(() => SessionManager.Instance != null && SessionManager.Instance.IsInited);
            Debug.Log("UNITY LOG ========= ARRenderRGB, startRGBCamera");
            rgbCameraDevice.Open();
        }

        private void stopRGBCamera()
        {
            if (rgbCameraDevice.IsStarted())
                rgbCameraDevice.Close();
        }
        #endregion

    }
}
