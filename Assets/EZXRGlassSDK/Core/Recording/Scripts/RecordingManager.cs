﻿using EZXR.Glass.SixDof;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using NatSuite.Recorders;
using NatSuite.Recorders.Clocks;
using NatSuite.Recorders.Inputs;
using EZXR.Glass.Recording;
using System;
using EZXR.Glass.SystemMenu;

public class RecordingManager : MonoBehaviour
{
    private static RecordingManager instance;
    public static RecordingManager Instance
    {
        get
        {
            return instance;
        }
    }

    private int width = 1280;
    private int height = 960;
    private IMediaRecorder recorder;
    private CameraInput cameraInput;

    bool beSave = false;

    //public Renderer testRenderer;
    public GameObject canvas;
    public Text record_savedText;
    public GameObject record_savedTips;
    private Coroutine toastCoroutine;

    private Coroutine captureCoroutine;
    private const string gallery_savedDir = "/storage/emulated/0/Pictures/Screenshots";


    private void Awake()
    {
        instance = this;

        //ARRenderRGB.SetRGBResolution(width, height);
    }

    // Start is called before the first frame update
    void Start()
    {
        CameraResolution cameraResolution = new CameraResolution();
        NativeTracking.GetRGBCameraResolution(ref cameraResolution);
        width = cameraResolution.width;
        height = cameraResolution.height;
    }

    private async void GetPathAsync()
    {
        var path = await recorder.FinishWriting();
        Debug.Log("Saved Recording to : " + path);

        SaveToGallery(path);
    }

    public void HandleRecordingOpen()
    {
        ARRenderRGB.Instance.HandleARRenderOpen();

        var frameRate = 30;
        var sampleRate = 0;
        var channelCount = 0;
        var clock = new RealtimeClock();
        recorder = new MP4Recorder(width, height, frameRate, sampleRate, channelCount);
        Camera renderCam = ARRenderRGB.Instance.gameObject.GetComponent<Camera>();
        cameraInput = new CameraInput(recorder, clock, renderCam);

        Debug.Log("UNITY LOG ========= Start Record");
    }

    public void HandleRecordingClose()
    {
        ARRenderRGB.Instance.HandleARRenderClose();

        cameraInput.Dispose();

        Debug.Log("UNITY LOG ========= Stop Record");

        GetPathAsync();

        if (toastCoroutine != null) StopCoroutine(toastCoroutine);
        record_savedText.text = "录屏已保存";
        toastCoroutine = StartCoroutine(WaitForToastTips());
    }

    public void HandleScreenCapture()
    {
        Debug.Log("UNITY LOG ========= ScreenCapture, size: " + width + ", " + height);

        if (captureCoroutine != null) return;

        ARRenderRGB.Instance.HandleARRenderOpen();

        captureCoroutine = StartCoroutine(WaitForScreenCapture());
    }

    private IEnumerator WaitForScreenCapture()
    {
        yield return new WaitUntil(() => ARRenderRGB.Instance.isReady);
        //RGB 相机打开时曝光适配需要时间，0.41s是尝试值，如果实际测试有不够亮，再进行调整
        yield return new WaitForSeconds(0.41f);
        bool status = ARRenderRGB.Instance.HandleARRenderShot();
        if (!status)
        {
            Debug.Log("UNITY LOG ========= HandleARRenderShot failed! ");
            captureCoroutine = null;
            yield break;
        }
        else
        {
            Debug.Log("UNITY LOG ========= HandleARRenderShot success! ");
        }

        //fix: RT depth为0会引起渲染层级和blend错乱
        RenderTexture rt = new RenderTexture(width, height, 24);
        Camera renderCam = ARRenderRGB.Instance.gameObject.GetComponent<Camera>();
        renderCam.targetTexture = rt;
        renderCam.Render();

        RenderTexture.active = rt;
        Texture2D screenShot = new Texture2D(width, height, TextureFormat.RGB24, false);
        screenShot.ReadPixels(new Rect(0, 0, width, height), 0, 0);// 注：这个时候，它是从RenderTexture.active中读取像素  
        screenShot.Apply();

        //testRenderer.sharedMaterial.SetTexture("_MainTex", screenShot);

        // 重置相关参数，以使用camera继续在屏幕上显示  
        renderCam.targetTexture = null;
        RenderTexture.active = null; // added to avoid errors
        GameObject.Destroy(rt);

        var imageName = "screenshot_" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_fff") + ".jpg";
        var imagePath = Path.Combine(Application.persistentDataPath, imageName);
        var bytes = screenShot.EncodeToJPG();
        File.WriteAllBytes(imagePath, bytes);

        Debug.Log("UNITY LOG ========= ScreenCapture, save jpg at path: " + imagePath);
        SaveToGallery(imagePath);

        captureCoroutine = null;
        ARRenderRGB.Instance.HandleARRenderClose();

        if (toastCoroutine != null) StopCoroutine(toastCoroutine);
        record_savedText.text = "截屏已保存";
        toastCoroutine = StartCoroutine(WaitForToastTips());
    }

    private void SaveToGallery(string src_path)
    {
        Debug.Log("UNITY LOG =========, save to gallery from: " + src_path);

        var dst_dir = SystemMenuUtils.GetScreenshotsPath();
        if (string.IsNullOrEmpty(dst_dir) || !Directory.Exists(dst_dir))
        {
            dst_dir = Path.GetDirectoryName(gallery_savedDir);
        }

        var dst_path = Path.Combine(dst_dir, Path.GetFileName(src_path));
        File.Move(src_path, dst_path);

        Debug.Log("UNITY LOG =========, saved to gallery at: " + dst_path);

        SystemMenuUtils.AddToAlbum(dst_path);
        Debug.Log("UNITY LOG =========, AddToAlbum at: " + dst_path);

        //var src_name = Path.GetFileNameWithoutExtension(src_path);
        //AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        //AndroidJavaObject currentActivity = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");
        //AndroidJavaObject contentResolver = currentActivity.Call<AndroidJavaObject>("getContentResolver");

        //// 调用insertImage保存到相册
        //AndroidJavaClass galleryClass = new AndroidJavaClass("android.provider.MediaStore$Images$Media");
        //AndroidJavaObject galleryObject = new AndroidJavaObject("java.io.File", src_path);
        //AndroidJavaObject galleryUri = galleryClass.CallStatic<AndroidJavaObject>("insertImage",
        //    contentResolver, galleryObject.Call<string>("getAbsolutePath"), src_name, "");

        //Debug.Log("UNITY LOG =========, saved to gallery!");
        //File.Delete(src_path);
    }

    private IEnumerator WaitForToastTips()
    {
        Debug.Log("UNITY LOG ========= WaitForToastTips call");
        canvas.SetActive(true);
        record_savedTips.SetActive(true);
        yield return new WaitForSeconds(2);
        record_savedTips.SetActive(false);
        canvas.SetActive(false);
    }
}
