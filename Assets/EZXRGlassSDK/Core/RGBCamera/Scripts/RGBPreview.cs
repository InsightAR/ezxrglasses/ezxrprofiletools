﻿using System;
using System.Collections;
using UnityEngine;
using EZXR.Glass.Common;
using EZXR.Glass.SixDof;
using System.Runtime.InteropServices;
using AOT;

namespace EZXR.Glass.RGBCamera
{
    public class RGBPreview : MonoBehaviour
    {
        public static RGBPreview Instance
        {
            get
            {
                return _instance;
            }
        }

        private static RGBPreview _instance = null;

        private const int RENDER_EVENT_DRAWRGB = 0x0001;


        /// <summary> Renders the event delegate described by eventID. </summary>
        /// <param name="eventID"> Identifier for the event.</param>
        private delegate void RenderEventDelegate(int eventID);
        /// <summary> Handle of the render thread. </summary>
        private static RenderEventDelegate RenderThreadHandle = new RenderEventDelegate(RunOnRenderThread);
        private static IntPtr RenderThreadHandlePtr = Marshal.GetFunctionPointerForDelegate(RenderThreadHandle);


        #region params
        public GameObject m_VideoQuad;
        private Material m_BackgroundMaterial;
        private Texture2D m_VideoTexture;

        private int _imageWidth = 0;
        private int _imageHeight = 0;

        private NormalRGBCameraDevice rgbCameraDevice;
        private Coroutine previewCoroutine;

        #endregion

        #region unity functions

        private void Awake()
        {
            _instance = this;
        }
        private void OnEnable()
        {
            StartCoroutine(startRGBCamera());
            previewCoroutine = StartCoroutine(startRGBPreview());
        }

        private void LateUpdate()
        {
            this.transform.position = ARFrame.HeadPose.position;
            //this.transform.rotation = ARFrame.HeadPose.rotation;
        }

        private void OnDisable()
        {
            if (previewCoroutine != null)
            {
                Debug.Log("-10001- Stop previewCoroutine");
                StopCoroutine(previewCoroutine);
                previewCoroutine = null;
            }
            stopRGBCamera();
        }

        private void OnDestroy()
        {
            _instance = null;
        }
        #endregion

        #region custom functions

        /// <summary> Executes the 'on render thread' operation. </summary>
        /// <param name="eventID"> Identifier for the event.</param>
        [MonoPInvokeCallback(typeof(RenderEventDelegate))]
        private static void RunOnRenderThread(int eventID)
        {
            if (eventID == RENDER_EVENT_DRAWRGB)
            {
                if (_instance != null)
                    _instance.drawRGBTexture();
            }
        }

        private IEnumerator startRGBPreview()
        {
            yield return new WaitUntil(() => rgbCameraDevice != null);
            Debug.Log("-10001- startRGBPreview");
            if (m_BackgroundMaterial == null)
            {
                m_BackgroundMaterial = new Material(Shader.Find("ARBackgroundNew"));
                //m_BackgroundMaterial = new Material(Shader.Find("Unlit/Texture"));
            }

            CameraResolution cameraResolution = new CameraResolution();
            NativeTracking.GetRGBCameraResolution(ref cameraResolution);
            _imageWidth = cameraResolution.width;
            _imageHeight = cameraResolution.height;
            Debug.Log("-10001-startRGBPreview cameraSize "+_imageWidth+","+_imageHeight);

            //Texture 坐标和opengl图像坐标是上下颠倒的,修正方向
            m_BackgroundMaterial.SetVector(
                "_UvTopLeftRight",
                new Vector4(
                    0.0f, 1.0f, 1.0f, 1.0f));
            m_BackgroundMaterial.SetVector(
                "_UvBottomLeftRight",
                new Vector4(0.0f, 0.0f, 1.0f, 0.0f));

            ConfigVideoPlane();

            if (m_VideoQuad != null)
            {
                m_VideoQuad.GetComponent<MeshRenderer>().material = m_BackgroundMaterial;
            }

            while (true)
            {
                // Wait until all frame rendering is done
                yield return new WaitForEndOfFrame();

                // Issue a plugin event with arbitrary integer identifier.
                // The plugin can distinguish between different
                // things it needs to do based on this ID.
                // For our simple plugin, it does not matter which ID we pass here.
                //GL.IssuePluginEvent(rgbCameraDevice.GetDrawRGBVideoFrameFunc(), 1);
                GL.IssuePluginEvent(RenderThreadHandlePtr, RENDER_EVENT_DRAWRGB);
            }
        }

        private IEnumerator startRGBCamera()
        {
            yield return new WaitUntil(() => SessionManager.Instance != null && SessionManager.Instance.IsInited);
            Debug.Log("-10001-startRgbCamera new rgbCameraDevice Open");
            rgbCameraDevice = new NormalRGBCameraDevice();
            rgbCameraDevice.Open();
        }

        private void stopRGBCamera()
        {
            Debug.Log("-10001- stopRGBCamera");
            if (rgbCameraDevice != null)
                rgbCameraDevice.Close();
            rgbCameraDevice = null;
        }
        private void ConfigVideoPlane()
        {
            if (m_VideoQuad == null) {
                m_VideoQuad = GameObject.CreatePrimitive(PrimitiveType.Plane);
                m_VideoQuad.transform.parent = this.transform;
            }

            Collider videoQuadCollider = m_VideoQuad.GetComponent<Collider>();
            if (videoQuadCollider != null)
            {
                Destroy(videoQuadCollider);
            }

            float fx = 10;
            float aspect = (float)_imageWidth / _imageHeight;
            m_VideoQuad.transform.localPosition = Vector3.forward * fx;
            m_VideoQuad.transform.localRotation = Quaternion.AngleAxis(-90, Vector3.right) * Quaternion.AngleAxis(180, Vector3.up);
            //m_VideoQuad.transform.localRotation = Quaternion.identity;

            m_VideoQuad.transform.localScale = new Vector3(aspect * 0.5f, 1.0f, 0.5f);
        }
        private void drawRGBTexture()
        {
            if (rgbCameraDevice != null)
            {
                rgbCameraDevice.DrawRGBVideoFrame();
                if (rgbCameraDevice.GetRGBVideoTexture() > 0)
                {
                    if (m_VideoTexture == null)
                    {
                        m_VideoTexture = Texture2D.CreateExternalTexture(_imageWidth, _imageHeight,
                            TextureFormat.RGBA32, false, true, (IntPtr)rgbCameraDevice.GetRGBVideoTexture());
                        m_VideoTexture.filterMode = FilterMode.Bilinear;
                        m_VideoTexture.wrapMode = TextureWrapMode.Clamp;

                        m_VideoTexture.UpdateExternalTexture((IntPtr)rgbCameraDevice.GetRGBVideoTexture());

                        m_BackgroundMaterial.SetTexture("_MainTex", m_VideoTexture);
                    }
                    else
                    {
                        m_VideoTexture.UpdateExternalTexture((IntPtr)rgbCameraDevice.GetRGBVideoTexture());
                    }
                }
            }
        }
        #endregion

    }
}
