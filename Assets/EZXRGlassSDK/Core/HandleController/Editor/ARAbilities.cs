﻿using EZXR.Glass.Hand;
using EZXR.Glass.HandleController;
using UnityEditor;
using UnityEngine;

namespace EZXR.Glass.Common
{
    public partial class ARAbilities : MonoBehaviour
    {
        #region HandleController
        [MenuItem("GameObject/XR Abilities/HandleController", false, 20)]
        public static void EnableHandleController()
        {
            if (FindObjectOfType<HandleControllerManager>() == null)
            {
                string filePath = "Assets/EZXRGlassSDK/Core/HandleController/Prefabs/HandleController.prefab";
                if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
                {
                    filePath = AssetDatabase.GUIDToAssetPath("bdeb75d80ee1c443483a96c2c24761ae");
                }
                Common.PrefabUtility.InstantiatePrefabWithUndoAndSelection(AssetDatabase.LoadAssetAtPath<GameObject>(filePath));
            }

            if (FindObjectOfType<ARHandManager>() != null)
            {
                DestroyImmediate(FindObjectOfType<ARHandManager>().gameObject);
                Debug.LogWarning("Destroy HandRig gameObject");
            }
        }
        #endregion
    }
}