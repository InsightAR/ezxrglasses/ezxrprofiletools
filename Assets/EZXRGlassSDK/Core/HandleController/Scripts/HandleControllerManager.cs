using System;
using System.Collections;
using System.Collections.Generic;
using EZXR.Glass.Common;
using EZXR.Glass.Hand;
using UnityEngine;

namespace EZXR.Glass.HandleController
{
    public enum HandleKeyEvent
    {
        Idle,       //默认状态
        Down,       //按下(仅单帧有效)
        Up,         //弹起(仅单帧有效)
        /// <summary>
        /// (feat)等同于长按
        /// Down之后每帧有效
        /// </summary>
        PrePressed, //介于按下和长按之间状态
        Pressing    //长按(每帧有效)
    }

    [ScriptExecutionOrder(-49)]
    public class HandleControllerManager : MonoBehaviour
    {
        #region singleton
        private static HandleControllerManager instance;
        public static HandleControllerManager Instance
        {
            get
            {
                return instance;
            }
        }
        #endregion

        /// <summary>
        /// 记录了左手柄的所有信息
        /// </summary>
        public static HandleControllerInfo leftHand;
        /// <summary>
        /// 记录了右手柄的所有信息
        /// </summary>
        public static HandleControllerInfo rightHand;

        // 手柄模型
        public Transform leftController;
        public Transform rightController;

        private List<Action<HandType, bool>> bindingActions = new List<Action<HandType, bool>>();
        private List<Action<HandType, bool>> connectedActions = new List<Action<HandType, bool>>();
        private List<Action<HandType, float>> powerChangedActions = new List<Action<HandType, float>>();

        private void Awake()
        {
            instance = this;
            leftHand = leftController.GetComponent<HandleControllerInfo>();
            rightHand = rightController.GetComponent<HandleControllerInfo>();
        }

        private void Start()
        {
            HandleControllerSession.Instance.InitRegistration(OnBindingEventCallback, OnConnectEventCallback, OnButtonEventCallback, OnAxis2DEventCallback, OnHoldEventCallback);

            if (leftHand != null)
            {
                // 初始化手柄的生命状态
                leftHand.Init(HandType.Left);
            }
            if (rightHand != null)
            {
                // 初始化手柄的生命状态
                rightHand.Init(HandType.Right);
            }

            StartCoroutine(WaitForTimeout());
        }

        private void Update()
        {
            //if (Input.GetKeyDown(KeyCode.Q))
            //    Debug.Log($"HandleControllerManager, rightHand binding = {rightController != null && rightHand.Exist}, controllerPose: ({HandleControllerSession.controllerPose_right.position.x}, {HandleControllerSession.controllerPose_right.position.y}, {HandleControllerSession.controllerPose_right.position.z})");

            if (leftHand.Exist || rightHand.Exist)
                HandleControllerSession.Instance.UpdateHandlePose();
            if (leftController != null && leftHand.Exist)
                leftController.SetPositionAndRotation(HandleControllerSession.controllerPose_left.position, HandleControllerSession.controllerPose_left.rotation);
            if (rightController != null && rightHand.Exist)
                rightController.SetPositionAndRotation(HandleControllerSession.controllerPose_right.position, HandleControllerSession.controllerPose_right.rotation);
        }

        /// <summary>
        /// 初始化注册，监听手柄生命状态
        /// </summary>
        /// <param name="bindingEventCallback">绑定状态改变的回调事件</param>
        /// <param name="connectedEventCallback">连接状态改变的回调事件</param>
        /// <param name="powerChangedCallback">电量改变的回调事件</param>
        public void InitRegistration(Action<HandType, bool> bindingEventCallback, Action<HandType, bool> connectedEventCallback, Action<HandType, float> powerChangedCallback)
        {
            bindingActions.Add(bindingEventCallback);
            connectedActions.Add(connectedEventCallback);
            powerChangedActions.Add(powerChangedCallback);
        }

        /// <summary>
        /// 进行手柄配对
        /// </summary>
        public bool BindHandle(HandType handType)
        {
            return HandleControllerSession.Instance.BindHandle((int)handType);
        }

        /// <summary>
        /// 取消手柄配对
        /// </summary>
        public bool UnbindHandle(HandType handType)
        {
            return HandleControllerSession.Instance.UnbindHandle((int)handType);
        }

        /// <summary>
        /// 手柄震动，震动等级 1-8，震动时长 0-65535 ms
        /// </summary>
        public bool VibrateHandle(HandType handType, int level, int time)
        {
            return HandleControllerSession.Instance.VibrateHandle((int)handType, level, time);
        }

        /// <summary>
        /// 获取手柄绑定状态
        /// </summary>
        /// <param name="handType"></param>
        /// <returns></returns>
        public bool GetBindState(HandType handType)
        {
            return HandleControllerSession.Instance.GetBindState(handType);
        }

        /// <summary>
        /// 获取手柄连接状态
        /// </summary>
        public bool GetConnectState(HandType handType)
        {
            return HandleControllerSession.Instance.GetConnectState((int)handType);
        }

        /// <summary>
        /// 获取手柄电量
        /// </summary>
        public float GetPowerStats(HandType handType)
        {
            return HandleControllerSession.Instance.GetPowerStats((int)handType);
        }

        /// <summary>
        /// 手柄按键是否按下
        /// </summary>
        public bool GetButtonDown(HandType handType, HandleKeyCode keyCode)
        {
            if (handType == HandType.Left)
                return leftHand.GetButtonDown(keyCode);
            else
                return rightHand.GetButtonDown(keyCode);
        }
        /// <summary>
        /// 手柄按键是否弹起
        /// </summary>
        public bool GetButtonUp(HandType handType, HandleKeyCode keyCode)
        {
            if (handType == HandType.Left)
                return leftHand.GetButtonUp(keyCode);
            else
                return rightHand.GetButtonUp(keyCode);
        }
        /// <summary>
        /// 手柄按键是否长按
        /// </summary>
        public bool GetButton(HandType handType, HandleKeyCode keyCode)
        {
            if (handType == HandType.Left)
                return leftHand.GetButton(keyCode);
            else
                return rightHand.GetButton(keyCode);
        }
        /// <summary>
        /// 获取手柄摇杆坐标
        /// </summary>
        /// <param name="handType">左手柄or右手柄</param>
        /// <returns>坐标范围[-1,-1]到[1,1]，复位为[0,0]</returns>
        public Vector2 GetAxis2D(HandType handType)
        {
            if (handType == HandType.Left)
                return leftHand.GetAxis2D();
            else
                return rightHand.GetAxis2D();
        }

        private void OnBindingEventCallback(HandType handType, bool status)
        {
            if (handType == HandType.Left)
            {
                leftHand.UpdateBindingState(status, _status =>
                {
                    bindingActions.ForEach(action => { if (action != null) action(HandType.Left, _status); });
                });
            }
            else
            {
                rightHand.UpdateBindingState(status, _status =>
                {
                    bindingActions.ForEach(action => { if (action != null) action(HandType.Right, _status); });
                });
            }
        }

        private void OnConnectEventCallback(HandType handType, bool connected)
        {
            if (handType == HandType.Left)
            {
                leftHand.UpdateConnectedState(connected, _connected =>
                {
                    connectedActions.ForEach(action => { if (action != null) action(HandType.Left, _connected); });
                });
            }
            else
            {
                rightHand.UpdateConnectedState(connected, _connected =>
                {
                    connectedActions.ForEach(action => { if (action != null) action(HandType.Right, _connected); });
                });
            }
        }

        private void OnPowerStatsChangedCallback(HandType handType, float power)
        {
            if (handType == HandType.Left)
            {
                leftHand.UpdatePowerStats(power, _power =>
                {
                    powerChangedActions.ForEach(action => { if (action != null) action(HandType.Left, _power); });
                });
            }
            else
            {
                rightHand.UpdatePowerStats(power, _power =>
                {
                    powerChangedActions.ForEach(action => { if (action != null) action(HandType.Right, _power); });
                });
            }
        }

        private void OnButtonEventCallback(HandType handType, HandleKeyCode keycode, bool pressed)
        {
            if (handType == HandType.Left)
                leftHand.UpdateButtonState(keycode, pressed);
            else
                rightHand.UpdateButtonState(keycode, pressed);
        }

        private void OnAxis2DEventCallback(HandType handType, Vector2 coord)
        {
            if (handType == HandType.Left)
                leftHand.UpdateAxis2D(coord);
            else
                rightHand.UpdateAxis2D(coord);
        }

        private void OnHoldEventCallback(HandType handType, HandleKeyCode keycode, bool isHeld)
        {
            if (handType == HandType.Left)
                leftHand.UpdateHoldState(isHeld);
            else
                rightHand.UpdateHoldState(isHeld);
        }

        /// <summary>
        /// 定时刷新手柄电量
        /// </summary>
        /// <returns></returns>
        private IEnumerator WaitForTimeout()
        {
            while (true)
            {
                yield return new WaitForSeconds(300);
                if (leftHand.Exist) OnPowerStatsChangedCallback(HandType.Left, GetPowerStats(HandType.Left));
                if (rightHand.Exist) OnPowerStatsChangedCallback(HandType.Right, GetPowerStats(HandType.Right));
            }
        }
    }

}