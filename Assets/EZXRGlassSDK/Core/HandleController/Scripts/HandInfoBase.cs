using System;
using System.Collections;
using System.Collections.Generic;
using EZXR.Glass.Hand;
using EZXR.Glass.SixDof;
using UnityEngine;
using Wheels.Unity;

public enum HandType
{
    Left = 0,
    Right = 1
}

public abstract class HandInfoBase : MonoBehaviour
{
    /// <summary>
    /// 这个HandInfo是属于左手还是右手，0为左手，1为右手
    /// </summary>
    [HideInInspector]
    public HandType handType;
    /// <summary>
    /// 记录了左手的所有信息，包括当前有没有捏合，有没有握持物体等等
    /// </summary>
    public static HandInfoBase leftHand;
    /// <summary>
    /// 记录了右手的所有信息，包括当前有没有捏合，有没有握持物体等等
    /// </summary>
    public static HandInfoBase rightHand;

    /// <summary>
    /// 当前手是否在做捏合动作
    /// </summary>
    [HideInInspector]
    public bool isPinching;
    /// <summary>
    /// 手开始做捏合动作（仅仅开始捏合的帧为true）
    /// </summary>
    [HideInInspector]
    public bool startPinch;
    /// <summary>
    /// 手结束做捏合动作（仅仅结束捏合的帧为true）
    /// </summary>
    [HideInInspector]
    public bool endPinch;


    /// <summary>
    /// 是否启用射线交互
    /// </summary>
    public bool enableRayInteraction;
    /// <summary>
    /// 射线起始生效距离，决定了射线起始点的位置（从肩膀到食指根节点为发射方向，食指根节点Position + 生效距离 * 发射方向）
    /// </summary>
    public float rayStartDistance = 0.15f;
    /// <summary>
    /// 手的射线起始点
    /// </summary>
    [HideInInspector]
    public Vector3 rayPoint_Start;
    /// <summary>
    /// 手的射线终止点
    /// </summary>
    [HideInInspector]
    public Pose rayPoint_End;
    /// <summary>
    /// 从射线起始点到终止点的射线方向（单位向量）
    /// </summary>
    [HideInInspector]
    public Vector3 rayDirection;


    /// <summary>
    /// 手捏合时刻抓住的点（在被抓住的物体的局部坐标系下的坐标）
    /// </summary>
    public Vector3 grabLocalPoint;
    /// <summary>
    /// 手正在拖拽
    /// </summary>
    public bool isDragging;


    /// <summary>
    /// 当前眼镜视野中是否存在这个手
    /// </summary>
    public virtual bool Exist
    {
        get
        {
            return false;
        }
    }
    /// <summary>
    /// 手掌正在朝向头部，如果手掌朝向和手掌与头的连线的夹角小于angle则认为手掌是朝向了头部
    /// </summary>
    /// <param name="angle">默认90</param>
    /// <returns></returns>
    public abstract bool isPalmFacingHead(float angle = 90);

    /// <summary>
    /// 初始化，设置当前HandInfo所属的手是左手还是右手
    /// </summary>
    /// <param name="handType">0为左手，1为右手</param>
    public virtual void Init(HandType handType)
    {
        this.handType = handType;

        if (handType == HandType.Left) leftHand = this;
        if (handType == HandType.Right) rightHand = this;
    }

    protected virtual void Awake()
    {

    }

    protected virtual void Start()
    {

    }

    protected virtual void Update()
    {
        UpdatePinching();
    }

    protected virtual void UpdatePinching()
    {

    }

    #region 当前正在触碰的物体信息
    /// <summary>
    /// 指示当前射线是否正射在某个物体上
    /// </summary>
    public bool isRayContacting;
    /// <summary>
    /// 当前射线碰到的Collider（仅仅是射线碰到，并不是射线抓取）
    /// </summary>
    protected Collider curRayContactingTarget;
    public Collider CurRayContactingTarget
    {
        get
        {
            return curRayContactingTarget;
        }
        set
        {
            if (value == null)
            {
                isRayContacting = false;
            }
            else
            {
                isRayContacting = true;
            }

            //if (curRayContactingTarget != value)
            {
                //记录上一帧被射线打到的物体的相关信息
                LastRayContactingTarget = curRayContactingTarget;

                curRayContactingTarget = value;

                if (curRayContactingTarget != null)
                {
                    curRayContactingTarget_Renderer = (curRayContactingTarget == null ? null : curRayContactingTarget.GetComponent<Renderer>());
                    if (curRayContactingTarget_Renderer != null)
                    {
                        if (curRayContactingTarget_PropertyBlock == null)
                        {
                            curRayContactingTarget_PropertyBlock = new MaterialPropertyBlock();
                        }
                        curRayContactingTarget_Renderer.GetPropertyBlock(curRayContactingTarget_PropertyBlock);
                    }

                    if (curRayContactingTarget_Renderer != null && curRayContactingTarget_PropertyBlock != null)
                    {
                        curRayContactingTarget_PropertyBlock.SetVector("_HitWorldPos", Vector3.one * 1000);
                        curRayContactingTarget_Renderer.SetPropertyBlock(curRayContactingTarget_PropertyBlock);
                    }
                }
            }
        }
    }

    /// <summary>
    /// 射线碰到Collider的时候触发此事件
    /// </summary>
    public event Action<Collider, bool> Event_GetRayCastResult;

    /// <summary>
    /// 由HandRaycast调用此处来设置当前射线射击到的目标
    /// </summary>
    /// <param name="other">射线打到的Collider</param>
    /// <param name="isUI">射线打到的是否是UI</param>
    public void SetCurRayContactingTarget(Collider other, bool isUI = false)
    {
        if (CurRayContactingTarget != null && other != null)
        {
            if (CurRayContactingTarget != other)
            {
                SpatialObject.PerformOnHandRayExit(CurRayContactingTarget);
                SpatialObject.PerformOnHandRayEnter(other);
            }
            else
            {
                SpatialObject.PerformOnHandRayStay(other);
            }
        }
        else if (CurRayContactingTarget == null && other != null)
        {
            SpatialObject.PerformOnHandRayEnter(other);
        }
        else if (CurRayContactingTarget != null && other == null)
        {
            SpatialObject.PerformOnHandRayExit(CurRayContactingTarget);
        }

        CurRayContactingTarget = other;

        if (Event_GetRayCastResult != null)
        {
            //将射线检测结果分发出
            Event_GetRayCastResult(other, isUI);
        }
    }
    public Renderer curRayContactingTarget_Renderer;
    public MaterialPropertyBlock curRayContactingTarget_PropertyBlock;

    /// <summary>
    /// 射线上一次碰到的Collider
    /// </summary>
    protected Collider lastRayContactingTarget;
    public Collider LastRayContactingTarget
    {
        get
        {
            return lastRayContactingTarget;
        }
        set
        {
            if (lastRayContactingTarget != value)
            {
                lastRayContactingTarget = value;
                if (lastRayContactingTarget != null)
                {
                    lastRayContactingTarget_Renderer = (lastRayContactingTarget == null ? null : lastRayContactingTarget.GetComponent<Renderer>());
                    if (lastRayContactingTarget_Renderer != null)
                    {
                        if (lastRayContactingTarget_PropertyBlock == null)
                        {
                            lastRayContactingTarget_PropertyBlock = new MaterialPropertyBlock();
                        }
                        lastRayContactingTarget_Renderer.GetPropertyBlock(lastRayContactingTarget_PropertyBlock);
                    }
                }
            }
        }
    }
    public Renderer lastRayContactingTarget_Renderer;
    public MaterialPropertyBlock lastRayContactingTarget_PropertyBlock;
    #endregion

    #region 当前正在抓取的物体的信息
    /// <summary>
    /// 指示手正在通过射线拖拽物体
    /// </summary>
    protected bool isRayGrabbing;
    /// <summary>
    /// 指示手正在通过射线拖拽物体
    /// </summary>
    public bool IsRayGrabbing
    {
        get
        {
            return isRayGrabbing;
        }
        set
        {
            isRayGrabbing = value;

        }
    }

    protected Transform curRayGrabbingTarget;
    /// <summary>
    /// 当前射线正在抓取的物体（非旋转手柄或者缩放角）
    /// </summary>
    public Transform CurRayGrabbingTarget
    {
        get
        {
            return curRayGrabbingTarget;
        }
        set
        {
            if (curRayGrabbingTarget == null && value == null)
            {
                return;
            }

            if (curRayGrabbingTarget == null && value != null)
            {
                SpatialObject.PerformOnHandRayGrab(value.GetComponent<Collider>());
            }
            else if (curRayGrabbingTarget != null && value == null)
            {
                SpatialObject.PerformOnHandRayRelease(curRayGrabbingTarget.GetComponent<Collider>());
            }

            if (value != null)
            {
                CurRayGrabbingSpatialObject = value.GetComponent<SpatialObject>();
            }
            else
            {
                CurRayGrabbingSpatialObject = null;
            }

            curRayGrabbingTarget = value;
        }
    }

    protected SpatialObject curRayGrabbingSpatialObject;
    public SpatialObject CurRayGrabbingSpatialObject
    {
        get
        {
            return curRayGrabbingSpatialObject;
        }
        set
        {
            curRayGrabbingSpatialObject = value;
        }
    }

    protected Transform curCloseGrabbingTarget;
    /// <summary>
    /// 当前正在近距离抓取的Transform（非旋转手柄或者缩放角）
    /// </summary>
    /// <param name="other"></param>
    public Transform CurCloseGrabbingTarget
    {
        get
        {
            return curCloseGrabbingTarget;
        }
        set
        {
            if (curCloseGrabbingTarget == null && value == null)
            {
                return;
            }

            if (curCloseGrabbingTarget == null && value != null)
            {
                SpatialObject.PerformOnHandTriggerGrab(value.GetComponent<Collider>());
            }
            else if (curCloseGrabbingTarget != null && value == null)
            {
                SpatialObject.PerformOnHandTriggerRelease(curCloseGrabbingTarget.GetComponent<Collider>());
            }

            if (value != null)
            {
                CurCloseGrabbingSpatialObject = value.GetComponent<SpatialObject>();
            }
            else
            {
                CurCloseGrabbingSpatialObject = null;
            }

            curCloseGrabbingTarget = value;
        }
    }

    protected SpatialObject curCloseGrabbingSpatialObject;
    public SpatialObject CurCloseGrabbingSpatialObject
    {
        get
        {
            return curCloseGrabbingSpatialObject;
        }
        set
        {
            curCloseGrabbingSpatialObject = value;
        }
    }

    /// <summary>
    /// 手当前是否正在抓取物体（无论射线还是近距离）
    /// </summary>
    public bool IsGrabbing
    {
        get
        {
            if (CurCloseGrabbingTarget == null && CurRayGrabbingTarget == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
    /// <summary>
    /// 得到当前手正在抓取的物体（无论射线还是近距离）
    /// </summary>
    public Transform CurGrabbingTarget
    {
        get
        {
            if (CurCloseGrabbingTarget == null)
            {
                if (CurRayGrabbingTarget != null)
                {
                    return CurRayGrabbingTarget;
                }
            }
            else
            {
                return CurCloseGrabbingTarget;
            }
            return null;
        }
    }
    #endregion

    #region 用于非SpatialObject物体
    [HideInInspector]
    public Transform lastPointingObject;
    /// <summary>
    /// 仅用于非SpatialObject物体，指示手正在通过射线点击非SpatialObject，会向这种物体发送pointerDownHandler事件，通常用于UGUI这类的点击
    /// </summary>
    [HideInInspector]
    public bool IsRayPressing;
    [HideInInspector]
    public Transform curPressingObject;

    #endregion

}
