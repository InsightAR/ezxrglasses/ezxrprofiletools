using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using AOT;
using EZXR.Glass.SixDof;
using UnityEngine;

namespace EZXR.Glass.HandleController
{
    public enum HandleKeyCode
    {
        Home = 301,
        Return = 300,
        Primary = 303,
        Secondary = 302,
        Rocker = 304,
        Trigger = 305,
        Grid = 306
    }

    public class HandleControllerSession : MonoBehaviour
    {
        //[StructLayout(LayoutKind.Sequential, Pack = 1)]
        [StructLayout(LayoutKind.Sequential)]
        private struct HandControllerResult
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
            public float[] pose;    //xyz, wxyz
            public float is_3dof;
            public double timestamp;
            public int controllerId; //0左手柄，1右手柄
        }

        private static HandleControllerSession instance;
        public static HandleControllerSession Instance
        {
            get
            {
                return instance;
            }
        }

        //public Transform leftController;
        //public Transform rightController;
        //public static Action<int> OnKeyPressed;
        //public static Action<int> OnKeyReleased;

        public static Pose controllerPose_left = new Pose();
        public static Pose controllerPose_right = new Pose();

        private HandControllerResult controllerLeftResult = new HandControllerResult();
        private HandControllerResult controllerRightResult = new HandControllerResult();

        private Action<HandType, bool> bindingEventCallback;
        private Action<HandType, bool> connectEventCallback;
        private Action<HandType, HandleKeyCode, bool> buttonEventCallback;
        private Action<HandType, Vector2> axis2DEventCallback;
        private Action<HandType, HandleKeyCode, bool> holdEventCallback;

        private delegate void ControllerBindEventCallback(int value);
        private delegate void ControllerConnectEventCallback(bool connected, int controllerId);
        private delegate void ControllerButtonEventCallback(int keycode, int state, int controllerId);
        private delegate void ControllerAxis2DEventCallback(int x, int y, int controllerId);
        private delegate void ControllerHoldEventCallback(int keycode, bool isHeld, int controllerId);

        private void Awake()
        {
            instance = this;

            NativeAPI.registerControllerBondChangeCallback(OnControllerBindEventCallback);
            NativeAPI.registerControllerConnectEventCallback(OnControllerConnectEventCallback);
            NativeAPI.registerControllerTouchEvent(OnControllerButtonEventCallback);
            NativeAPI.registerControllerRockerTouchEvent(OnControllerAxis2DEventCallback);
            NativeAPI.registerControllerTouchChangeEventCallback(OnControllerHoldEventCallback);
            Debug.Log("HandleControllerSession, successfully register Events(5)");
        }

        // Start is called before the first frame update
        private void Start()
        {
            controllerLeftResult.controllerId = -1;
            controllerRightResult.controllerId = -1;
            controllerLeftResult.pose = new float[7];
            controllerRightResult.pose = new float[7];

            //leftController.localPosition = new Vector3(0.0017f, -0.006f, 0.01f);
            //leftController.localEulerAngles = new Vector3(-87.5f, 0, 0);
            //rightController.localPosition = new Vector3(0.0017f, -0.006f, 0.01f);
            //rightController.localEulerAngles = new Vector3(-87.5f, 0, 0);

        }

        // Update is called once per frame
        private void Update()
        {
            /*
            if (Input.GetKeyDown(KeyCode.Space))
            {
                //Debug.Log("HandleControllerSession, Space KeyDown");
                Debug.Log($"HandleControllerSession, get Battery: L{NativeAPI.GetControllerBattery(0)}, R{NativeAPI.GetControllerBattery(1)}");
                Debug.Log($"HandleControllerSession, get ConnectState: L{NativeAPI.GetControllerConnectState(0)}, R{NativeAPI.GetControllerConnectState(1)}");
            }
            if (Input.GetKeyDown(KeyCode.C))
            {
                //Debug.Log("HandleControllerSession, C KeyDown");
                Debug.Log($"HandleControllerSession, cancel Binding: L{NativeAPI.ClearBond(0)}, R{NativeAPI.ClearBond(1)}");
            }
            if (Input.GetKeyDown(KeyCode.L))
            {
                //Debug.Log("HandleControllerSession, L KeyDown");
                Debug.Log($"HandleControllerSession, Left Binding: {NativeAPI.bondController(0)}");
            }
            if (Input.GetKeyDown(KeyCode.R))
            {
                //Debug.Log("HandleControllerSession, R KeyDown");
                Debug.Log($"HandleControllerSession, Right Binding: {NativeAPI.bondController(1)}");
            }
            if (Input.GetKeyDown(KeyCode.V))
            {
                //Debug.Log("HandleControllerSession, V KeyDown");
                Debug.Log($"HandleControllerSession, vibrate: {NativeAPI.vibrateController(1, 2, 200)}");
            }
            if (Input.GetKeyDown(KeyCode.B))
            {
                //Debug.Log("HandleControllerSession, B KeyDown");
                Debug.Log($"HandleControllerSession, bind status: {NativeAPI.GetControllerBondState() & 0xF0}");
            }
            */
        }

        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="connectEventCallback"></param>
        /// <param name="buttonEventCallback"></param>
        /// <param name="axis2DEventCallback"></param>
        public void InitRegistration(Action<HandType, bool> bindingEventCallback,
                                     Action<HandType, bool> connectEventCallback,
                                     Action<HandType, HandleKeyCode, bool> buttonEventCallback,
                                     Action<HandType, Vector2> axis2DEventCallback,
                                     Action<HandType, HandleKeyCode, bool> holdEventCallback)
        {
            this.bindingEventCallback = bindingEventCallback;
            this.connectEventCallback = connectEventCallback;
            this.buttonEventCallback = buttonEventCallback;
            this.axis2DEventCallback = axis2DEventCallback;
            this.holdEventCallback = holdEventCallback;
        }

        /// <summary>
        /// 进行手柄配对
        /// </summary>
        /// <param name="controllerId">0左手柄，1右手柄</param>
        /// <returns></returns>
        public bool BindHandle(int controllerId)
        {
            // 先解绑，再配对
            NativeAPI.ClearBond(controllerId);
            // 返回0是执行正常，成功与否要从回调中获得
            return NativeAPI.bondController(controllerId) >= 0;
        }

        /// <summary>
        /// 取消手柄配对
        /// </summary>
        /// <param name="controllerId">0左手柄，1右手柄</param>
        /// <returns></returns>
        public bool UnbindHandle(int controllerId)
        {
            // 返回0是执行正常，成功与否要从回调中获得
            return NativeAPI.ClearBond(controllerId) >= 0;
        }

        /// <summary>
        /// 获取绑定状态
        /// </summary>
        /// <param name="handType">指定手柄</param>
        /// <returns></returns>
        public bool GetBindState(HandType handType)
        {
            int value = NativeAPI.GetControllerBondState();
            if (handType == HandType.Left)
            {
                if ((value & 0x0F) == 0x00) return false;   // unbind
                if ((value & 0x0F) == 0x04) return true;    // bound
            }
            else
            {
                if ((value & 0xF0) == 0x00) return false;   // unbind
                if ((value & 0xF0) == 0x40) return true;    // bound
            }
            Debug.Log($"HandleControllerSession, GetBindState Exception: {value}");
            return false;
        }

        /// <summary>
        /// 获取连接状态
        /// </summary>
        /// <param name="controllerId">0左手柄，1右手柄</param>
        /// <returns></returns>
        public bool GetConnectState(int controllerId)
        {
            // 返回-1或0是未连接，返回1是已连接
            return NativeAPI.GetControllerConnectState(controllerId) > 0;
        }

        /// <summary>
        /// 获取手柄电量
        /// </summary>
        /// <param name="controllerId">0左手柄，1右手柄</param>
        /// <returns></returns>
        public float GetPowerStats(int controllerId)
        {
            // 电量0到6，转换成百分比
            return NativeAPI.GetControllerBattery(controllerId) / 6.0f;    // 满电量6
        }

        /// <summary>
        /// 进行手柄震动
        /// </summary>
        /// <param name="controllerId">0左手柄，1右手柄</param>
        /// <param name="level">震动等级 1-8</param>
        /// <param name="time">震动时长 0-65535 ms</param>
        public bool VibrateHandle(int controllerId, int level, int time)
        {
            // 返回0是执行正常，没有回调
            return NativeAPI.vibrateController(controllerId, level, time) >= 0;
        }

        /// <summary>
        /// 更新手柄位姿状态
        /// </summary>
        public void UpdateHandlePose()
        {
            if (ARFrame.SessionStatus == EZVIOState.EZVIOCameraState_Tracking)
            {
                if (NativeAPI.getControllerResult(ref controllerLeftResult, ref controllerRightResult))
                {
#if UNITY_EDITOR
                    var left_position = Vector3.zero;
                    var left_rotation = Quaternion.identity;
                    var right_position = Vector3.zero;
                    var right_rotation = Quaternion.identity;
#else
                    var left_position = new Vector3(controllerLeftResult.pose[0], controllerLeftResult.pose[1], controllerLeftResult.pose[2]);
                    var left_rotation = new Quaternion(controllerLeftResult.pose[4], controllerLeftResult.pose[5], controllerLeftResult.pose[6], controllerLeftResult.pose[3]);
                    var right_position = new Vector3(controllerRightResult.pose[0], controllerRightResult.pose[1], controllerRightResult.pose[2]);
                    var right_rotation = new Quaternion(controllerRightResult.pose[4], controllerRightResult.pose[5], controllerRightResult.pose[6], controllerRightResult.pose[3]);
#endif
                    controllerPose_left.position = left_position;
                    controllerPose_left.rotation = left_rotation;
                    ConversionUtility.RecenterByOffset(controllerPose_left, ARFrame.accumulatedRecenterOffset4x4, ref controllerPose_left);
                    //leftController.SetPositionAndRotation(controllerPose_left.position, controllerPose_left.rotation);

                    controllerPose_right.position = right_position;
                    controllerPose_right.rotation = right_rotation;
                    ConversionUtility.RecenterByOffset(controllerPose_right, ARFrame.accumulatedRecenterOffset4x4, ref controllerPose_right);
                    //rightController.SetPositionAndRotation(controllerPose_right.position, controllerPose_right.rotation);
                }
            }
        }

        //手柄绑定状态回调
        [MonoPInvokeCallback(typeof(ControllerBindEventCallback))]
        private static void OnControllerBindEventCallback(int value)
        {
            bool status = false;
            HandType handType = HandType.Left;
            if ((value & 0x0F) == 0x00) { handType = HandType.Left; status = false; }
            if ((value & 0x0F) == 0x04) { handType = HandType.Left; status = true; }
            if ((value & 0xF0) == 0x00) { handType = HandType.Right; status = false; }
            if ((value & 0xF0) == 0x40) { handType = HandType.Right; status = true; }
            Debug.Log($"HandleControllerSession, OnControllerBindEventCallback: handType = {handType}, status = {status}");
            HandleControllerSession.Instance.bindingEventCallback(handType, status);
        }

        //手柄连接状态回调 左手0 右手1
        [MonoPInvokeCallback(typeof(ControllerConnectEventCallback))]
        private static void OnControllerConnectEventCallback(bool connected, int controllerId)
        {
            Debug.Log($"HandleControllerSession, OnControllerConnectEventCallback: connected = {connected}, controllerId = {controllerId}");
            HandleControllerSession.Instance.connectEventCallback((HandType)controllerId, connected);
        }

        //手柄按键事件回调
        //controllerId 0左手柄，1右手柄
        //state 1按下，0弹起
        //keycode 300返回，301主页，302BY键，303AX键，304摇杆键
        [MonoPInvokeCallback(typeof(ControllerButtonEventCallback))]
        private static void OnControllerButtonEventCallback(int keycode, int state, int controllerId)
        {
            //Debug.Log($"HandleControllerSession, OnControllerButtonEventCallback: keycode = {keycode}, state = {state}, controllerId = {controllerId}");
            HandleControllerSession.Instance.buttonEventCallback((HandType)controllerId, (HandleKeyCode)keycode, state > 0);
        }

        //手柄摇杆事件回调
        //controllerId 0左手柄，1右手柄
        //xy整型，左下角[0,0]，中间[8,8]，右上角[15,15]
        [MonoPInvokeCallback(typeof(ControllerAxis2DEventCallback))]
        private static void OnControllerAxis2DEventCallback(int x, int y, int controllerId)
        {
            //Debug.Log($"HandleControllerSession, OnControllerAxis2DEventCallback: x = {x}, y = {y}, controllerId = {controllerId}");
            float axis_x = x < 8 ? Mathf.Lerp(-1, 0, x / 8.0f) : Mathf.Lerp(0, 1, (x - 8) / (15.0f - 8.0f));
            float axis_y = y < 8 ? Mathf.Lerp(-1, 0, y / 8.0f) : Mathf.Lerp(0, 1, (y - 8) / (15.0f - 8.0f));
            HandleControllerSession.Instance.axis2DEventCallback((HandType)controllerId, new Vector2(axis_x, axis_y));
        }

        //手柄握持事件回调
        //controllerId 0左手柄，1右手柄
        //level 震动等级 1-8
        //time 震动时长 0-65535 ms
        [MonoPInvokeCallback(typeof(ControllerAxis2DEventCallback))]
        private static void OnControllerHoldEventCallback(int keycode, bool isHeld, int controllerId)
        {
            Debug.Log($"HandleControllerSession, OnControllerHoldEventCallback: keycode = {keycode}, hold = {isHeld}, controllerId = {controllerId}");
            HandleControllerSession.Instance.holdEventCallback((HandType)controllerId, (HandleKeyCode)keycode, isHeld);
        }

        private partial struct NativeAPI
        {
#if !UNITY_EDITOR
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern bool getControllerResult(ref HandControllerResult controllerLeftResult, ref HandControllerResult controllerRightResult);

            //注册手柄按键事件回调
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void registerControllerTouchEvent(ControllerButtonEventCallback callback);
            //注册手柄摇杆事件回调
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void registerControllerRockerTouchEvent(ControllerAxis2DEventCallback callback);
            //注册手柄握持事件回调
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void registerControllerTouchChangeEventCallback(ControllerHoldEventCallback callback);
            //注册手柄绑定状态回调
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void registerControllerBondChangeCallback(ControllerBindEventCallback callback);
            //注册手柄连接状态回调
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void registerControllerConnectEventCallback(ControllerConnectEventCallback callback);

            //进行手柄配对：左手0 右手1 (手柄同时按住扳机键和返回键)
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern int bondController(int controllerId);
            //取消手柄配对：左手0 右手1
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern int ClearBond(int controllerId);
            //获取手柄绑定状态
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern int GetControllerBondState();
            //获取手柄连接状态：1连接,0未连接
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern int GetControllerConnectState(int controllerId);
            //获取手柄电量，[0, 6]
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern int GetControllerBattery(int controllerId);
            //进行手柄震动
            //level 震动等级 1-8
            //time 震动时长 0-65535 ms
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern int vibrateController(int controllerId, int level, int time);
#else
            public static bool getControllerResult(ref HandControllerResult controllerLeftResult, ref HandControllerResult controllerRightResult) {
                return false;
            }

            //注册手柄按键事件回调
            public static void registerControllerTouchEvent(ControllerButtonEventCallback callback) { }
            //注册手柄摇杆事件回调
            public static void registerControllerRockerTouchEvent(ControllerAxis2DEventCallback callback) { }
            //注册手柄握持事件回调
            public static void registerControllerTouchChangeEventCallback(ControllerHoldEventCallback callback) { }
            //注册手柄绑定状态回调
            public static void registerControllerBondChangeCallback(ControllerBindEventCallback callback) { }
            //注册手柄连接状态回调
            public static void registerControllerConnectEventCallback(ControllerConnectEventCallback callback) { }

            //进行手柄配对：左手0 右手1 (手柄同时按住扳机键和返回键)
            public static int bondController(int controllerId) {
                return -1;
            }
            //取消手柄配对：左手0 右手1
            public static int ClearBond(int controllerId) {
                return -1;
            }
            //获取手柄绑定状态
            public static int GetControllerBondState() {
                return -1;
            }
            //获取手柄连接状态：1连接,0未连接
            public static int GetControllerConnectState(int controllerId) {
                return -1;
            }
            //获取手柄电量，[0, 6]
            public static int GetControllerBattery(int controllerId) {
                return -1;
            }
            //进行手柄震动
            //level 震动等级 1-8
            //time 震动时长 0-65535 ms
            public static int vibrateController(int controllerId, int level, int time) {
                return -1;
            }
#endif
        }

    }
}