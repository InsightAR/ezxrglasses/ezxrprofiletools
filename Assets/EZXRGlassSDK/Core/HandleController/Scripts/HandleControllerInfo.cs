using System;
using System.Collections;
using System.Collections.Generic;
using EZXR.Glass.Common;
using EZXR.Glass.HandleController;
using UnityEngine;

namespace EZXR.Glass.HandleController
{
    [ScriptExecutionOrder(-100)]
    public class HandleControllerInfo : HandInfoBase
    {
        public Transform handleOrigin;

        protected bool isBinding = false; 

        protected bool isConnected = false;

        protected float powerStats = 0.0f;

        protected Vector2 axis2DCoord;

        /// <summary>
        /// 监听手柄的生命状态改变(立即发生)
        /// </summary>
        private bool bindingChanged = false;
        private bool connectedChanged = false;
        private bool powerChanged = false;
        /// <summary>
        /// 监听到生命状态改变的回调(在Update中发生)
        /// 可能立即发生也可能滞后一帧
        /// </summary>
        private Action<bool> bindingAction;
        private Action<bool> connectedAction;
        private Action<float> powerChangedAction;
        /// 保存按键状态(在Update中发生，可能立即发生也可能滞后一帧)
        private Dictionary<HandleKeyCode, HandleKeyEvent> buttonState;
        /// 监听按键状态的改变(立即发生)
        private Dictionary<HandleKeyCode, bool> buttonStateChanged;
        //private Dictionary<HandleKeyCode, float> buttonDownTimeSpan;

        /// <summary>
        /// 手柄当前是否被握持
        /// </summary>
        protected bool isHeld;
        public bool IsHeld
        {
            get
            {
                return isHeld;
            }
        }

        /// <summary>
        /// 当前眼镜视野中是否存在这个手
        /// </summary>
        public override bool Exist
        {
            get
            {
                return isConnected;
            }
        }
        /// <summary>
        /// 手掌正在朝向头部
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        public override bool isPalmFacingHead(float angle = 90) { return false; }


        protected override void Awake()
        {
            base.Awake();

            buttonState = new Dictionary<HandleKeyCode, HandleKeyEvent>();
            //buttonDownTimeSpan = new Dictionary<HandleKeyCode, float>();
            buttonStateChanged = new Dictionary<HandleKeyCode, bool>();
        }

        protected override void Update()
        {
            base.Update();

            UpdateStatus();

            if (handleOrigin != null)
            {
                //射线方向
                rayDirection = handleOrigin.forward;

                //射线起点
                rayPoint_Start = handleOrigin.position + rayDirection * rayStartDistance;
            }
        }

        public override void Init(HandType handType)
        {
            base.Init(handType);

            isBinding = HandleControllerManager.Instance.GetBindState(handType);
            isConnected = HandleControllerManager.Instance.GetConnectState(handType);
            powerStats = HandleControllerManager.Instance.GetPowerStats(handType);
            Debug.Log($"HandleControllerInfo, Init: handType = {this.handType} isBinding = {isBinding}, isConnected = {isConnected}, powerStats = {powerStats}");

            StartCoroutine(CheckStatus());
        }

        public void UpdateBindingState(bool status, Action<bool> callback)
        {
            if (isBinding == status) return;

            isBinding = status;
            bindingChanged = true;
            bindingAction = callback;
            Debug.Log($"HandleControllerInfo, UpdateBinding: handType = {this.handType} isBinding = {status}");
        }

        public void UpdateConnectedState(bool connected, Action<bool> callback)
        {
            if (isConnected == connected) return;

            isConnected = connected;
            connectedChanged = true;
            connectedAction = callback;
            Debug.Log($"HandleControllerInfo, UpdateConnectedState: handType = {this.handType} isConnected = {connected}");
        }

        public void UpdatePowerStats(float power, Action<float> callback)
        {
            Debug.Log($"HandleControllerInfo, UpdatePowerStats: handType = {this.handType} power = {power}");
            if (powerStats == power) return;

            powerStats = power;
            powerChanged = true;
            powerChangedAction = callback;
        }

        public void UpdateButtonState(HandleKeyCode keyCode, bool pressed)
        {
            buttonStateChanged[keyCode] = true;
            if (!buttonState.ContainsKey(keyCode)) buttonState[keyCode] = HandleKeyEvent.Idle;

            if (pressed)
                Debug.Log($"HandleControllerInfo, {handType} {keyCode} StateChanged to pressed ({Time.frameCount})");
            else
                Debug.Log($"HandleControllerInfo, {handType} {keyCode} StateChanged to released ({Time.frameCount})");

            /*
            if (!buttonState.ContainsKey(keyCode)) buttonState[keyCode] = HandleKeyEvent.Idle;
            if (buttonState[keyCode] == HandleKeyEvent.Idle)
            {
                if (pressed)
                {
                    //buttonState[keyCode] = HandleKeyEvent.Down;
                    buttonStateChanged[keyCode] = true;
                    Debug.Log($"HandleControllerInfo, {handType} {keyCode} StateChanged to pressed ({Time.frameCount})");
                    //按下扳机键触发开始捏合动作
                    //if (keyCode == HandleKeyCode.Trigger) startPinch = true;
                }
            }
            else //if (buttonState[keyCode] == HandleKeyEvent.Pressing)
            {
                if (!pressed)
                {
                    //buttonState[keyCode] = HandleKeyEvent.Up;
                    buttonStateChanged[keyCode] = true;
                    Debug.Log($"HandleControllerInfo, {handType} {keyCode} StateChanged to released ({Time.frameCount})");
                    //弹起扳机键触发结束捏合动作
                    //if (keyCode == HandleKeyCode.Trigger) endPinch = true;
                }
            }
            */
        }
        public void UpdateAxis2D(Vector2 coord)
        {
            axis2DCoord = coord;
        }

        public void UpdateHoldState(bool isHeld)
        {
            this.isHeld = isHeld;
        }

        public bool GetButtonDown(HandleKeyCode keyCode)
        {
            if (buttonState.ContainsKey(keyCode))
                return buttonState[keyCode] == HandleKeyEvent.Down;
            return false;
        }
        public bool GetButtonUp(HandleKeyCode keyCode)
        {
            if (buttonState.ContainsKey(keyCode))
                return buttonState[keyCode] == HandleKeyEvent.Up;
            return false;
        }
        public bool GetButton(HandleKeyCode keyCode)
        {
            if (buttonState.ContainsKey(keyCode))
                return buttonState[keyCode] == HandleKeyEvent.Pressing;
            return false;
        }
        public Vector2 GetAxis2D()
        {
            return axis2DCoord;
        }

        protected override void UpdatePinching()
        {
            var keys = new List<HandleKeyCode>(buttonStateChanged.Keys);
            for (int j = 0; j < keys.Count; ++j)
            {
                var keyCode = keys[j];
                if (!buttonStateChanged[keyCode]) continue;
                if (buttonState[keyCode] == HandleKeyEvent.Idle)
                {
                    buttonState[keyCode] = HandleKeyEvent.Down;
                    Debug.Log($"HandleControllerInfo, {handType} {keyCode} change to Down from Idle ({Time.frameCount})");
                    //按下扳机键触发开始捏合动作
                    if (keyCode == HandleKeyCode.Trigger) startPinch = true;
                }
                else if (buttonState[keyCode] == HandleKeyEvent.Pressing)
                {
                    buttonState[keyCode] = HandleKeyEvent.Up;
                    Debug.Log($"HandleControllerInfo, {handType} {keyCode} change to Up from Pressing ({Time.frameCount})");
                    //按下扳机键触发开始捏合动作
                    if (keyCode == HandleKeyCode.Trigger) startPinch = true;
                }
                //buttonStateChanged.Remove(keyCode);
            }

            keys = new List<HandleKeyCode>(buttonState.Keys);
            for (int i = 0; i < keys.Count; ++i)
            {
                var keyCode = keys[i];
                if (buttonStateChanged.ContainsKey(keyCode))
                {
                    buttonStateChanged.Remove(keyCode);
                    continue;
                }
                if (buttonState[keyCode] == HandleKeyEvent.Down)
                {
                    buttonState[keyCode] = HandleKeyEvent.Pressing;
                    Debug.Log($"HandleControllerInfo, {handType} {keyCode} change to Pressing from Down ({Time.frameCount})");
                    // startPinch结束，触发持续捏合
                    if (keyCode == HandleKeyCode.Trigger) { startPinch = false; isPinching = true; }
                }
                else if (buttonState[keyCode] == HandleKeyEvent.Up)
                {
                    buttonState[keyCode] = HandleKeyEvent.Idle;
                    Debug.Log($"HandleControllerInfo, {handType} {keyCode} change to Idle from Up ({Time.frameCount})");
                    // endPinch结束，触发结束捏合
                    if (keyCode == HandleKeyCode.Trigger) { endPinch = false; isPinching = false; }
                }
            }

            /*
            var keys = new List<HandleKeyCode>(buttonState.Keys);
            for (int i = 0; i < keys.Count; ++i)
            {
                var key = keys[i];
                var value = buttonState[key];
                if (value == HandleKeyEvent.Idle) continue;
                if (value == HandleKeyEvent.Down)
                {
                    buttonState[key] = HandleKeyEvent.PrePressed;
                    buttonDownTimeSpan[key] = 0;
                    Debug.Log($"HandleControllerInfo, {handType} {key} change to PrePressed from Down ({Time.frameCount})");
                }
                else if (value == HandleKeyEvent.Up)
                {
                    buttonState[key] = HandleKeyEvent.Idle;
                    buttonDownTimeSpan.Remove(key);
                    //弹起扳机键触发结束捏合
                    if (key == HandleKeyCode.Trigger) isPinching = false;
                    Debug.Log($"HandleControllerInfo, {handType} {key} change to Idle from Up ({Time.frameCount})");
                }
                //else if (value == HandleKeyEvent.PrePressed)
                if (buttonState[key] == HandleKeyEvent.PrePressed)
                {
                    buttonDownTimeSpan[key] += Time.deltaTime;
                    //长按扳机键触发持续捏合
                    if (key == HandleKeyCode.Trigger) isPinching = true;
                    if (buttonDownTimeSpan[key] > 0.5f)
                    {
                        buttonState[key] = HandleKeyEvent.Pressing;
                        buttonDownTimeSpan.Remove(key);
                        Debug.Log($"HandleControllerInfo, {handType} {key} change to Pressing from PrePressed ({Time.frameCount})");
                    }
                }
            }

            if (startPinch)
            {
                startPinch = false;
                Debug.Log($"HandleControllerInfo, reset startPinch to false ({Time.frameCount})");
            }
            if (endPinch)
            {
                endPinch = false;
                Debug.Log($"HandleControllerInfo, reset endPinch to false ({Time.frameCount})");
            }

            if (isPinching)
            {
                Debug.Log($"HandleControllerInfo, {handType} isPinching ({Time.frameCount})");
            }
            */
        }

        private void UpdateStatus()
        {
            if (bindingChanged)
            {
                if (bindingAction != null) bindingAction(isBinding);
                bindingChanged = false;
            }

            if (connectedChanged)
            {
                if (connectedAction != null) connectedAction(isConnected);
                connectedChanged = false;
            }

            if (powerChanged)
            {
                if (powerChangedAction != null) powerChangedAction(powerStats);
                powerChanged = false;
            }
        }

        // for fix
        private IEnumerator CheckStatus()
        {
            yield return null;
            yield return null;
            yield return null;

            isBinding = HandleControllerManager.Instance.GetBindState(handType);
            isConnected = HandleControllerManager.Instance.GetConnectState(handType);
            powerStats = HandleControllerManager.Instance.GetPowerStats(handType);
            Debug.Log($"HandleControllerInfo, Check: handType = {this.handType} isBinding = {isBinding}, isConnected = {isConnected}, powerStats = {powerStats} ({Time.frameCount})");
        }
    }
}