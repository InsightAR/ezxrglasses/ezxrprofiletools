﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZXR.Glass.Recording;
#if ARMiracast
using Unity.RenderStreaming;
#endif

namespace EZXR.Glass.MiraCast
{
    public class MiracastManager : MonoBehaviour
    {
        #region 单例
        private static MiracastManager instance;
        public static MiracastManager Instance
        {
            get
            {
                return instance;
            }
        }
        #endregion

        public int width = 1280;
        public int height = 960;
#if ARMiracast
        public RenderStreaming renderStreaming;
#endif

        private void Awake()
        {
            if (Application.isPlaying)
            {
                instance = this;

#if ARMiracast
                renderStreaming = GetComponent<RenderStreaming>();
#endif
                ARRenderRGB.SetRGBResolution(width, height);
            }
            else
            {

            }
        }

    }
}