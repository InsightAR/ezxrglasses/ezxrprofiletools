﻿using UnityEditor;
using UnityEngine;

namespace EZXR.Glass.Common
{
    public partial class ARAbilities : MonoBehaviour
    {
        #region SystemManager
        [MenuItem("GameObject/XR Abilities/Additional.../SystemManager", false, 100)]
        public static void EnableSystemManager()
        {
            if (FindObjectOfType<SystemManager>() == null)
            {
                string filePath = "Assets/EZXRGlassSDK/Core/SystemManager/Prefabs/SystemManager.prefab";
                if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
                {
                    filePath = AssetDatabase.GUIDToAssetPath("52a7e5cd09269b642a76e2048d9e43d1");
                }
                Common.PrefabUtility.InstantiatePrefabWithUndoAndSelection(AssetDatabase.LoadAssetAtPath<GameObject>(filePath));
            }
        }
        #endregion
    }
}