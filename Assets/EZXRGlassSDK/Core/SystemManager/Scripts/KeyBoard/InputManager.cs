﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace EZXR.Glass.Inputs
{
    public class InputManager : MonoBehaviour
    {
        private static InputManager instance;

        public KeyBoard keyBoard;

        public static Action Callback_OnPowerPressed;
        public static Action Callback_OnPowerLongPressed;
        public static Action Callback_OnVolumeIncreasePressed;
        public static Action Callback_OnVolumeDecreasePressed;
        public enum KeyEventType
        {
            Power,
            PowerLong,
            VolumeIncrease,
            VolumeDecrease,
        }
        public Dictionary<KeyEventType, bool> keyEventHandler = new Dictionary<KeyEventType, bool>();


        private void Awake()
        {
            instance = this;

            foreach (KeyEventType item in Enum.GetValues(typeof(KeyEventType)))
            {
                keyEventHandler.Add(item, false);
            }
        }
        // Start is called before the first frame update
        void Start()
        {
            keyBoard.Hide();
        }

        // Update is called once per frame
        void Update()
        {
            if (EventSystem.current != null && EventSystem.current.currentSelectedGameObject != null)
            {
                InputField inputField = EventSystem.current.currentSelectedGameObject.GetComponent<InputField>();
                TMP_InputField tmpInputField = EventSystem.current.currentSelectedGameObject.GetComponent<TMP_InputField>();
                if (inputField != null)
                {
                    keyBoard.Show(inputField);
                }
                else if(tmpInputField != null)
                {
                    keyBoard.Show(tmpInputField);
                }
                else
                {
                    keyBoard.Hide();
                }
            }
            //else
            //{
            //    keyBoard.Hide();
            //}
        }

        //public bool GetKey(KeyEventType keyEventType)
        //{
        //    return keyEventHandler[keyEventType];
        //}

        //public void GetKeyDown(KeyEventType keyEventType)
        //{

        //}

        //public void GetKeyUp(KeyEventType keyEventType)
        //{

        //}
    }
}