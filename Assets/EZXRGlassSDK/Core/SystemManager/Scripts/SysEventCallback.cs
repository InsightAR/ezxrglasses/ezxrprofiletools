﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EZXR.Glass.SystemMenu
{
    public class SysEventCallback : AndroidJavaProxy
    {
        public event Action<PermissionInfo[], int> callBack_OnPermissionEvent;
        public event Action<string, int> callBack_OnToast;

        public SysEventCallback() : base("com.ezxr.glass.nativelib.callback.SysEventCallback")
        {
        }

        public SysEventCallback(
            Action<PermissionInfo[], int> callBack_OnPermissionEvent = null,
            Action<string, int> callBack_OnToast = null
            ) : base("com.ezxr.glass.nativelib.callback.SysEventCallback")
        {
            this.callBack_OnPermissionEvent += callBack_OnPermissionEvent;
            this.callBack_OnToast += callBack_OnToast;
        }

        /// <summary>
        /// 用户请求权限事件,收到该回调请弹窗
        /// </summary>
        /// <param name="permissionInfos"></param>
        /// <param name="requestCode"></param>
        void onPermissionEvent(PermissionInfo[] permissionInfos, int requestCode)
        {
            Debug.Log("SysEventCallback--> onPermissionEvent, permissionInfos.Length:" + permissionInfos.Length + ", requestCode:" + requestCode);

            if (callBack_OnPermissionEvent != null)
            {
                callBack_OnPermissionEvent(permissionInfos, requestCode);
            }
        }

        /// <summary>
        /// 需要弹出toast
        /// </summary>
        /// <param name="content"></param>
        /// <param name="duration"></param>
        void onToast(string content, int duration)
        {
            Debug.Log("SysEventCallback--> onToast, content:" + content + ", duration:" + duration);

            if (callBack_OnToast != null)
            {
                callBack_OnToast(content, duration);
            }
        }
    }
}