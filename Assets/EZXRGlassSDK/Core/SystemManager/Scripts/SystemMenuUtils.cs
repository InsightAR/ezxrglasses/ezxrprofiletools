﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EZXR.Glass.SystemMenu
{
    public class SystemMenuUtils
    {
        static AndroidJavaObject instance;
        public static AndroidJavaObject Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AndroidJavaClass("com.ezxr.glass.nativelib.API").CallStatic<AndroidJavaObject>("getInstance");

                    AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                    AndroidJavaObject activity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
                    Init(
                        activity,
                        new SysEventCallback(OnPermissionEvent, OnToast)
                        );
                }
                return instance;
            }
        }

        public static Action<PermissionInfo[], int> Callback_OnPermissionEvent;
        public static Action<string, float> Callback_OnToast;


        static void OnPermissionEvent(PermissionInfo[] permissionInfos, int requestCode)
        {
            Debug.Log("SystemMenuUtils--> OnPermissionEvent, permissionInfos.Length:" + permissionInfos.Length + ", requestCode:" + requestCode);

            if (Callback_OnPermissionEvent != null)
            {
                Callback_OnPermissionEvent(permissionInfos, requestCode);
            }
        }

        /// <summary>
        /// 需要弹出toast
        /// </summary>
        /// <param name="content"></param>
        /// <param name="duration"></param>
        static void OnToast(string content, int duration)
        {
            Debug.Log("SystemMenuUtils--> onToast, content:" + content + ", duration:" + duration);

            if (Callback_OnToast != null)
            {
                Callback_OnToast(content, duration);
            }
        }

        public static void Init()
        {
            instance = Instance;
        }

        /// <summary>
        /// 进入应用时调用
        /// </summary>
        /// <param name="context"></param>
        /// <param name="keyCallback"></param>
        static void Init(
                        AndroidJavaObject context,
                        SysEventCallback keyCallback
            )
        {
#if UNITY_EDITOR

#else
            Instance.Call("init", context, keyCallback);
#endif
        }

        /// <summary>
        /// 退出应用时调用
        /// </summary>
        public static void DeInit()
        {
#if UNITY_EDITOR

#else
            Instance.Call("deInit");
#endif
        }

        /// <summary>
        /// 获取string，如果key不存在则返回defaultValue
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string GetString(string key, string defaultValue)
        {
#if UNITY_EDITOR
            return "";
#else
            return Instance.Call<string>("getString", key, defaultValue);
#endif
        }

        /// <summary>
        /// 获取int，如果key不存在则返回defaultValue
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static int GetInt(string key, int defaultValue)
        {
#if UNITY_EDITOR
            return 0;
#else
            return Instance.Call<int>("getInt", key, defaultValue);
#endif
        }

        /// <summary>
        /// 获取boolean，如果key不存在则返回defaultValue
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static bool GetBoolean(string key, bool defaultValue)
        {
#if UNITY_EDITOR
            return false;
#else
            return Instance.Call<bool>("getBoolean", key, defaultValue);
#endif
        }

        /// <summary>
        /// 获取float，如果key不存在则返回defaultValue
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static float GetFloat(string key, float defaultValue)
        {
#if UNITY_EDITOR
            return 0;
#else
            return Instance.Call<float>("getFloat", key, defaultValue);
#endif
        }

        /// <summary>
        /// 获取string数组，如果key不存在则返回defaultValue
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string[] GetStringArray(string key, string[] defaultValue)
        {
#if UNITY_EDITOR
            return null;
#else
            return Instance.Call<string[]>("getStringArray", key, defaultValue);
#endif
        }

        /// <summary>
        /// 获取设备SN号
        /// </summary>
        /// <returns></returns>
        public static string GetDeviceId()
        {
#if UNITY_EDITOR
            return "";
#else
            return Instance.Call<string>("getDeviceId");
#endif
        }

        /// <summary>
        /// 保存完图片或视频后可以调用该接口添加到图库
        /// </summary>
        /// <returns></returns>
        /// <param name="path">绝对路径</param>
        public static void AddToAlbum(string path)
        {
#if UNITY_EDITOR

#else
            Instance.Call("addToAlbum", path);
#endif
        }

        /// <summary>
        /// 获取截屏录屏目录
        /// </summary>
        public static string GetScreenshotsPath()
        {
#if UNITY_EDITOR
            return "";
#else
            return Instance.Call<string>("getScreenshotsPath");
#endif
        }
    }
}