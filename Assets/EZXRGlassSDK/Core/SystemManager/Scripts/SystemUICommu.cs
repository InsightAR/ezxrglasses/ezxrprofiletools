using EZXR.Glass.Common;
using EZXR.Glass.Hand;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SystemUICommu : MonoBehaviour
{
    bool lastRayState_L = false;
    bool lastRayState_R = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (lastRayState_L != ARHandManager.leftHand.isRayContacting || lastRayState_R != ARHandManager.rightHand.isRayContacting)
        {
            if (ARHandManager.leftHand.isRayContacting || ARHandManager.rightHand.isRayContacting)
            {
                Utilities.Android.SendIntent("HandInfo", "com.ezxr.glass.systemui", "RayState", "False");
            }
            else
            {
                Utilities.Android.SendIntent("HandInfo", "com.ezxr.glass.systemui", "RayState", "True");
            }

            lastRayState_L = ARHandManager.leftHand.isRayContacting;
            lastRayState_R = ARHandManager.rightHand.isRayContacting;
        }
    }
}
