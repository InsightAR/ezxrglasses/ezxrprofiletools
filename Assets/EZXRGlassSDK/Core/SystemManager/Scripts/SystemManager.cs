﻿using EZXR.Glass.Common;
using EZXR.Glass.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[ScriptExecutionOrder(-40)]
public class SystemManager : MonoBehaviour
{
    private static SystemManager instance;

    float lastBackPressTime = 0f;

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;

        Debug.Log("SystemManager--> InputManager");
        Instantiate(ResourcesManager.Load<GameObject>("InputManager"));
        Instantiate(ResourcesManager.Load<GameObject>("OSEventSystem"));
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnApplicationQuit()
    {

    }
}
