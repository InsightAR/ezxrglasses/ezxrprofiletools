﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EZXR.Glass.SystemMenu
{
    public class PermissionInfo
    {
        /// <summary>
        /// 权限类型，与Android系统的权限类型一致
        /// </summary>
        public string permission;
        /// <summary>
        /// 弹窗时界面上显示的详细信息，不包含固定前缀，由开发者自己添加
        /// </summary>
        public string permissionTip;
    }
}