using EZXR.Glass.Common;
using EZXR.Glass.Hand;
using EZXR.Glass.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;

public class OSEventSystem : MonoBehaviour
{
    public static Action<bool> OnWristTouched;

    // Start is called before the first frame update
    void Start()
    {
        BroadcastReceiver.Instance.Register("WristPanel_Touched", "Recording", "HandInfo");
        BroadcastReceiver.Instance.RegisterCallback("isSystemMenuShow", OnReceive_IsSystemMenuShow);
        BroadcastReceiver.Instance.RegisterCallback("instructions", OnReceive_RecordingInstructions);
        BroadcastReceiver.Instance.RegisterCallback("RayState", OnReceive_RayState);
    }

    private void OnReceive_IsSystemMenuShow(string data)
    {
        bool enabled = bool.Parse(data);

        Debug.Log("UnityBroadcastReceiver OnReceive_IsSystemMenuShow: " + data);
        ARHandManager.leftHand.GetComponent<HandsVisualization>().Visibility = enabled;
        ARHandManager.rightHand.GetComponent<HandsVisualization>().Visibility = enabled;

        if (OnWristTouched != null)
        {
            OnWristTouched(enabled);
        }
    }

    private void OnReceive_RecordingInstructions(string data)
    {
        Debug.Log("UnityBroadcastReceiver OnReceive_RecordingInstructions: " + data);

        switch (data)
        {
            case "recordingStart":
                HandleRecordingOpen();
                break;
            case "recordingStop":
                HandleRecordingClose();
                break;
            case "screenshotStart":
                SetScreenCapture();
                break;
        }
    }

    private void OnReceive_RayState(string data)
    {
        bool enable = bool.Parse(data);

        Debug.Log("UnityBroadcastReceiver OnReceive_RayState: " + data);
        ARHandManager.leftHand.RaycastInteraction = enable;
        ARHandManager.rightHand.RaycastInteraction = enable;
        UGUIHandler.Instance.gameObject.SetActive(enable);
    }

    // 录屏
    private void HandleRecordingOpen()
    {
        if (RecordingManager.Instance != null)
            RecordingManager.Instance.HandleRecordingOpen();
    }
    private void HandleRecordingClose()
    {
        if (RecordingManager.Instance != null)
            RecordingManager.Instance.HandleRecordingClose();
    }

    // 截屏
    private void SetScreenCapture()
    {
        if (RecordingManager.Instance != null)
            RecordingManager.Instance.HandleScreenCapture();
    }


    /// <summary>
    /// 设置系统级交互的状态，包括：食指指尖Home、手腕菜单、系统级手的显示状态
    /// </summary>
    /// <param name="state">false表示禁用系统级交互</param>
    public static void SetSystemInteractionState(bool state)
    {
        Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "SetSystemHandState", state.ToString());
    }
}
