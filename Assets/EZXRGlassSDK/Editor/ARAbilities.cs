﻿using UnityEditor;
using UnityEngine;

namespace EZXR.Glass.Common
{
    public partial class ARAbilities : MonoBehaviour
    {
        #region PhoneScreenUI（针对分体式眼镜）
        [MenuItem("GameObject/XR Abilities/Additional.../PhoneScreenUI", false, 90)]
        public static void EnablePhoneScreenUI()
        {
            if (FindObjectOfType<PhoneUIRenderer>() == null)
            {
                string filePath = "Assets/EZXRGlassSDK/Core/Shared/Resources/PhoneScreenUI.prefab";
                if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
                {
                    filePath = AssetDatabase.GUIDToAssetPath("cfbaf50af0b162b4ebe2e4180ff35950");
                }
                Common.PrefabUtility.InstantiatePrefabWithUndoAndSelection(AssetDatabase.LoadAssetAtPath<GameObject>(filePath));
            }
        }
        #endregion
    }
}