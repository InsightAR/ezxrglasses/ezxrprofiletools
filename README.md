# EZXRProfileTools使用文档

EZXRProfileTools AR内容性能工具

## 使用步骤

#### 1.下载EZXRProfileTools工程

https://gitlab.com/InsightAR/ezxrglasses/ezxrprofiletools

#### 2.打开Profile Scene

![1](doc/1.png)

#### 3.替换Prefab

在ProfileController组件里，替换要查看性能的美术资源Prefab

建议美术资源按照ART里面的文件夹的方式组织

该Prefab会在眼镜里显示在应用运行时的相机正前方1m处

#### 4.打包眼镜应用

1）确保眼镜OS系统，目前是OS 0.2.1，方法有2种

a. 更新眼镜固件（暂时还未release，该方法需要等固件release）

b.手动更新

需要更新ezxrGlassServer、ARLauncher、SystemUI/

当前OS 0.2.1版本下，这三个应用对应的版本是：

ezxrGlassServer-insight_ar_glass-2.3.0.beta10.apk

ARLauncher_v0.9.2.3.apk

SystemUI_v0.1.2.3.apk

2）眼镜连接电脑，Unity的build Settings->Run device能显示出X1眼镜

3）选择Build And Run

![2](doc/2.png)



## Release Note

### V0.1.0

1. 支持导出EZXR Glasses X1工程
